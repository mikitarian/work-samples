Solution Approach:

Because we are dealing with scientific and medical data I took a pointed approach of only transforming specific characters.  Because of the unique and very specific meaning associated with special characters used in this setting, I didn’t want to assume too much when performing a search-and-replace.

The result is this block of code added to the processSPL method in SplDocumentService.java
<pre>
//	TODO:  turn the below into a separate method  
  
//	TODO:  Get IE to render unicode vs text for LTE and GTE  
xmlContent = xmlContent.replace("â‰¤", "LTE ");			//Less Than or Equal To**  
//xmlContent = xmlContent.replace("â‰¤", "GTE ");		//Greater Than or Equal To**  
xmlContent = xmlContent.replaceAll("â„¢", "\u2122");		//Trademark  
xmlContent = xmlContent.replaceAll("Î¼", "\u00B5");		//Micro symbol, in Greek, µ  
xmlContent = xmlContent.replaceAll("Â¬", "\u00AC");		//"not" sign in logical notation  
xmlContent = xmlContent.replaceAll("â€¢", "\u2022");		//Bullet  
xmlContent = xmlContent.replaceAll("Â¢", "\u00A2");		//Cent, currency  
xmlContent = xmlContent.replaceAll("Ë†", "\u02C6");		//Circumflex  
xmlContent = xmlContent.replaceAll("â€š", "\u201A");		//comma  
xmlContent = xmlContent.replaceAll("Â©", "\u00A9");		//Copyright  
xmlContent = xmlContent.replaceAll("Â³", "\u00B3");		//Cubed  
xmlContent = xmlContent.replaceAll("Â¤", "\u00A4");		//Currency, unspecified  
xmlContent = xmlContent.replaceAll("â€", "\u2020");		//Dagger  
xmlContent = xmlContent.replaceAll("Â°", "\u00B0");		//Degree symbol  
xmlContent = xmlContent.replaceAll("Ã•", "\u00F7");		//Division operator  
xmlContent = xmlContent.replaceAll("â€¡", "\u2021");		//Double Dagger  
xmlContent = xmlContent.replaceAll("â€¦", "\u2026");		//Elipses  
xmlContent = xmlContent.replaceAll("â€”", "\u2014");		//em dash  
xmlContent = xmlContent.replaceAll("â€“", "\u2013");		//en dash  
xmlContent = xmlContent.replaceAll("â‚¬", "\u20AC");		//Euro curreny  
xmlContent = xmlContent.replaceAll("Æ’", "\u0192");		//Florin currency  
xmlContent = xmlContent.replaceAll("â€˜", "\u2018");		//Left apostrophe  
xmlContent = xmlContent.replaceAll("Âº", "\u00BA");		//Masculine ordinal symbol  
xmlContent = xmlContent.replaceAll("Ã˜", "\u00D8");		//Mathematically: diameter  
xmlContent = xmlContent.replaceAll("Â•", "\u00B7");		//Multiplication operator  
xmlContent = xmlContent.replaceAll("Ã—", "\u00D7");		//Multiplication operator  
xmlContent = xmlContent.replaceAll("Âª", "\u00AA");		//ordinal indicator  
xmlContent = xmlContent.replaceAll("â€°", "\u2030");		//Per mille, as in per thousand  
xmlContent = xmlContent.replaceAll("Â±", "\u00B1");		//Plus-Minus sign  
xmlContent = xmlContent.replaceAll("Â£", "\u00A3");		//Pound, currency  
xmlContent = xmlContent.replaceAll("Â®", "\u00AE");		//Registered symbol  
xmlContent = xmlContent.replaceAll("â€™", "\u2019");		//Right apostrophe  
xmlContent = xmlContent.replaceAll("Â§", "\u00A7");		//section sign (as in doc section)  
xmlContent = xmlContent.replaceAll("Ëœ", "\u02DC");		//Square Enix (NOT a tilde)  
xmlContent = xmlContent.replaceAll("Â²", "\u00B2");		//Squared  
xmlContent = xmlContent.replaceAll("Â¹", "\u00B9");		//Superscript 1  
xmlContent = xmlContent.replaceAll("Â¯", "\u00AF");		//Upperscore  
xmlContent = xmlContent.replaceAll("Â¦", "\u00A6");		//vertical broken bar  
xmlContent = xmlContent.replaceAll("Â½", "\u00BD");		//Vulgar fraction one half  
xmlContent = xmlContent.replaceAll("Â¼", "\u00BC");		//Vulgar fraction one quarter  
xmlContent = xmlContent.replaceAll("Â¾", "\u00BE");		//Vulgar fraction three quarters  
xmlContent = xmlContent.replaceAll("Â¥", "\u00A5");		//Yen or Yuan, currency  
  
//	**I chose text replacement here because IE wouldn't render the Unicode characters of \u2264 (LTE) and \u2265 (GTE)  
//	The corrupt representation of ≥ still needs to be confirmed before running replaceAll on it  
//	TODO:  turn the above into a separate method  
</pre>  
