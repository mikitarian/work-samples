# CloudManager Deploy

This is the main CloudManager application deploy which creates the ECS cluster for the various microservices as well as other required resources.

Sepcific resources can be targetted with the following

```bash
./run_terraform.sh dev apply -target="aws_s3_bucket.www" -target="aws_s3_bucket_public_access_block.www" -target="aws_s3_bucket_website_configuration.www"
```

### env directory

The `env` directory contains the Terraform variables (`env/<environment name>.tfvars`) as well as the set of parameters created in Systems Manager Parameter Store (`env/<envirnoment name>.yaml`).

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | ~> 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.67.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.5.1 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_api_gateway"></a> [api\_gateway](#module\_api\_gateway) | ../modules/ecs-service | n/a |
| <a name="module_auth_server"></a> [auth\_server](#module\_auth\_server) | ../modules/ecs-service | n/a |
| <a name="module_cloud_manager_server"></a> [cloud\_manager\_server](#module\_cloud\_manager\_server) | ../modules/ecs-service | n/a |
| <a name="module_db"></a> [db](#module\_db) | ../community-modules/terraform-aws-rds | n/a |
| <a name="module_ddb_permission"></a> [ddb\_permission](#module\_ddb\_permission) | ../community-modules/terraform-aws-dynamodb-table | n/a |
| <a name="module_ddb_site_role"></a> [ddb\_site\_role](#module\_ddb\_site\_role) | ../community-modules/terraform-aws-dynamodb-table | n/a |
| <a name="module_ddb_user"></a> [ddb\_user](#module\_ddb\_user) | ../community-modules/terraform-aws-dynamodb-table | n/a |
| <a name="module_ddb_user_profile"></a> [ddb\_user\_profile](#module\_ddb\_user\_profile) | ../community-modules/terraform-aws-dynamodb-table | n/a |
| <a name="module_ddb_user_role"></a> [ddb\_user\_role](#module\_ddb\_user\_role) | ../community-modules/terraform-aws-dynamodb-table | n/a |
| <a name="module_nlb"></a> [nlb](#module\_nlb) | ../modules/aws-lb | n/a |
| <a name="module_purchase_request_server"></a> [purchase\_request\_server](#module\_purchase\_request\_server) | ../modules/ecs-service | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_log_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_dynamodb_table_item.admin_user](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table_item) | resource |
| [aws_dynamodb_table_item.admin_user_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table_item) | resource |
| [aws_dynamodb_table_item.permission](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table_item) | resource |
| [aws_dynamodb_table_item.site_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table_item) | resource |
| [aws_ecs_cluster.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_cluster) | resource |
| [aws_s3_bucket.cloudtracker_invoices](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.purchase_requests](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.www](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_policy.www](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) | resource |
| [aws_s3_bucket_public_access_block.cloudtracker_invoices](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_public_access_block.purchase_requests](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_public_access_block.www](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_server_side_encryption_configuration.cloudtracker_invoices](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_server_side_encryption_configuration) | resource |
| [aws_s3_bucket_server_side_encryption_configuration.purchase_requests](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_server_side_encryption_configuration) | resource |
| [aws_s3_bucket_server_side_encryption_configuration.www](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_server_side_encryption_configuration) | resource |
| [aws_s3_bucket_versioning.cloudtracker_invoices](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_versioning) | resource |
| [aws_s3_bucket_versioning.purchase_requests](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_versioning) | resource |
| [aws_s3_bucket_website_configuration.www](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_website_configuration) | resource |
| [aws_secretsmanager_secret.db_connection_string](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret) | resource |
| [aws_secretsmanager_secret_version.db_connection_string](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret_version) | resource |
| [aws_security_group.ecs-services](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_service_discovery_private_dns_namespace.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/service_discovery_private_dns_namespace) | resource |
| [aws_ssm_parameter.cloud_region](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.invoice_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.name_prefix](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.purchase_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.ssm_paremeters_with_values](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.ssm_paremeters_without_values](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [random_password.db](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [aws_iam_policy_document.www_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [aws_secretsmanager_secret.admin_user_secret](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret) | data source |
| [aws_secretsmanager_secret_version.admin_user_info](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_container_repository_base"></a> [container\_repository\_base](#input\_container\_repository\_base) | The base path for the container repository | `string` | `"281978155256.dkr.ecr.us-gov-east-1.amazonaws.com"` | no |
| <a name="input_db_allocated_storage"></a> [db\_allocated\_storage](#input\_db\_allocated\_storage) | Allocated size for RDS instance | `number` | `20` | no |
| <a name="input_db_engine"></a> [db\_engine](#input\_db\_engine) | The plain text name of database engine to use | `string` | `"sqlserver-web"` | no |
| <a name="input_db_engine_version"></a> [db\_engine\_version](#input\_db\_engine\_version) | The database engine to use | `string` | `"15.00.4236.7.v1"` | no |
| <a name="input_db_family"></a> [db\_family](#input\_db\_family) | The database family to use | `string` | `"sqlserver-web-15.0"` | no |
| <a name="input_db_instance_class"></a> [db\_instance\_class](#input\_db\_instance\_class) | The instance class to use for the database | `string` | `"db.t3.medium"` | no |
| <a name="input_db_major_engine_version"></a> [db\_major\_engine\_version](#input\_db\_major\_engine\_version) | The database engine to use | `string` | `"15.00"` | no |
| <a name="input_db_max_allocated_storage"></a> [db\_max\_allocated\_storage](#input\_db\_max\_allocated\_storage) | Max allocated size for RDS instance | `number` | `1000` | no |
| <a name="input_ecs_execution_role"></a> [ecs\_execution\_role](#input\_ecs\_execution\_role) | The IAM role to use for ECS execution | `string` | n/a | yes |
| <a name="input_ecs_task_role"></a> [ecs\_task\_role](#input\_ecs\_task\_role) | The IAM role used by the ECS task | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | The environment name, used to prefix and distinguish resources | `string` | n/a | yes |
| <a name="input_fqdn"></a> [fqdn](#input\_fqdn) | The FQDN of the application (do not include https://) | `string` | n/a | yes |
| <a name="input_image_tag"></a> [image\_tag](#input\_image\_tag) | The container image tag to use when pulling images from repository | `string` | n/a | yes |
| <a name="input_log_retention_days"></a> [log\_retention\_days](#input\_log\_retention\_days) | Number of days to retain logs | `number` | `60` | no |
| <a name="input_private_subnets"></a> [private\_subnets](#input\_private\_subnets) | List of private subnets to use | `list(string)` | n/a | yes |
| <a name="input_public_subnets"></a> [public\_subnets](#input\_public\_subnets) | List of private subnets to use | `list(string)` | n/a | yes |
| <a name="input_ui_fqdn"></a> [ui\_fqdn](#input\_ui\_fqdn) | The FQDN of the S3 UI bucket (do not include http:// | `string` | n/a | yes |
| <a name="input_vpc_cidr_block"></a> [vpc\_cidr\_block](#input\_vpc\_cidr\_block) | The CIDR of the VPC | `string` | n/a | yes |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | The ID of the VPC | `string` | n/a | yes |
| <a name="input_web_ingress_cidr_list"></a> [web\_ingress\_cidr\_list](#input\_web\_ingress\_cidr\_list) | List of CIDR blocks to add to ingress for api-gateway | `list(string)` | `[]` | no |
| <a name="input_web_ingress_prefix_list"></a> [web\_ingress\_prefix\_list](#input\_web\_ingress\_prefix\_list) | List of prefix lists to add to ingress for api-gateway | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_nlb_dns"></a> [nlb\_dns](#output\_nlb\_dns) | The DNS of the network load balancer |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->