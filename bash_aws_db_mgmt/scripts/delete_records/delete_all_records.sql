SET NOCOUNT ON;

DELETE FROM $(dbname).dbo.AlertRoleXWalk;

DELETE FROM $(dbname).dbo.Alerts;

DELETE FROM $(dbname).dbo.AlertRoles;

DELETE FROM $(dbname).dbo.AlertType;

DELETE FROM $(dbname).dbo.AlertUserRead;

DELETE FROM $(dbname).dbo.CloudServiceOrders;

DELETE FROM $(dbname).dbo.CloudAccounts;

DELETE FROM $(dbname).dbo.CrmInvoice;

DELETE FROM $(dbname).dbo.FinancialPOCs;

DELETE FROM $(dbname).dbo.FundingAllocation;

DELETE FROM CM_DEMO_DATA.dbo.PurchaseRequests;

DELETE FROM $(dbname).dbo.Notifications;

DELETE FROM $(dbname).dbo.PurchaseAgreement;

DELETE FROM $(dbname).dbo.PurchaseBundles;

DELETE FROM $(dbname).dbo.PurchaseRequestForm;

DELETE FROM $(dbname).dbo.SystemOwners;

DELETE FROM $(dbname).dbo.TechnicalPOCs;

SET NOCOUNT OFF;