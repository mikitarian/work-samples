import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { Breeds } from '../interfaces/breeds.interface';
import { BREEDS } from '../models/breeds.model';

@Injectable({
  providedIn: 'root',
})
export class BreedsService {

  constructor() { }

  getBreeds(): Observable<Breeds[]> {
    return of(BREEDS);
  }

  getBreedById(id: number): Observable<Breeds>{
    return this.getBreeds().pipe(
        map(breeds => breeds.find(breed => breed.id === id))
    );
  }

}
