export interface Breeds {
  id: number;
  name: string;
  about: string;
  status: string;
}
