**Research and Helpful Links:**  
  
**Transformed Characters Table**  
https://www.i18nqa.com/debug/utf8-debug.html  
https://www.i18nqa.com/debug/bug-iso8859-1-vs-windows-1252.html  
https://www.i18nqa.com/debug/bug-double-conversion.html  
https://www.i18nqa.com/debug/bug-utf-8-latin1.html  
  
**juniversalchardet example**  
https://code.google.com/archive/p/juniversalchardet/  
  
**StringEscapeUtils examples**  
https://javarevisited.blogspot.com/2012/09/how-to-replace-escape-xml-special-characters-java-string.html  
https://stackoverflow.com/questions/37189437/replace-non-english-character-in-a-string-with-utf-8-character-in-android-java    
