from app_defaults import *

class DefaultTaskListDirectory():
	def __init__(self,target_directory):
		self.target_directory = target_directory


class TaskListInfo():
	def __init__(self, filename, working_directory):
		self.filename = filename
		self.directory = working_directory

	def get_task_list_contents(self,selected_task_list):

		task_list = Path(selected_task_list)

		#check for existence of selected file
		if task_list.is_file() == True:
			file = open(task_list, "r")
			all_tasks = file.readlines()
			file.close()
			return all_tasks
		else:
			pass


class SpinnerListInfo():
	def __init__(self):
		pass

	def get_list_contents(self,target_file,prepend_flag):

		list_items = []
		with open(target_file, 'r') as items:
			for line in sorted(items, key=str.casefold):
				list_items.append(line.strip())
			
			if prepend_flag == True:
				list_items.insert(0,'[i](Edit List)[/i]')
				list_items.insert(0,'')

		return list_items