import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { MatDialog, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { BreedDialogComponent } from '../breed-dialog/breed-dialog.component';
import { Breeds } from '../../interfaces/breeds.interface';
import { BREEDS } from '../../models/breeds.model';
import { BreedsService } from 'src/app/services/breeds.service';

@Component({
  selector: 'app-breeds',
  templateUrl: './breeds.component.html',
  styleUrls: ['./breeds.component.scss']
})
export class BreedsComponent implements OnInit {

  breeds = BREEDS;
  // breeds$: Observable<Breeds[]>;

  // columns = [
  //   { prop: 'id', name: 'ID' },
  //   { prop: 'name', name: 'Name' },
  //   { prop: 'about', name: 'About' },
  //   { prop: 'status', name: 'Status' },
  //   { prop: 'id', name: 'Controls'},
  // ];

  // "breeds" is an Array that comes from models/breeds.model
  // "breeds$" is an Observable that comes from interfaces/breeds.interface
  // We can loop through either of them in the UI
  // If we choose the breeds array, syntax is *ngFor="let breed of breeds"
  // If we choose the breeds$ observable, syntax is *ngFor"let breed of breeds$ | async"
  // Observables:
  //      emit multiple values over time
  //      are lazy loading
  //      can be canceled
  //      can be used with map, filter, reduce and other operators


  title = 'Breeds';
  timeout: any;
  selected: any;

  constructor(
    private titleService:Title,
    private breedsService: BreedsService,
    private matDialog: MatDialog,
    ) {}

  // get all breeds from breedsService
  // getBreeds(): void {
  //   this.breeds$ = this.breedsService.getBreeds();
  // }

  // onSelect(breed: any) {
  //   // console.log('Select Event', selected, this.selected);
  //   console.log('ID Received: ' + breed.id);

  //   // initialize Material Dialog Configuration
  //   const dialogConfig = new MatDialogConfig();

  //   // set Configuration Parameters
  //   dialogConfig.disableClose = true;
  //   dialogConfig.autoFocus = true;
  //   dialogConfig.data = breed;

  //   // set a dialog reference handle ("dialogRef")
  //   // so we can manipulate the dialog later on
  //   let dialogRef = this.matDialog.open(BreedDialogComponent, dialogConfig);

  //   // use dialogRef to do stuff in conjunction with a dialog event (.afterClosed)
  //   dialogRef.afterClosed().subscribe(data => {
  //     console.log(`ID Sent: ${dialogConfig.data.id}`);
  //   });

  // }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    // this.getBreeds();
  }

}
