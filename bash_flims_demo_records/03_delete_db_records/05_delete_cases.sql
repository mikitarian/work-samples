/* DELETE records from case TABLE
	
	Sam Mikitarian 29 Dec 2021
*/

DO $$
DECLARE

BEGIN

	DELETE FROM public.case;
	
END;
$$