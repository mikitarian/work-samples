name: Terraform Deploy

on:
  workflow_call:  # including all inputs from deployOnDemand.yml/workflow_dispatch, even if not currently used by this workflow
    inputs:
      target_aws_region:
        description: 'Target AWS Region to deploy to'
        required: true
        default: us-gov-east-1
        type: string
      target_environment:
        description: 'Target Environment to deploy to.'
        required: true
        default: dev
        type: string
      custom_image_tag:
        description: 'Custom Tag for Build Artifacts. (optional)'
        required: false
        default: latest
        type: string
      web_ui_source_zip:
        description: 'Name of .zip file of Web UI deployment artifact.'
        default: app-name-source.zip
        required: false
        type: string
      bucket_public_ui:
        description: 'Public AWS S3 bucket where UI is served'
        default: dev-ui.mydomain.net
        required: false
        type: string
      bucket_private:
        description: 'Private AWS S3 bucket where artifacts are stored'
        default: app-name-artifacts
        required: false
        type: string

env:
  # env variables hold default values used by github "push" event

  # input variables - should match default values in "workflow_call" event above
  TARGET_AWS_REGION: us-gov-east-1
  IMAGE_TAG: latest
  TARGET_ENVIROMENT: dev                          # this value equates to Terraform <env>.tfvars files for
                                                  # dev, test, staging, prod, carmy_dev, camo_dev, etc

jobs:
  deployCluster:
    name: 'Deploy via Terraform init, validate and apply commands'
    runs-on: ubuntu-latest
    defaults:
      run:
        working-directory: ./terraform/deploy
    steps:
      - name: Checkout Workspace
        uses: actions/checkout@v3   # checks-out repo under $GITHUB_WORKSPACE

      - name: Setup Terraform CLI
        uses: hashicorp/setup-terraform@v1  # Sets up Terraform CLI in GitHub Actions workflow

      ##### Begin update of GITHUB_ENV and Terraform "-var" variables #####

      - name: set-values  # inputs.<var_name> are available for both workflow_call and workflow_dispatch events
        id: set-values
        run: |
          if [[ "${{ github.event_name }}" == "push" ]]
          then
            tgt_region="${{ env.TARGET_AWS_REGION }}"
            image_tag="${{ env.IMAGE_TAG }}"
            image_tag=$(echo $image_tag | tr -d '/')
            tgt_env="${{ env.TARGET_ENVIROMENT }}"
          else
            tgt_region="${{ inputs.target_aws_region }}"
            image_tag="${{ inputs.custom_image_tag }}"
            tgt_env="${{ inputs.target_environment }}"
          fi
          echo "final-target-aws-region=$tgt_region" >> $GITHUB_OUTPUT
          echo "TARGET_AWS_REGION=$(echo $tgt_region)" >> $GITHUB_ENV
          echo "final-image-tag=$image_tag" >> $GITHUB_OUTPUT
          echo "final-target-environment=$tgt_env" >> $GITHUB_OUTPUT

      ##### End update of GITHUB_ENV and Terraform "-var" variables #####

      - name: Set AWS credentials
        uses: aws-actions/configure-aws-credentials@v1    # Configure AWS credential environment variables for use in other GitHub Actions
                                                          # secrets.<var-name> are passed from web-ui.yml and this file's workflow_call trigger receives them
        with:
          aws-access-key-id: ${{ secrets.AWS_ACCESS_KEY_ID }}   # stored in GitHub Secrets
          aws-secret-access-key: ${{ secrets.AWS_SECRET_ACCESS_KEY }}   # stored in GitHub Secrets
          aws-region: ${{ env.TARGET_AWS_REGION }}  # set in env: section above

      - name: Terraform Init
        id: init
        run: terraform init
      - name: Terraform Init Result
        run: echo "Terraform Init outcome is ${{ steps.init.outcome }}"
      - name: Terraform Apply
        id: launch
        run: bash ./run_terraform.sh ${{ steps.set-values.outputs.final-target-environment }} apply -lock-timeout=5m -input=false -auto-approve -var="image_tag=${{ steps.set-values.outputs.final-image-tag }}"

      - name: Roll ECS Services
        id: roll_ecs
        run: bash ./roll_ecs.sh ${{ steps.set-values.outputs.final-target-environment }}
