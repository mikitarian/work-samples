### AppDynamics Points of Contact and License Info

Information about our account with AppDynamics (Cisco) is available here: [https://accounts.appdynamics.com](https://accounts.appdynamics.com)

If you cannot create an account on your own, see Ryan Seelinger or Justin Martin for assistance.

#### Account and POCs:

**Account Name:**  USMC - MLB

**Phone:**  843-766-4437

**Website:**  https://www.marcorsyscom.marines.mil/PEOs/PEO-MLB/PM-LI2S/GCSS-MC-Organizational-Chart

**Address:**  2200 Lester Street, Quantico, VA 22554

**Customer Success Contact:**  Milena Clavijo Julio, mclavijo@appdynamics.com

**Sales Contact:**  Jeanne Falick, jfalick@appdynamics.com

**Technical Contacts:**

* Bryce Dickinson, brycdick@cisco.com
* John Helter, jhelter@cisco.com
* Wayne Jones II, wjonesii@cisco.com

#### License Info:

The license data below can be saved to a file named license.lic

Apply the license by copying the file to the Controller installation directory (typically in {controller_home}/platform/product/controller/)

**License:**

meta:
version: 3.0.2
create-date: 2023-12-22T14:55:15Z
license:
id: 170325690076NMP
expiration-date: 2024-03-15T00:00:00Z
customer:
id: cust
name: Appdynamics, Inc.
email: john.helter@appdynamics.com
type: ONPREM-CONTROLLER
hardware-fingerprint: ANY
environment: PROD
packages:

- package: ENTERPRISE
  create-date: 2023-12-22T00:00:00Z
  start-date: 2023-12-22T00:00:00Z
  expiration-date: 2024-03-15T00:00:00Z
  type: TRIAL
  license-units: 64
  properties:
  dataRetentionDays: 8
- package: RUM_PEAK
  create-date: 2023-12-22T00:00:00Z
  start-date: 2023-12-22T00:00:00Z
  expiration-date: 2024-03-15T00:00:00Z
  type: TRIAL
  license-units: 2
  properties:
  dataRetentionDays: 8
  eum-license-type: EUM_PRO
  eum-unified-license-allows-overages: "false"
  eum-unified-license-max-units: 2
  mobile-license-type: MOBILE_PRO
- package: SYNTHETIC_HOSTED_PRO
  create-date: 2023-12-22T00:00:00Z
  start-date: 2023-12-22T00:00:00Z
  expiration-date: 2024-03-15T00:00:00Z
  type: TRIAL
  license-units: 5
  properties: {}
- package: PREMIUM
  create-date: 2023-12-22T00:00:00Z
  start-date: 2023-12-22T00:00:00Z
  expiration-date: 2024-03-15T00:00:00Z
  type: TRIAL
  license-units: 32
  properties: {}
- package: LOG_ANALYTICS_PRO
  create-date: 2023-12-22T00:00:00Z
  start-date: 2023-12-22T00:00:00Z
  expiration-date: 2024-03-15T00:00:00Z
  type: TRIAL
  license-units: 2
  properties:
  dataRetentionDays: 8
  license-unit-ratios: []
  properties:
  eum-account-name: test-eum-account-seelingercaci-1703256694917
  eum-deployment: onprem
  eum-license-key: 0a231b8f-89d7-49e7-9b7f-b13c51791db8

---

encryption:
algorithm: None
signature:
algorithm: SHA256withRSA
value: aa6a67ba8f4e12bc6a037a4bc6dc7a72d1ea871323ec72d83b0f74a296926015a7c0a36f7afdee2629c0531de5071bb922344c0ccd0f21f2e47883ca5a96af4380c0a76989c240fe6ff89ae61e5d2c0760090f03dab7ca6196ff0b469ee325e0c6ffc6a3fb7ec5aa43c306cda405c5163107ed8c8378ef1d183b2a01bcdca3e7625cfa13ff7f64179f5c3250c798da21cff58b9fdbaf527441839016b9198d00eebc7fc14cd4ec954e3cc27e88c487a28d9777843a1f40b188569c2573416f238e5b8d58c7da7a941154b271a9deb5efd4db2b325337e927ba109dfa5776fa1057180b72edef7b142923a0d529958960beb6ef5543199d58e7f7048925752acd9876f202120f0fb04f2eb59dab93e519ad1ff0d521c4d8b28e58568ae346fc4b5ea67140dc31db99cc8549947536d5255b1f76c638de9c85f64f781edff12d7cbed82ba0fa05fe16f490ff25c02450ce221670965039c3424d0c4b0250a55c87517f9e6831aae833e2df9be7123ae2bee07ad9d7ad7447f03528cfea0d270be1f95ab8e8b7a4d1d682632d98de8fc5233fe6a941bab9fb19479215412f2599e3ed5cf32eb2b798ae3230b20ea4ebf5ec134dc41928f038e55a980671ef0e35285761cc6c9adf68599e38282fafa597a40284501deb15b3a4775f80a62f23b8d5aa9ccaa427472baabad96bc83ca8f7d4d8c4a27975dbccf2781344d6b3d04d62
