# CloudManager Basics

The configuration within this directory tracks account configuration that may or may not be required for application deployments. For example, the creation of a VPC for the development account may not be neccesary in customer accounts that provide their own VPC. Additional examples include IAM roles or other resources necessary for development activities, but not part of the application itself.

THe soft coupling between these resources (e.g. VPC, subnets, etc) is preferred over hard couplilng so we can easily install into environments with VPCs provided without any code changes.

### Future Enhancements
* Separate out Secrets access between prod and non-prod

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_ecs_execution_role"></a> [ecs\_execution\_role](#module\_ecs\_execution\_role) | ../modules/aws-iam-role | n/a |
| <a name="module_vpc"></a> [vpc](#module\_vpc) | ../community-modules/terraform-aws-vpc | n/a |
| <a name="module_vpc_endpoints"></a> [vpc\_endpoints](#module\_vpc\_endpoints) | ../community-modules/terraform-aws-vpc/modules/vpc-endpoints | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_partition.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/partition) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

No inputs.

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_iam_role_ecs_execution_arn"></a> [iam\_role\_ecs\_execution\_arn](#output\_iam\_role\_ecs\_execution\_arn) | The ARN of the ECS execution role |
| <a name="output_private_subnets"></a> [private\_subnets](#output\_private\_subnets) | The list of private subnets within the VPC |
| <a name="output_public_subnets"></a> [public\_subnets](#output\_public\_subnets) | The list of public subnets within the VPC |
| <a name="output_vpc_cidr_block"></a> [vpc\_cidr\_block](#output\_vpc\_cidr\_block) | The CIDR of the VPC |
| <a name="output_vpc_id"></a> [vpc\_id](#output\_vpc\_id) | The ID of the VPC |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->