#!/bin/bash

set -e

# Roll the ECS services to pickup new images
# Example: ./roll_ecs.sh dev

ENVIRONMENT=$1

usage() {
    echo ""
    echo "Roll the ECS services to pickup new images"
    echo "./roll_ecs.sh dev plan"
    echo ""
}

if [ -z "$ENVIRONMENT" ]; then
    echo "Must provide environment name (matches to ECS cluster name)"
    usage
    exit 1
fi

aws ecs update-service --cluster cm-${ENVIRONMENT} --service purchase-request-server --force-new-deployment
aws ecs update-service --cluster cm-${ENVIRONMENT} --service auth-server --force-new-deployment
aws ecs update-service --cluster cm-${ENVIRONMENT} --service api-gateway --force-new-deployment
aws ecs update-service --cluster cm-${ENVIRONMENT} --service app-server --force-new-deployment
