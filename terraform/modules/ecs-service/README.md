# ecs-service

This module creates a simple task definition and ECS service running on FARGATE.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_ecs_service.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_service) | resource |
| [aws_ecs_task_definition.task](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition) | resource |
| [aws_service_discovery_service.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/service_discovery_service) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_container_cpu"></a> [container\_cpu](#input\_container\_cpu) | CPU Units of container being run.  See the AWS AMI table for valid values | `number` | `512` | no |
| <a name="input_container_is_essential"></a> [container\_is\_essential](#input\_container\_is\_essential) | Memory/RAM size of container being run.  See the AWS AMI table for valid values | `bool` | `true` | no |
| <a name="input_container_memory"></a> [container\_memory](#input\_container\_memory) | Memory/RAM size of container being run.  See the AWS AMI table for valid values | `number` | `2048` | no |
| <a name="input_container_ports"></a> [container\_ports](#input\_container\_ports) | List of container ports to expose | `list(number)` | n/a | yes |
| <a name="input_desired_count"></a> [desired\_count](#input\_desired\_count) | Number of tasks to run for the service | `number` | `1` | no |
| <a name="input_ecs_cluster"></a> [ecs\_cluster](#input\_ecs\_cluster) | The ECS cluster to execute the service and task | `string` | n/a | yes |
| <a name="input_environment_variables"></a> [environment\_variables](#input\_environment\_variables) | List of environment varialbes provided to container | `list(map(string))` | `[]` | no |
| <a name="input_execution_role_arn"></a> [execution\_role\_arn](#input\_execution\_role\_arn) | ARN of the AWS IAM Role that will execute the aws\_ecs\_task\_definition | `string` | n/a | yes |
| <a name="input_health_check_command"></a> [health\_check\_command](#input\_health\_check\_command) | The command to run within the container to determine if its healthy | `list(string)` | n/a | yes |
| <a name="input_image_tag"></a> [image\_tag](#input\_image\_tag) | Tag of the image being used.  This gets concatenated with var.repository\_uri, above. | `string` | `"latest"` | no |
| <a name="input_load_balancer_blocks"></a> [load\_balancer\_blocks](#input\_load\_balancer\_blocks) | List of load balancers to associate service with | `list(map(any))` | `[]` | no |
| <a name="input_log_group"></a> [log\_group](#input\_log\_group) | Name of CloudWatch log group to write logs | `string` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | Region to write logs in CloudWatch | `string` | n/a | yes |
| <a name="input_repository_uri"></a> [repository\_uri](#input\_repository\_uri) | URI of the Repository that holds the image to be used | `string` | n/a | yes |
| <a name="input_secrets"></a> [secrets](#input\_secrets) | List of secrets (valueFrom ARN) provided to container | `list(map(string))` | `[]` | no |
| <a name="input_security_groups"></a> [security\_groups](#input\_security\_groups) | Security groups used by the ECS service | `list(string)` | `[]` | no |
| <a name="input_service_discovery_namespace_id"></a> [service\_discovery\_namespace\_id](#input\_service\_discovery\_namespace\_id) | The ID of the service discovery namespace | `string` | n/a | yes |
| <a name="input_service_name"></a> [service\_name](#input\_service\_name) | Name to be used on to identify the service | `string` | n/a | yes |
| <a name="input_subnets"></a> [subnets](#input\_subnets) | Subnets used by the ECS service | `list(string)` | n/a | yes |
| <a name="input_task_name"></a> [task\_name](#input\_task\_name) | Name to be used to identify the task | `string` | n/a | yes |
| <a name="input_task_role_arn"></a> [task\_role\_arn](#input\_task\_role\_arn) | ARN of the AWS IAM Role that task execution will use | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->