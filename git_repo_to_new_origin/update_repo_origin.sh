#!/bin/bash

# This script will run the `git remote set-url origin` command
# on the list of repositories in the `repos_to_update` array
# 
# It will allow the user to:
#     preview the list of repos to be updated
#     update or revert those repos
# 
# To use, place this script in the directory where
# the user's local repositories are stored
# then run the script in a terminal window that supports bash scripts
# 
# Sam Mikitarian
# 02 June 2022


# declare arrays
declare a repos_to_update=("cloudtracker-auditor" "CloudTracker-Java" "ct-bootstrap-pipeline-library" 
                            "eaads-devops" "eaads-pipeline-library" "EAADS-Scripts" "eaads-serenity-qa"
                             "eaads-ui" "EAADS-Utils" "AccessIT-Mock" "CloudTracker-push" "CloudTracker-merged")

declare a directory_does_not_exist=()

declare a directory_is_not_repo=()

declare a updated_repos=()

declare a unsuccessful_update_repos=()

# begin update/revert
loop_through_repos(){
    
    command=$1 && shift
    git_target=$1 && shift
	arr=("$@")

    # arr=("$@") is the "${repos_to_update[@]}" array after being passed into the function from case statement below"

    # shift (see above) is a builtin command in bash which after getting executed, shifts/moves 
    # the command line arguments to one position left. The first argument is lost after using shift command. 
    # This command takes only one integer as an argument. This command is useful when you 
    # want to get rid of the command line arguments which are not needed after parsing them.

    count=0

    clear

    if [ $command == "List" ]
    then
        printf "\n Repos to be updated by this script are...\n\n"
    elif [ $command == "Update" ]
    then
        printf "\n UPDATING GitHub origins to \"$git_target\" for the following repos...\n\n"
    elif [ $command == "Revert" ]
    then
        printf "\n REVERTING GitHub origins to \"$git_target\" for the following repos...\n\n"
    fi

	for r in "${arr[@]}";
	do
        
        let count++
        printf "\t$r \n"

        if [ $command != "List" ]
        then
            if [ ! -d "./$r" ]
            then
                directory_does_not_exist+=($r)
            elif [ -d "./$r" ] && [ ! -f "./$r/.git/config" ]
            then
                directory_is_not_repo+=($r)
            elif [ -d "./$r" ] && [ -f "./$r/.git/config" ]
            then
                cd $r
                result=$(git remote set-url origin git@github.com:$git_target/$r.git 2>&1 )

                if [ ${#result} -gt 0 ]
                then
                    unsuccessful_update_repos+=($r)
                else
                    updated_repos+=($r)
                fi
                cd ..
            fi
		fi

        sleep 0.5

		if [ $count -eq ${#arr[@]} ] && [ $command != "List" ]
		then

            if [ $command == "Update" ]
            then
                action="updated"
            elif [ $command == "Revert" ]
            then
                action="reverted"
            fi
            ur_len=${#updated_repos[@]}
            uur_len=${#unsuccessful_update_repos[@]}
            dnr_len=${#directory_is_not_repo[@]}
            ddne_len=${#directory_does_not_exist[@]}

            printf "\n\n RESULTS:\n"

            if [ $ur_len -gt 0 ] || [ $uur_len -gt 0 ]
            then
                if [ $ur_len -gt 0 ]
                then
                    printf "\n GitHub origins were $action to \"$git_target\" for the following repos in this directory:\n\n"
                    for r in "${updated_repos[@]}";
                    do
                        printf "\t$r\n"
                    done
                fi

                if [ $uur_len -gt 0 ]
                then
                    printf "\n A GitHub \"origin update\" to \"$git_target\" was ATTEMPTED but FAILED for the following repos in this directory:\n\n"
                    for r in "${unsuccessful_update_repos[@]}";
                    do
                        printf "\t$r\n"
                    done
                    printf "\n Please run \"git remote set-url origin git@github.com:$git_target/<repo_name>.git\" manually\n\n"
                fi

                if [ $dnr_len -gt 0 ]
                then
                    printf "\n The following directories were found, but they were NOT Git Repositories"
                    printf "\n and therefore were not $action:\n\n"
                    for r in "${directory_is_not_repo[@]}";
                    do
                        printf "\t$r\n"
                    done
                fi

            else
                printf "\n NO repos were $action by this script.\n\n"
            fi

            if [ $dnr_len -gt 0 ]
            then
                printf "\n The following repos were not found in this directory,"
                printf "\n and therefore were not $action:\n\n"
                for r in "${directory_does_not_exist[@]}";
                do
                    printf "\t$r\n"
                done
            fi

            printf "\n\n"

        elif [ $count -eq ${#arr[@]} ] && [ $command == "List" ]
        then
            printf "\n\n"
		fi

	done

}

quit_script(){
	printf "\n\n\tQuitting update-repo-origins.sh\n\n"
	exit 0
}

main_menu()
{
	clear
	printf "\nThis script will update your local Git repository origins"
	printf "\nfrom Berico-Technologies to Novetta"
	printf "\n"
    printf "\nChoose your next action:"
	printf "\n     -List repositories to be updated (l)"
    printf "\n     -Update repositories to git@github.com:Novetta (u)"
    printf "\n     -Revert repositories to git@github.com:Berico-Technologies (r)"
	printf "\n     -Quit this script (q)"
	printf "\n"
	read -n1 -p "Enter your choice here: " decision
		case "$decision" in
			l)
                
				loop_through_repos "List" "none" "${repos_to_update[@]}"
				;;
			u)
                
				loop_through_repos "Update" "Novetta" "${repos_to_update[@]}"
				;;
			r)
                
				loop_through_repos "Revert" "Berico-Technologies" "${repos_to_update[@]}"
				;;
			q)
				quit_script
				;;
			*)
				printf "\n\n***********************************************************************************************************"
				printf "\n\nValid inputs are \"l\", \"u\", \"r\" and \"q\"."
				printf "\n\nAnything else will bring you to this message."
				printf "\n\nIf those inputs are not being accepted"
                printf "\ncontact Sam Mikitarian, or anyone else on the DevOps Team, for help.\n\n"
				printf "\n***********************************************************************************************************\n"
				;;
		esac
}

main_menu
