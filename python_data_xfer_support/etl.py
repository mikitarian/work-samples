import pyodbc

from variables import datawarehouse_name, old_s3_endpoint, new_s3_endpoint


def etl(query, source_cnx, target_cnx):
    """
    The Extract/Transform/Load Method will take a pair of queries along with a source and target destination
    And transform from one DB to the other.   This is a very "one off" solution for the 1x baseline
    And in the future a better solution can be used to migrate the FENCES customer over when we know the rule sets better

    For now, we can use this and improve over time.
    :param query:  This is the pair of queries
    :param source_cnx: the source context (where it came from)
    :param target_cnx: the target context (where its going)
    :return: we don't return yet, but we could
    """

    # extract data from source db
    source_cursor = source_cnx.cursor()
    # We want to store both the query name and the table name to more easily determine mappings
    sql_query = query.extract_query["query"]
    source_cursor.execute(sql_query)
    # Grab all the data (this is only good for SMALL data sets)
    data = source_cursor.fetchall()
    # Close the cursor now that we have it
    source_cursor.close()

    # load data into warehouse db
    if data:
        transformed_data = []
        target_cursor = target_cnx.cursor()
        target_cursor.execute("USE {}".format(datawarehouse_name))

        if query.extract_query["tableName"] == "PurchaseAgreement":
            for row in data:
                row[0] = row[0].replace("-", "")
                row[7] = transform_status_to_int(row[7])
                transformed_data.append(row)
        elif query.extract_query["tableName"] == "purchaseRequests":
            """
            Some weird mapping needs to take place on this one
            Convert the status from a string to an int (enum)
            Determine what text they had in the pop and determine if it's 12 months (1) 24 months (2)
            or something we cannot make sense of (0)
            """
            # We need some custom mapping the status needs to be updated from a NVARCHAR  to an INT
            for row in data:
                row[0] = row[0].replace("-", "")
                pop_val = transform_pop_to_int(row[5])
                inc_funded = row[6] == 1
                fully_funded = row[7] == 1
                if inc_funded or not fully_funded:
                    inc_funded = 1
                else:
                    inc_funded = 0

                row[15] = transform_status_to_int(row[15])
                # We no longer care about POP 12/24 months nor about the fully_incremented from 1x
                starting_items = row[0:5]
                last_six = row[-6:]

                new_row = [*row[0:5], # Rows 1-5 (Up to Customer Agr)
                           -1, -1, #Then the Period of Performance
                           inc_funded, row[8], #increment and is assert   Row 9
                           -1, # single ALIN per pop (row 10)
                           row[9], #Is host responsible for spending (row 11)
                           row[10], #Is ECMA OVERRUN (row12)
                           -1,	# Is novetta contact poc (row 13)
                            *last_six]
                transformed_data.append(new_row)
        elif query.extract_query["tableName"] == "FundingAllocation":
            for row in data:
                # Funding allocation is weird because the PK is just an ID
                row[3] = transform_color_of_money(row[3])
                row[4] = row[4].replace("-", "")
                new_row = row[1:]
                transformed_data.append(new_row)
        elif query.extract_query["tableName"] == "camoRomForms":
            for row in data:
                row[0] = row[0].replace("-", "")
                row[4] = transform_status_to_int(row[4])
                new_row = [row[0], 1, *row[1:]]
                transformed_data.append(new_row)
        elif query.extract_query["tableName"] == "purchaseForms":
            # In this example, we have a 1:1 mapping except for the 2nd column
            # And because this is a PR FORM its value is 2
            for row in data:
                row[0] = row[0].replace("-", "")
                row[4] = transform_status_to_int(row[4])
                new_row = [row[0], 2, *row[1:]]
                transformed_data.append(new_row)
        elif query.extract_query["tableName"] == "purchaseBundles":
            for row in data:
                row[0] = row[0].replace("-", "")
                if row[1] is not None:
                    row[1] = row[1].replace(old_s3_endpoint, new_s3_endpoint) # PR Agreement
                row[2] = transform_status_to_int(row[2])
                if row[3] is not None:
                    row[3] = row[3].replace(old_s3_endpoint, new_s3_endpoint) #PR
                row[4] = transform_status_to_int(row[4])
                if row[5] is not None:
                    row[5] = row[5].replace(old_s3_endpoint, new_s3_endpoint) # PR Upload
                if row[6] is not None:
                    row[6] = row[6].replace(old_s3_endpoint, new_s3_endpoint) # fourth form
                row[7] = transform_status_to_int(row[7])
                row[8] = transform_status_to_int(row[8])
                # Because this is a tuple, we need to get creative on how to add that extra column at the end
                new_data = [r for r in row]
                new_data.append(None)
                transformed_data.append(new_data)
        else:
            print("Unknown data type %s", query.extract_query["tableName"])

        print("Moving data from %s" , query.extract_query["tableName"])
        try:
            target_cursor.executemany(query.load_query, transformed_data)
        except Exception as ex:
            print('data loaded to target destination %s', ex)
        target_cursor.close()
    else:
        print('No data to extract')


def transform_status_to_int(status):
    """
    Takes a status from 1x (string version) and swaps it to the enum value for dev
    :param status:  Text status from the 1X DB
    :return: enum status
    """
    if status == "Submitted":
        return 1
    elif status == "Approved":
        return 2
    elif status == "Rejected":
        return 3
    elif status == "Archived":
        return 4
    elif status == "Finalized":
        return 5
    else:
        return None


def transform_color_of_money(val):
    """
    Transform a previously known Color of money (from text)
    And convert to the new enumeration val.  If the user entered something else
    then we'll convert it to OTHER (5)
    :param val: the string in the 1x DB
    :return: The enum val
    """
    if val == "OMA":
        return 1
    elif val == "OPA":
        return 2
    elif val == "RDTE":
        return 3
    elif val == "ACWF":
        return 4
    else:
        return 5 #5 designates other


def transform_pop_to_int(pop_val):
    """
    Read the old data from the 1X DB and see if we can make sense of it makes sense
    :param pop_val:  The Period of Performance from 1x as a string
    :return: The enumeration val
    """
    if "12" in pop_val:
        return 1
    elif "24" in pop_val:
        return 2
    else:
        return None


def etl_process(queries, target_cnx, source_db_config, db_platform):
    """
    Take a list of queries - send the data to the target context and pull from the source db
    We'll only be interested in Microsoft SQL ta the moment
    :param queries: our list of tuples
    :param target_cnx:  the target (where it's going)
    :param source_db_config:  the source (where it came from)
    :param db_platform:  The DB type
    :return:
    """
    if db_platform == 'sqlserver':
        source_cnx = pyodbc.connect(**source_db_config)
    else:
        return 'Error! unrecognised db platform'

    # loop through sql queries
    for query in queries:
        etl(query, source_cnx, target_cnx)

    # close the source db connection
    source_cnx.close()
