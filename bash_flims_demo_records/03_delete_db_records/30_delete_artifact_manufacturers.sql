/* DELETE records from artifact_manufacturer TABLE
	
	Sam Mikitarian 04 Jan 2022
*/

DO $$
DECLARE

BEGIN

	DELETE FROM artifact_manufacturer;
	
END;
$$