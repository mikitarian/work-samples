/*	DELETE records from role TABLE

	Sam Mikitarian 04 Jan 2022
*/

DO $$
DECLARE

	manage_all_role_id integer := (SELECT id FROM role WHERE UPPER(name)='MANAGE_ALL');

BEGIN
	
	-- DELETE all roleId/permissionId combinations pertaining to the MANAGE_ALL role
	DELETE FROM role_permissions_permission WHERE "roleId"=manage_all_role_id;
	
	-- DELETE the MANAGE_ALL role;
	DELETE FROM role WHERE "id"=manage_all_role_id;
	
END;
$$