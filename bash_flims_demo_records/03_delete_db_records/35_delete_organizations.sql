/* DELETE records from organization TABLE
	
	Sam Mikitarian 04 Jan 2022
*/

DO $$
DECLARE

BEGIN

	DELETE FROM organization;
	
END;
$$