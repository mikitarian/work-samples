from app_defaults import *

# these are kivy classes defined in ntctaskgrid.kv

class NtcTaskHeaderLabel(Label):
	pass
	
class NtcTaskLabel(Label):
	pass

class NtcTaskStatusButton(Button):
	pass

class NtcTaskEditButton(Button):
	pass

class NtcTaskDuplicateButton(Button):
	pass

class NtcTaskDeleteButton(Button):
	pass