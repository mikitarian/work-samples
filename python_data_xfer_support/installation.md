## INSTALLATION PROCEDURES:

The data transfer python scripts were originally designed to be run from
a Windows machine.

However they can also be run from a \*nix machine. To do so, edit the
**db_credentials.py** file and replace the driver value of **'{SQL Server}'**
with **'{ODBC Driver 17 for SQL Server}'**.

This linux (WSL/Ubuntu) approach has been tested and proven in the AFS
environment, and the Windows approach has been proven in the FENCES
customer environment as well as the AFS environment.

### Steps
(Windows example shown, but linux steps are the same. Just remember to update the **ODBC Driver** value first, as mentioned above.)

#### Step 1  
**Get the data transfer scripts** Copy the data-mgmt.zip file from **s3://cloudmanager-artifacts/acmp-1x-dev-xfer/data-mgmt.zip** to the target machine  

#### Step 2  
**Unzip** Unzip the file to the desired location, for example:  

    D:\data-mgmt\CAMO_ETL

#### Step 3  
**Python Setup** Run the following commands from a Windows CMD prompt.  

Ensure Python3.x is installed

    python3 -V

Install pyodbc library

    python -m pip install pyodbc

Result should be something like this:

    Collecting pyodbc
      Downloading pyodbc-4.0.39-cp39-cp39-win_amd64.whl (69 kB)
         ---------------------------------------- 69.8/69.8 KB 1.3 MB/s eta 0:00:00
    Installing collected packages: pyodbc
    Successfully installed pyodbc-4.0.39

#### Step 4
**Database Settings** In a text edior, IDE or from the command line, edit the **db_credentials.py** file.

Provide the server, database, user and password values for the **datawarehouse_db_config** and **sqlserver_db_config** configurations.  

format:

      'server': '<server>',
      'database': '<db_name>',
      'user': '<db_user>',
      'password': '<db_password>',

**NOTE:**  If you need to specify a **database port number**, the format is as follows:

      'database': '<db_name,port_number>',
      eg:
      'database': 'proxy.cm2.cloudtracker.net,8383',

      This is useful if the DB is running on a non-standard port, or is protected behind a firewall.


example:  
(remember to use the driver that is compatible with your OS.  Windows=SQL Server, Linux=ODBC Driver 17 for SQL Server)

      # WHERE WE'RE SENDING THE DATA
    datawarehouse_db_config = {
      'driver': '{SQL Server}',
      'server': 'proxy.cm2.cloudtracker.net',
      'database': 'CM_TEST_INSTALL',
      'user': 'cloud_manager',
      'password': 'db_password_in_plain_text',
          'autocommit': True,
    }

      # SOURCE_DB - Where we're pulling the initial data from
    sqlserver_db_config = [
      {
      'driver': '{SQL Server}',
      'server': 'devdb.cm.cloudtracker.net',
      'database': 'PR-20221221-1',
      'user': 'ecmauser',
      'password': 'db_password_in_plain_text',

          'autocommit': True,
    }

#### Step 5
**Set Variables** In a text edior, IDE or from the command line, edit the **variables.py** file as follows...

**Datawarehouse Name**  
Set the **datawarehouse_name** to match the database name found in **db_credentials.py -\> datawarehouse_db_config**.

example:

     datawarehouse_name = 'CM_TEST_INSTALL'


**S3 Bucket Settings:**  
Set the **old_s3_endpoint** to the name of the S3 bucket where ACMP 1.x Purchase Request documents are currently stored.  
Set the **new_s3_endpoint** to the name of the S3 bucket where ACMP 3.x Purchase Request documents will be stored after data migration.

example:

     old_s3_endpoint = "acmp1-dev-purchase-requests.s3.us-gov-west-1.amazonaws.com"
     new_s3_endpoint = "acmp-20-staging.s3-us-gov-east-1.amazonaws.com"

#### Step 6  
**Run the Scripts** From the Windows CMD prompt, cd to the directory containing the data transfer files

    D:
    cd D:\data-mgmt\CAMO_ETL

Run the data transfer

    C:\devwin\CloudManager\tools\data-mgmt\CAMO_ETL>python3 main.py

Result should be something like this:

    loading db: PR-20221221-1
    Moving data from %s PurchaseAgreement
    Moving data from %s purchaseRequests
    Moving data from %s purchaseForms
    Moving data from %s camoRomForms
    Moving data from %s purchaseBundles
    Moving data from %s FundingAllocation

If you have a database management tool, such as DBeaver or SQL Server Management Studio, you can check the table contents directly.

Or you can log in to the target web site to see if the data is present.

### Document Transfer  
Once the DB has been migrated you'll need to s3 sync the two buckets so the customer's Purchase Request documents move from the old S3 bucket to the new one.  
Use the **old_s3_endpoint** and **new_s3_endpoint** values entered in variables.py in the **aws s3 sync** command below.  

    aws s3 sync s3://old_s3_endpoint s3://new_s3_endpoint  
    eg:  
    aws s3 sync s3://acmp1-dev-purchase-requests s3://cm-test-purchase-requests  


### Helpful SQL Queries
As part of the data migration you may need to manually update S3 Bucket values in the target tables.  Assuming you have access to a DB management tool like DBeaver (or SQL access at the command line), here are some queries that will get the job done:

    UPDATE <db_name>.dbo.PurchaseRequests
    SET SignedUrl = Replace(SignedUrl,'old_s3_endpoint','new_s3_endpoint');

    UPDATE <db_name>.dbo.PurchaseAgreement 
    SET SignedUrl = Replace(SignedUrl,'old_s3_endpoint','new_s3_endpoint');

    UPDATE <db_name>.dbo.PurchaseBundles 
    SET 
        PurchaseAgreement = Replace(PurchaseAgreement,'old_s3_endpoint','new_s3_endpoint'),
        PurchaseRequest = Replace(PurchaseRequest,'old_s3_endpoint','new_s3_endpoint'),
        PurchaseUploadForm = Replace(PurchaseUploadForm,'old_s3_endpoint','new_s3_endpoint'),
        FourthFormUploadForm = Replace(FourthFormUploadForm,'old_s3_endpoint', 'new_s3_endpoint');
