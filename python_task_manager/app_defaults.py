import kivy
from kivy.config import Config

# disable touch screen emulation since we're not on mobile device
# this allows right-click of text selection for copy/paste
Config.set('input', 'mouse', 'mouse,disable_multitouch')

# disable screen resizing, it just makes the layout look bad.
Config.set('graphics', 'resizable', False)
Config.set('graphics', 'position', 'custom')
Config.set('graphics', 'left', 400)
Config.set('graphics', 'top',  50)

from kivy.app import App
from kivy.core.window import Window

from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.scrollview import ScrollView
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.uix.slider import Slider
from kivy.uix.spinner import Spinner, SpinnerOption
from kivy.uix.dropdown import DropDown
from kivy.uix.textinput import TextInput
from kivy.uix.filechooser import FileChooserListView
from kivy.uix.checkbox import CheckBox

import os, sys
import re
import iptools, time
import json
from functools import partial
from pathlib import Path
from sys import platform

# set up application logging
logpath = "C:\\ntclogs\\"
if os.path.isdir(logpath)==False:
	os.mkdir(logpath, 755);

logfile = Path(os.path.join("C:\\ntclogs",'ntclogfile.txt'))

try:
	os.environment['KIVY_GL_BACKEND'] = 'angle_sdl1'
	Config.set('graphics', 'multisamples', '0')
except:
	# pass
	e = sys.exc_info()[0]
	if logfile.is_file() == False:
		f = open(logfile,'a')
		f.close()
	with open(logfile, "a") as myfile:
	    myfile.write("***ERROR***\t%s" % e + "\n")
else:
	# pass
	with open(logfile, "a") as myfile:
	    myfile.write("NO ERROR in the KIVY_GL_BACKEND try/catch\n")

kivy.require('1.11.0')

Builder.load_file('./views/ntcmain.kv')
Builder.load_file('./views/task_grid/ntctaskgrid.kv')

#init global variables
ntc_controllers_directory = Path("controllers")
ntc_data_model_directory = Path("data_model")
ntc_task_list_directory = Path("tasklists")
# target_directories_file = Path("data_model/target_directories")

#ensure target files exist

#this file populates the Script Paths dropdown in the Task Edit area
script_path_file_name = Path(os.path.join(ntc_data_model_directory,'scriptpaths'))
if script_path_file_name.is_file() == False:
	f = open(script_path_file_name,'a')
	f.close()

#this file populates the IP Address dropdown in the Task Edit area
ip_address_file_name = Path(os.path.join(ntc_data_model_directory,'ipaddresses'))
if ip_address_file_name.is_file() == False:
	f = open(ip_address_file_name,'a')
	f.close()

buttons_and_controls_color = 166/255, 158/255, 176/255, 1		#rosyBrown
disabled_controls_color = 166/255, 158/255, 176/255, .25		#rosyBrownTransparent
alt_insets_and_boxes_color = 239/255, 239/255, 242/255, 1		#papayaWhip
insets_and_boxes_color = 242/255, 226/255, 205/255, 1			#wheat
default_background_color = 218/255, 218/255, 227/255, 1			#lightGray
delete_button_color = 171/255, 75/255, 82/255, 1				#redwood
green_button_color = 46/255, 139/255, 87/255, 1					#seagreen
text_color = 1,1,1,1											#white
disabled_text_color = 48/255, 48/255, 48/255, 1					#mediumJungleGreen
inverse_text_color = 0,0,0,1									#black
error_text_color = 1,0,0,1										#red
feedback_text_color =0,0,1,1									#blue
small_font_size = 10											#10 point font
normal_font_size = 13											#13 point font
large_font_size = 16											#16 point font
title_bar_font_size = 24										#24 point font

#set up window dimensions
Window.size = (900, 700)
windowWidth = Window.size[0]
windowHeight = Window.size[1]

Window.clearcolor = (default_background_color)