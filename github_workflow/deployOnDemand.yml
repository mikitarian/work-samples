name: deployOnDemand.yml

on:
  workflow_call:  # the "workflow_call" github event is required so schedule.yml can kick off this workflow
    inputs:
      target_aws_region:
        description: 'Target AWS Region to deploy to'
        required: true
        default: us-gov-east-1
        type: string
      target_environment:
        description: 'Target Environment to deploy to.'
        required: true
        default: dev
        type: string
      custom_image_tag:
        description: 'Custom Tag for Build Artifacts. (optional)'
        required: false
        default: latest
        type: string
      web_ui_source_zip:
        description: 'Name of .zip file of Web UI deployment artifact.'
        default: cm-source-code.zip
        required: false
        type: string
      bucket_public_ui:
        description: 'Public AWS S3 bucket where UI is served'
        default: dev-ui.cm2.cloudtracker.net
        required: false
        type: string
      bucket_private:
        description: 'Private AWS S3 bucket where artifacts are stored'
        default: cloudmanager-artifacts
        required: false
        type: string
  workflow_dispatch:  # the "workflow_dispatch" github event is required to manually kick off this workflow
    inputs:
      target_aws_region:
        description: 'Target AWS Region to deploy to'
        required: true
        default: 'us-gov-east-1'
        type: choice
        options:
        - us-gov-east-1
        - us-gov-west-1
        - us-east-1
        - us-east-2
        - us-west-1
        - us-west-2
      target_environment:
        description: 'Target Environment to deploy to.'
        required: true
        default: dev
        type: choice
        options:
        - dev
        - test
        - staging
        - prod
        - camo_dev
        - carmy_dev
      custom_image_tag:
        description: 'Custom Tag for Build Artifacts. (optional)'
        required: false
        default: latest
        type: string
      web_ui_source_zip:
        description: 'Name of .zip file of Web UI deployment artifact.'
        default: 'cm-source-code.zip'
        required: false
        type: string
      bucket_public_ui:
        description: 'Public AWS S3 bucket where UI is served'
        default: 'dev-ui.cm2.cloudtracker.net'
        required: false
        type: string
      bucket_private:
        description: 'Private AWS S3 bucket where artifacts are stored'
        default: 'cloudmanager-artifacts'
        required: false
        type: string

jobs:
  values-received:
    runs-on: ubuntu-latest
    outputs:
      target_aws_region: ${{ steps.set-values.outputs.target_aws_region }}
      target_environment: ${{ steps.set-values.outputs.target_environment }}
      custom_image_tag: ${{ steps.set-values.outputs.custom_image_tag }}
      web_ui_source_zip: ${{ steps.set-values.outputs.web_ui_source_zip }}
      bucket_public_ui: ${{ steps.set-values.outputs.bucket_public_ui }}
      bucket_private: ${{ steps.set-values.outputs.bucket_private }}

    steps:
      - name: meta data in cmDeployOnDemand
        id: meta-data-in-cmDeployOnDemand
        run: |
          echo "Selected branch or tag is ${{ github.ref }}"
          echo "github.event_name is ${{ github.event_name }}"

      - name: set-values  # inputs.<var_name> are available for both workflow_call and workflow_dispatch events
        id: set-values
        run: |
          tgt_region="${{ inputs.target_aws_region }}"
          tgt_env="${{ inputs.target_environment }}"
          image_tag="${{ inputs.custom_image_tag }}"
          image_tag=$(echo $image_tag | tr -d '/')
          web_zip="${{ inputs.web_ui_source_zip }}"
          b_public="${{ inputs.bucket_public_ui }}"
          b_private="${{ inputs.bucket_private }}"

          echo "target_aws_region=$tgt_region" >> $GITHUB_OUTPUT
          echo "target_environment=$tgt_env" >> $GITHUB_OUTPUT
          echo "custom_image_tag=$image_tag" >> $GITHUB_OUTPUT
          echo "web_ui_source_zip=$web_zip" >> $GITHUB_OUTPUT
          echo "bucket_public_ui=$b_public" >> $GITHUB_OUTPUT
          echo "bucket_private=$b_private" >> $GITHUB_OUTPUT

    clean_branch_name:
    uses: ./.github/workflows/r_stringTools.yml
    with:
      string_type: clean_branch_name
      string_value_one: ${{ github.ref }}

  branch_name_return:
    runs-on: ubuntu-latest
    needs: [clean_branch_name]
    steps:
      - name: show-output
        run: |
          echo "trimmed branch name is ${{ needs.clean_branch_name.outputs.trimmed_branch }}"
  
  validate_custom_image_tag:
    needs: [values-received]
    uses: ./.github/workflows/r_stringTools.yml
    with:
      string_type: custom_image_tag
      string_value_one: ${{ inputs.custom_image_tag }}
      
  custom_image_tag_return:
    runs-on: ubuntu-latest
    needs: [validate_custom_image_tag]
    steps:
      - name: show-output
        run: |
          echo ${{ needs.validate_custom_image_tag.outputs.string_is_valid }}
          echo ${{ needs.validate_custom_image_tag.outputs.feedback_msg }}
          echo ${{ needs.validate_custom_image_tag.outputs.clean_string_value_one }}
      - name: stop-or-go
        run: |
          if [[ "${{ needs.validate_custom_image_tag.outputs.string_is_valid }}" == "no" ]]
          then
            exit 1
          fi

  # ### web_ui_source_zip ### #
  validate_zip_file_name:
    needs: [values-received]
    uses: ./.github/workflows/r_stringTools.yml
    with:
      string_type: zip_file
      string_value_one: ${{ inputs.web_ui_source_zip }}
      
  zip_file_return:
    runs-on: ubuntu-latest
    needs: [validate_zip_file_name]
    steps:
      - name: show-output
        run: |
          echo ${{ needs.validate_zip_file_name.outputs.feedback_msg }}
      - name: stop-or-go
        run: |
          if [[ "${{ needs.validate_zip_file_name.outputs.string_is_valid }}" == "no" ]]
          then
            exit 1
          fi

  # ### bucket_public_ui ### #
  validate_bucket_public_ui:
    needs: [values-received]
    uses: ./.github/workflows/r_stringTools.yml
    with:
      string_type: bucket_name
      string_value_one: ${{ inputs.bucket_public_ui }}
      
  bucket_public_ui_return:
    runs-on: ubuntu-latest
    needs: [validate_bucket_public_ui]
    steps:
      - name: show-output
        run: |
          echo ${{ needs.validate_bucket_public_ui.outputs.feedback_msg }}
      - name: stop-or-go
        run: |
          if [[ "${{ needs.validate_bucket_public_ui.outputs.string_is_valid }}" == "no" ]]
          then
            exit 1
          fi

  # ### bucket_private ### #
  validate_bucket_private:
    needs: [values-received]
    uses: ./.github/workflows/r_stringTools.yml
    with:
      string_type: bucket_name
      string_value_one: ${{ inputs.bucket_private }}
      
  bucket_private_return:
    runs-on: ubuntu-latest
    needs: [validate_bucket_private]
    steps:
      - name: show-output
        run: |
          echo ${{ needs.validate_bucket_private.outputs.feedback_msg }}
      - name: stop-or-go
        run: |
          if [[ "${{ needs.validate_bucket_private.outputs.string_is_valid }}" == "no" ]]
          then
            exit 1
          fi

  run-services-workflow:
    needs: [custom_image_tag_return, zip_file_return, bucket_public_ui_return, bucket_private_return, values-received]
    name: 'Run services.yml Workflow'
    uses: ./.github/workflows/services.yml
    secrets: inherit
    with:
      target_aws_region: ${{ needs.values-received.outputs.target_aws_region }}
      target_environment: ${{ needs.values-received.outputs.target_environment }}
      custom_image_tag: ${{ needs.values-received.outputs.custom_image_tag }}
      web_ui_source_zip: ${{ needs.values-received.outputs.web_ui_source_zip }}
      bucket_public_ui: ${{ needs.values-received.outputs.bucket_public_ui }}
      bucket_private: ${{ needs.values-received.outputs.bucket_private }}


  run-web-ui-workflow:
    needs: [custom_image_tag_return, zip_file_return, bucket_public_ui_return, bucket_private_return, values-received]
    name: 'Run web-ui.yml Workflow'
    uses: ./.github/workflows/web-ui.yml
    secrets: inherit
    with:
      target_aws_region: ${{ needs.values-received.outputs.target_aws_region }}
      target_environment: ${{ needs.values-received.outputs.target_environment }}
      custom_image_tag: ${{ needs.values-received.outputs.custom_image_tag }}
      web_ui_source_zip: ${{ needs.values-received.outputs.web_ui_source_zip }}
      bucket_public_ui: ${{ needs.values-received.outputs.bucket_public_ui }}
      bucket_private: ${{ needs.values-received.outputs.bucket_private }}
