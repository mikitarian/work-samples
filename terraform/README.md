# CloudManager Terraform

This directory contains Terraform to setup and deploy CloudManager.

| Directory | Description |
| --- | --- |
| basics | Account configuration and other development resources
| community-modules | Modules pulled off the community shelf
| deploy | Resources to deploy the actual CloudManager application
| state | Bootstrap S3 and DDB for Terraform state tracking

## Terraform Workspaces

Terraform workspaces are utilized to separate the state of multiple installations within the same AWS account and S3 state bucket. For example, when installing dev, test, demo, etc. into a single account, a Terraform workspace is used to match the name. Additionally, an environment file (located in `deploy/env/<name>.tfvars`) is used with the same name. The helper script `deploy/run_terraform.sh` wraps this logic up and automatically selects the workspace as well as passing in the `var-file` reference. For example

```bash
./run_terraform.sh dev plan

./run_terraform.sh dev apply
```


## pre-commit hooks

We are utilizing the pre-commit framework to ensure linting and standardization in the Terraform code. The hooks are configured in the `.pre-commit-config.yaml` at the root of the repo. The following command allows you to run it locally using a Docker container rather than natively installing the tools.

```bash
TAG=v1.76.0; docker run -v $(pwd):/lint -w /lint ghcr.io/antonbabenko/pre-commit-terraform:${TAG} run -a
```