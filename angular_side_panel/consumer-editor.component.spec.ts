import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { RoutedTabsetDirective } from '@arsenal/directives/routed-tabset.directive';
import { SpinnerOverlayDirective } from '@arsenal/directives/spinner-overlay.directive';
import {
  loadMaterialStyles,
  mountFixture,
} from '@arsenal/testing/cypress-utils';
import { getCypressTestBed, initEnv } from 'cypress-angular-unit-test/dist';
import { MockComponent, MockDirective, MockModule } from 'ng-mocks';
import { EMPTY, of, throwError } from 'rxjs';
import { AccessVerificationService } from '@arsenal/services/access-verification.service';
import {
  ConsumersService,
  CONSUMERS_SERVICE_TOKEN,
} from '../../state/consumers.service';
import { ConsumerDetailsComponent } from '../consumer-details/consumer-details.component';
import { ConsumerViewsComponent } from '../consumer-views/consumer-views.component';
import { ConsumerEditorComponent } from './consumer-editor.component';
import { mockConsumer } from '@arsenal/features/ironsky/testing/mock-data/consumer.mock';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { FormErrorMessageDirective } from '@arsenal/directives/form-error-message.directive';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { AuthCheckDirective } from '@arsenal/directives/auth-check.directive';
import { PageSizeComponent } from '@arsenal/components/page-size/page-size.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ConsumersQuery } from '../../state/consumers.query';
import { RouteComponentStub } from '@arsenal/testing/stubs/route.stub';
import { ActivatedRoute, Router } from '@angular/router';
import { SinonStub } from 'cypress/types/sinon';

describe('ConsumerEditorComponent', () => {
  beforeEach(() => {
    const consumersQueryStub: Partial<ConsumersQuery> = {
      selectEntity: cy.stub().as('selectEntity').returns(of(mockConsumer)),
      selectLoading: cy.stub().returns(of(false)),
    };
    const consumersServiceStub: Partial<ConsumersService> = {
      get: cy.stub().as('getConsumer').returns(EMPTY),
    };
    loadMaterialStyles();
    initEnv(ConsumerEditorComponent, {
      declarations: [
        AuthCheckDirective,
        FormErrorMessageDirective,
        RouteComponentStub,
        MockComponent(ConsumerDetailsComponent),
        MockComponent(ConsumerViewsComponent),
        MockComponent(PageSizeComponent),
        MockDirective(SpinnerOverlayDirective),
        RoutedTabsetDirective,
      ],
      imports: [
        MatCardModule,
        MockModule(NgxDatatableModule),
        ReactiveFormsModule,
        MatFormFieldModule,
        MatIconModule,
        MatSlideToggleModule,
        MatTabsModule,
        MatTooltipModule,
        RouterTestingModule.withRoutes([
          { component: RouteComponentStub, path: '**' },
        ]),
        NoopAnimationsModule,
      ],
      providers: [
        AccessVerificationService,
        {
          provide: CONSUMERS_SERVICE_TOKEN,
          useValue: consumersServiceStub,
        },
        {
          provide: CONSUMERS_SERVICE_TOKEN,
          useValue: {
            get: cy.stub().as('get').returns(EMPTY),
          },
        },
        {
          provide: ConsumersQuery,
          useValue: consumersQueryStub,
        },
      ],
    });
    cy.wrap(getCypressTestBed().inject(Router)).as('router');
    cy.wrap(getCypressTestBed().inject(ActivatedRoute)).as('activatedRoute');
  });

  it('should be created', () => {
    mountFixture(ConsumerEditorComponent);
    cy.component().should('exist');
  });

  describe('Negative ID Provided', () => {
    beforeEach(() => {
      cy.get('@activatedRoute').set('snapshot.params', { id: -100 });
      mountFixture(ConsumerEditorComponent);
    });

    it('should navigate to /page-not-found', () => {
      cy.detectChanges()
        .get('@router')
        .its('url')
        .should('equal', '/page-not-found');
    });
  });

  describe('Non-numeric ID Provided', () => {
    beforeEach(() => {
      cy.get('@activatedRoute').set('snapshot.params', { id: 'invalid-id' });
      mountFixture(ConsumerEditorComponent);
    });

    it('should navigate to /page-not-found', () => {
      cy.detectChanges()
        .get('@router')
        .its('url')
        .should('equal', '/page-not-found');
    });
  });

  describe('Valid ID Provided', () => {
    beforeEach(() => {
      cy.get('@activatedRoute').set('snapshot.params', {
        id: mockConsumer.consumer_id,
      });
    });

    describe('Get View Returns Error', () => {
      beforeEach(() => {
        cy.get<SinonStub>('@get')
          .invoke('returns', throwError(new Error('Error')))
          .then(() => void mountFixture(ConsumerEditorComponent));
      });

      it('should navigate to /page-not-found', () => {
        cy.detectChanges()
          .get('@router')
          .its('url')
          .should('equal', '/page-not-found');
      });
    });

    describe('Get Consumer Returns Consumer', () => {
      beforeEach(() => {
        cy.get('@get')
          .invoke('returns', of(mockConsumer))
          .then(() => void mountFixture(ConsumerEditorComponent));
      });

      it('should display the name of the Consumer in the card title', () => {
        cy.get('mat-card-title').should('contain.text', mockConsumer.name);
      });

      it('should remain on the same page', () => {
        cy.detectChanges().get('@router').its('url').should('equal', '/');
      });

      ['Details', 'Views'].forEach((label, index) => {
        describe(`Click ${label} Tab`, () => {
          beforeEach(() => {
            // Since the tab group initializes to the first tab, select a different tab
            // on the first test so that the first tab can be switched to
            if (index === 0) {
              cy.get('.mat-tab-label')
                .last()
                .click()
                .detectChanges()
                .waitPromise(0);
            }
            cy.get('.mat-tab-label').contains(label).click().detectChanges();
          });

          it(`should set the URL fragment to ${label.toLocaleLowerCase()}`, () => {
            cy.get('@router')
              .its('url')
              .should('equal', `/#${label.toLocaleLowerCase()}`);
          });

          it(`should get the desired component`, () => {
            cy.get(`ironsky-consumer-${label.toLocaleLowerCase()}`).should(
              'exist',
            );
          });
        });
      });
    });
  });
});
