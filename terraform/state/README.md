# Terraform S3 State (DynamoDB Lock)

Use this to bootstrap the S3 bucket and DynamoDB table used for state and state locking. This state is not tracked or versioned and only created a single time.

```bash
terraform apply -var="state_name_prefix=cloud-manager-terraform"
```
