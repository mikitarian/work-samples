output "nlb_dns" {
  description = "The DNS of the network load balancer"
  value       = module.nlb.this_lb_dns_name
}