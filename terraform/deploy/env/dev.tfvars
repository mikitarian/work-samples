# the environment name, used to prefix resource names along with cm
# for example, cm-dev-<resource>
# also passed to services as ASPNETCORE_ENVIRONMENT environment varible to pull secrets and parameters
environment = "dev"

# the FQDN (used for S3 redirects)
fqdn = "dev.cm2.cloudtracker.net"

# name used for the UI S3 bucket
# if we need to create a CNAME (for ZPA), this should be the FQDN of the CNAME
# otherwise, it can be any S3 bucket name
ui_fqdn = "dev-ui.cm2.cloudtracker.net"

# image tag to use when pulling images
image_tag = "latest"

# option prefix list for ingress into api-gateway
web_ingress_prefix_list = ["pl-0e58cdd68afdbc5b2"]

# optional list of CIDR for ingress into api-gateway
web_ingress_cidr_list = ["149.96.5.11/32", "149.96.5.10/32"]

# following values come from the resources created in BASICS
# currently we are using the same role for execution (creating the service) and task (ongoing running of service)
ecs_execution_role = "arn:aws-us-gov:iam::281978155256:role/cm_ecs_execution"
ecs_task_role      = "arn:aws-us-gov:iam::281978155256:role/cm_ecs_execution"

vpc_id = "vpc-0150502826b482fed"

# the CIDR block of the VPC
# used for NLB health check ingress and other security group rules
vpc_cidr_block = "10.0.0.0/16"

# subnets to create the ECS services
private_subnets = [
  "subnet-0f2f493ee014ad7b8",
  "subnet-0fd1e04e02fd6a4e5",
  "subnet-0d11ba2787319052c",
]

# subnets to create the external NLB
public_subnets = [
  "subnet-0075b4f9b5eada2b8",
  "subnet-09c22c197802d7995",
  "subnet-02a8d46b224745bf4",
]
