"""
To support migration from DevExpress and the legacy CAMO Platform
The existing Purchase Requests need to be moved into the applicable database to ensure data is not lost in transfer
While these documents ARE stored in PDF long term hopes of generating custom reports and lookups would benefit
From this data being carried over
"""
import pyodbc

from db_credentials import datawarehouse_db_config, sqlserver_db_config
from etl import etl_process
from sql_queries import sqlserver_queries


def main():
    """
    The main entry point, doesn't return anything - just runs the ETL
    :return:
    """

    # establish connection for target database (sql-server)
    target_cnx = pyodbc.connect(**datawarehouse_db_config)

    """
     There's only one server, but there could be many if there was a bigger lift.
     When/IF we want to move the CRM stuff from FENCES over, the same logic can apply
    """
    for config in sqlserver_db_config:
        try:
            print("loading db: " + config['database'])
            etl_process(sqlserver_queries, target_cnx, config, 'sqlserver')
        except Exception as error:
            print("etl for {} has error".format(config['database']))
            print('error message: {}'.format(error))
            continue

    target_cnx.close()


if __name__ == "__main__":
    main()
