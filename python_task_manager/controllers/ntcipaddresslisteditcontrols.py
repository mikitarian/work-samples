from app_defaults import *
from controllers.datatools import *

class NtcIpAddressListEditControls(StackLayout):

	def adjust_ip_address_sizes(self,selected_value):
		if selected_value == 'ip_v4':
			self.ids.ip_address_one.size=(150, 25)
			self.ids.ip_address_one.text_size=(150, 25)
			self.ids.ip_address_two.size=(150, 25)
			self.ids.ip_address_two.text_size=(150, 25)
		elif selected_value == 'ip_v6':
			self.ids.ip_address_one.size=(335, 25)
			self.ids.ip_address_one.text_size=(335, 25)
			self.ids.ip_address_two.size=(335, 25)
			self.ids.ip_address_two.text_size=(335, 25)

		self.display_range_format_message()

	def react_to_ip_address_format(self,selected_value):
		if self.ids.ip_v4_checkbox.active == True:
			self.adjust_ip_address_sizes('ip_v4')
		elif self.ids.ip_v6_checkbox.active == True:
			self.adjust_ip_address_sizes('ip_v6')

		if selected_value == 'hostname':
			self.ids.ip_address_two.disabled=True
			self.ids.ip_v4_checkbox.disabled=True
			self.ids.ip_v6_checkbox.disabled=True
			self.ids.ip_address_two.disabled=True
			self.ids.ip_address_label_one.text='Hostname:'
			self.ids.ip_address_label_two.text=''
			self.ids.address_range_info_label.text=''
		elif selected_value == 'single_address':
			self.ids.ip_address_two.disabled=True
			self.ids.ip_v4_checkbox.disabled=False
			self.ids.ip_v6_checkbox.disabled=False
			self.ids.ip_address_two.disabled=True
			self.ids.ip_address_label_one.text='Address:'
			self.ids.ip_address_label_two.text='Address Two:'
			self.ids.address_range_info_label.text=''
		elif selected_value == 'address_range':
			self.ids.ip_address_two.disabled=True
			self.ids.ip_v4_checkbox.disabled=False
			self.ids.ip_v6_checkbox.disabled=False
			self.ids.ip_address_two.disabled=False
			self.ids.ip_address_label_one.text='Starting Address:'
			self.ids.ip_address_label_two.text='Ending Address:'
			self.evaluate_ip_address_entries(self.ids.ip_address_one.text)

		self.display_range_format_message()

	def evaluate_ip_address_entries(self,text):
		if self.ids.hostname_checkbox.active == True:
			pass # allow anything
		elif self.ids.single_address_checkbox.active == True:
			pass # we'll use iptools to validate on submit
		elif self.ids.address_range_checkbox.active == True:
			if '/' in text:	# checking for ip address range in aaa.bbb.ccc.ddd/n format
				self.ids.ip_address_two.disabled=True
			else:
				self.ids.ip_address_two.disabled=False

	def display_range_format_message(self):
		if self.ids.ip_v4_checkbox.active == True:
			addr_format = 'aaa.bbb.ccc.ddd/n'
		elif self.ids.ip_v6_checkbox.active == True:
			addr_format = 'aaaa:bbbb:cccc:dddd:eeee:ffff:gggg:hhhh/n'

		if self.ids.address_range_checkbox.active == True:
			self.ids.address_range_info_label.text='"'+addr_format+'\"    IP Range format is supported in the [b]Starting Address[/b] field.'
		else:
			self.ids.address_range_info_label.text=''

	def cancel_ip_address_input(self):
		self.ids.ip_v4_checkbox.active=True
		self.ids.single_address_checkbox.active=True
		self.ids.ip_address_one.text=''
		self.ids.ip_address_two.text=''
		self.ids.ip_address_two.disabled=True
		self.ids.address_range_info_label.text=''
		self.react_to_ip_address_format('single_address')
		self.clear_ip_address_feedback_message()

	def load_address_to_edit(self,address_string):
		self.cancel_ip_address_input()

		addr_one = ''
		addr_two = ''
		validip4_1 = False
		validip4cidr_1 = False
		validip6_1 = False
		validip6cidr_1 = False
		validip4_2 = False
		validip6_2 = False
		ip_addr_format_string='single_address'

		# first see if we need to split the string
		if address_string.find('-') > -1:
			addrs = address_string.split('-')
			addr_one = addrs[0]
			addr_two = addrs[1]
		else:
			addr_one = address_string

		# next see if we have an actual ip address
		validip4_1 = iptools.ipv4.validate_ip(addr_one)
		validip4cidr_1 = iptools.ipv4.validate_cidr(addr_one)
		validip6_1 = iptools.ipv6.validate_ip(addr_one)
		validip6cidr_1 = iptools.ipv6.validate_cidr(addr_one)

		# next determine which format it's in
		if validip4_1 == True or validip4cidr_1 == True:
			self.ids.ip_v4_checkbox.active=True
		elif validip6_1 == True or validip6cidr_1 == True:
			self.ids.ip_v6_checkbox.active=True

		# see if we have a second valid ip address
		if len(addr_two) > 0:
			validip4_2 = iptools.ipv4.validate_ip(addr_two)
			validip6_2 = iptools.ipv6.validate_ip(addr_two)

		# if we have an ip address range, select the address_range radio button
		if validip4cidr_1 == True or \
			validip6cidr_1 == True or \
			(validip4_1 == True and validip4_2 == True) or \
			(validip6_1 == True and validip6_2 == True):
			self.ids.address_range_checkbox.active=True
			ip_addr_format_string='address_range'
			
		elif validip4_1 == False and validip4_2 == False and \
			validip6_1 == False and validip6_2 == False:
			self.ids.hostname_checkbox.active=True
			ip_addr_format_string='hostname'

		self.react_to_ip_address_format(ip_addr_format_string)
		self.ids.ip_address_one.text=str(addr_one)
		self.ids.ip_address_two.text=str(addr_two)


	def validate_ip_addresses(self):
		error_msg = ''
		address_one = self.ids.ip_address_one.text.strip()
		address_two = self.ids.ip_address_two.text.strip()

		if self.ids.ip_v4_checkbox.active == True:
			ip_version = 'ip_v4'
		else:
			ip_version = 'ip_v6'

		if self.ids.hostname_checkbox.active == True:
			ip_format = 'hostname'
		elif self.ids.single_address_checkbox.active == True:
			ip_format = 'single_address'
		elif self.ids.address_range_checkbox.active == True:
			ip_format = 'address_range'

		no_ip_entered_msg = 'An IP Address is required in order to save.'

		if ip_version == 'ip_v4' and ip_format == 'single_address':
			validip4 = iptools.ipv4.validate_ip(address_one)
			if len(address_one) < 1:
				error_msg = no_ip_entered_msg
			elif validip4 is False:
				error_msg = 'The IP Address is not a valid, single IPv4 Address.\nPlease enter an IP Address in aaa.bbb.ccc.ddd format.'

		elif ip_version == 'ip_v6' and ip_format == 'single_address':
			validip6 = iptools.ipv6.validate_ip(address_one)
			if len(address_one) < 1:
				error_msg = no_ip_entered_msg
			elif validip6 is False:
				error_msg = 'The IP Address is not a valid, single IPv6 Address.\nPlease enter an IP Address in aaaa:bbbb:cccc:dddd:eeee:ffff:gggg:hhhh format.'

		elif ip_version == 'ip_v4' and ip_format == 'address_range':
			validip4cidr = iptools.ipv4.validate_cidr(address_one)
			validip4_1 = iptools.ipv4.validate_ip(address_one)
			validip4_2 = iptools.ipv4.validate_ip(address_two)
			validip4cidr_2 = iptools.ipv4.validate_cidr(address_two)

			if len(address_one) < 1:
				error_msg = 'Please enter a Starting Address.'
			elif validip4_1 == False and validip4cidr == False:
				error_msg = 'The Starting Address is not a valid, single IPv4 Address.\nPlease enter a Starting Address in aaa.bbb.ccc.ddd format.'
			elif validip4_1 == True and validip4cidr == False and len(address_two) < 1:
				error_msg = 'Please enter an Ending IP Address for this Address Range.'
			elif address_one == address_two:
				error_msg = 'The Starting Address is the same as the Ending Address.\nPlease enter two different IP Addresses.'
			elif validip4cidr_2 == True:
				error_msg = 'An IP Address Range is [b]not allowed[/b] in the [b]Ending Address[/b] field.'

		elif ip_version == 'ip_v6' and ip_format == 'address_range':
			validip6cidr = iptools.ipv6.validate_cidr(address_one)
			validip6_1 = iptools.ipv6.validate_ip(address_one)
			validip6_2 = iptools.ipv6.validate_ip(address_two)
			validip6cidr_2 = iptools.ipv6.validate_cidr(address_two)

			if len(address_one) < 1:
				error_msg = 'Please enter a Starting Address.'
			elif validip6_1 == False and validip6cidr == False:
				error_msg = 'The Starting Address is not a valid, single IPv6 Address.\nPlease enter a Starting Address in aaaa:bbbb:cccc:dddd:eeee:ffff:gggg:hhhh format.'
			elif validip6_1 == True and validip6cidr == False and len(address_two) < 1:
				error_msg = 'Please enter an Ending IP Address for this Address Range.'
			elif address_one == address_two:
				error_msg = 'The Starting Address is the same as the Ending Address.\nPlease enter two different IP Addresses.'
			elif validip6cidr_2 == True:
				error_msg = 'An IP Address Range is [b]not allowed[/b] in the [b]Ending Address[/b] field.'

		# now check for duplicate entries & warn user if it's a dupe
		if len(address_one) > 0 and len(address_two) > 0:
			the_address = address_one+'-'+address_two
		else:
			the_address = address_one

		list_of_ip_addresses = SpinnerListInfo.get_list_contents(self, str(ip_address_file_name), False)

		if the_address in list_of_ip_addresses:
				error_msg = 'The address you entered is already in the dropdown list.\nPlease enter a unique address'

		if len(error_msg) > 0:
			self.ids.ip_address_feedback_label.text=error_msg
			self.ids.ip_address_feedback_label.color=error_text_color
			return 'invalid_list_item'
		else:
			self.clear_ip_address_feedback_message()
			return 'valid_list_item'

	def clear_ip_address_feedback_message(self):
		self.ids.ip_address_feedback_label.text=''