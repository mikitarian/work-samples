/*	DELETE records from inventory_location TABLE
	
	Sam Mikitarian 04 Jan 2022
*/

DO $$
DECLARE

BEGIN

	DELETE FROM inventory_location;
	
END;
$$