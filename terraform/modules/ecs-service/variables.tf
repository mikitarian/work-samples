# BEGIN Variables for "aws_ecs_service"
variable "service_name" {
  description = "Name to be used on to identify the service"
  type        = string
}

variable "ecs_cluster" {
  description = "The ECS cluster to execute the service and task"
  type        = string
}

variable "desired_count" {
  description = "Number of tasks to run for the service"
  type        = number
  default     = 1
}

variable "load_balancer_blocks" {
  description = "List of load balancers to associate service with"
  type        = list(map(any))
  default     = []
}

variable "service_discovery_namespace_id" {
  description = "The ID of the service discovery namespace"
  type        = string
}
# END Variables for "aws_ecs_service"


# BEGIN Variables for "aws_ecs_task_definition"
variable "task_name" {
  description = "Name to be used to identify the task"
  type        = string
}

variable "subnets" {
  description = "Subnets used by the ECS service"
  type        = list(string)
}

variable "security_groups" {
  description = "Security groups used by the ECS service"
  type        = list(string)
  default     = []
}

variable "execution_role_arn" {
  description = "ARN of the AWS IAM Role that will execute the aws_ecs_task_definition"
  type        = string
}

variable "task_role_arn" {
  description = "ARN of the AWS IAM Role that task execution will use"
  type        = string
}

variable "repository_uri" {
  description = "URI of the Repository that holds the image to be used"
  type        = string
}

variable "image_tag" {
  description = "Tag of the image being used.  This gets concatenated with var.repository_uri, above."
  type        = string
  default     = "latest"
}

variable "health_check_command" {
  description = "The command to run within the container to determine if its healthy"
  type        = list(string)
}

variable "container_cpu" {
  description = "CPU Units of container being run.  See the AWS AMI table for valid values"
  type        = number
  default     = 512
}

variable "container_memory" {
  description = "Memory/RAM size of container being run.  See the AWS AMI table for valid values"
  type        = number
  default     = 2048
}

variable "container_is_essential" {
  description = "Memory/RAM size of container being run.  See the AWS AMI table for valid values"
  type        = bool
  default     = true
}

variable "container_ports" {
  description = "List of container ports to expose"
  type        = list(number)
}

variable "environment_variables" {
  description = "List of environment varialbes provided to container"
  type        = list(map(string))
  default     = []
}

variable "secrets" {
  description = "List of secrets (valueFrom ARN) provided to container"
  type        = list(map(string))
  default     = []
}

variable "region" {
  description = "Region to write logs in CloudWatch"
  type        = string
}

variable "log_group" {
  description = "Name of CloudWatch log group to write logs"
  type        = string
}
# END Variables for "aws_ecs_task_definition" 
