name: web-ui

on:
  push:
    branches:
      - develop
  workflow_call:  # including all inputs from deployOnDemand.yml/workflow_dispatch, even if not currently used by this workflow
    inputs:
      target_aws_region:
        description: 'Target AWS Region to deploy to'
        required: true
        default: us-gov-east-1
        type: string
      target_environment:
        description: 'Target Environment to deploy to.'
        required: true
        default: dev
        type: string
      custom_image_tag:
        description: 'Custom Tag for Build Artifacts. (optional)'
        required: false
        default: latest
        type: string
      web_ui_source_zip:
        description: 'Name of .zip file of Web UI deployment artifact.'
        default: cm-source-code.zip
        required: false
        type: string
      bucket_public_ui:
        description: 'Public AWS S3 bucket where UI is served'
        default: dev-ui.cm2.cloudtracker.net
        required: false
        type: string
      bucket_private:
        description: 'Private AWS S3 bucket where artifacts are stored'
        default: cloudmanager-artifacts
        required: false
        type: string

env:
  # env variables hold default values used by github "push" event

  # input variables - should match default values in "workflow_call" event above
  TARGET_AWS_REGION: us-gov-east-1
  IMAGE_TAG: latest
  WEB_UI_SOURCE_ZIP: cm-source-code.zip
  BUCKET_PUBLIC_UI: dev-ui.cm2.cloudtracker.net     # AWS S3 bucket from which UI is served
  BUCKET_PRIVATE: cloudmanager-artifacts            # AWS S3 bucket where artifacts are stored
  

jobs:
  clean-branch-name:
    uses: ./.github/workflows/r_stringTools.yml
    with:
      string_type: clean_branch_name
      string_value_one: ${{ github.ref }}

  branch-name-return:
    outputs:
      final-trimmed-branch-name: ${{ steps.show-output.outputs.final-trimmed-branch-name }}
    runs-on: ubuntu-latest
    needs: [clean-branch-name]
    steps:
      - name: show-output
        id: show-output
        run: |
          echo "final-trimmed-branch-name=${{ needs.clean-branch-name.outputs.trimmed_branch }}" >> $GITHUB_OUTPUT
          echo "trimmed branch name is ${{ needs.clean-branch-name.outputs.trimmed_branch }}"

  build-web-ui:
    needs: [branch-name-return]
    runs-on: ubuntu-latest
    outputs:
      final-target-aws-region: ${{ steps.set-values.outputs.final-target-aws-region }}
      final-image-tag: ${{ steps.set-values.outputs.final-image-tag }}
      final-bucket-public-ui: ${{ steps.set-values.outputs.final-bucket-public-ui }}
      final-bucket-private: ${{ steps.set-values.outputs.final-bucket-private }}
      final-web-ui-source-zip: ${{ steps.final-web-ui-source-zip.outputs.final-web-ui-source-zip }}

    steps:
    - uses: actions/checkout@v3
    - name: Setup .NET
      uses: actions/setup-dotnet@v2
      with:
        dotnet-version: 7.0.203

    - name: Add DevExpress Nuget source
      id: devexpress-nuget-source
      run: dotnet nuget add source --name devexpress "https://nuget.devexpress.com/${{ secrets.NUGET_DEVEXPRESS_KEY }}/api"

    - name: Restore .NET dependencies
      id: restore-dotnet-dependencies
      run: dotnet restore
      working-directory: CloudManager/Client
      
    ##### Begin update of variables #####

    - name: meta data in web-ui.yml
      id: event-meta-data
      run: |
        echo "github.event_name is ${{ github.event_name }}"
        echo "Selected branch or tag is ${{ github.ref }}"

    - name: set-values  # inputs.<var_name> are available for both workflow_call and workflow_dispatch events
      id: set-values
      run: |
        if [[ "${{ github.event_name }}" == "push" ]]
        then
          tgt_region="${{ env.TARGET_AWS_REGION }}"
          image_tag="${{ env.IMAGE_TAG }}"
          b_public="${{ env.BUCKET_PUBLIC_UI }}"
          b_private="${{ env.BUCKET_PRIVATE }}"
        else
          tgt_region="${{ inputs.target_aws_region }}"
          image_tag="${{ inputs.custom_image_tag }}"
          image_tag=$(echo $image_tag | tr -d '/')
          b_public="${{ inputs.bucket_public_ui }}"
          b_private="${{ inputs.bucket_private }}"
        fi
        echo "final-target-aws-region=$tgt_region" >> $GITHUB_OUTPUT
        echo "TARGET_AWS_REGION=$(echo $tgt_region)" >> $GITHUB_ENV
        echo "final-image-tag=$image_tag" >> $GITHUB_OUTPUT
        echo "final-bucket-public-ui=$b_public" >> $GITHUB_OUTPUT
        echo "final-bucket-private=$b_private" >> $GITHUB_OUTPUT
        
    - name: create final Web UI Source Zip string
      id: final-web-ui-source-zip
      run: |
        if [[ "${{ github.event_name }}" == "push" ]]
        then
          old_string=${{ env.WEB_UI_SOURCE_ZIP }}
        else
          old_string=${{ github.event.inputs.web_ui_source_zip }}
        fi
        new_string=${old_string%.*}
        final_string="${{ needs.branch-name-return.outputs.final-trimmed-branch-name }}-$new_string.${{ steps.set-values.outputs.final-image-tag }}.zip"
        echo "final-web-ui-source-zip=$final_string" >> $GITHUB_OUTPUT

    ##### End update of variables #####


    # added "dotnet workload install wasm-tools-net6" because dotnet v7 is not backward compatible with this step
    - name: Build
      id: build-client
      run: |
        dotnet workload install wasm-tools-net6
        dotnet publish -r browser-wasm --self-contained
      working-directory: CloudManager/Client

    - name: Create Zip File
      uses: montudor/action-zip@v0.1.0
      with:
        args: zip -r ${{ steps.final-web-ui-source-zip.outputs.final-web-ui-source-zip }} CloudManager

    # changed path from "net6.0/publish/wwwroot/" to "net7.0/publish/wwwroot/" as part of upgrade to dotnet-version: 7.0.x
    - name: Upload artifacts
      id: upload-artifacts
      uses: actions/upload-artifact@master
      with:
        name: cloudmanger-artifacts
        path: |
          CloudManager/Client/bin/Debug/net7.0/publish/wwwroot/
          ${{ steps.final-web-ui-source-zip.outputs.final-web-ui-source-zip }}

    - name: Check Web-ui Build
      if: |
        steps.devexpress-nuget-source.outcome != 'success'
        || steps.restore-dotnet-dependencies.outcome != 'success'
        || steps.build-client.outcome != 'success'
        || steps.upload-artifacts.outcome != 'success'
      run: exit 1

  deploy-web-ui:
    runs-on: ubuntu-latest
    needs: [build-web-ui, branch-name-return]
    steps:
    - name: Download artifacts
      id: download-artifacts
      uses: actions/download-artifact@master
      with:
        name: cloudmanger-artifacts
        path: cmartifacts

    - name: Set AWS credentials
      id: set-aws-credentials
      uses: aws-actions/configure-aws-credentials@v1
      with:
        aws-access-key-id: ${{ secrets.AWS_ACCESS_KEY_ID }}
        aws-secret-access-key: ${{ secrets.AWS_SECRET_ACCESS_KEY }}
        aws-region: ${{ env.TARGET_AWS_REGION }}

    # changed path from "net6.0/publish/wwwroot/" to "net7.0/publish/wwwroot/" as part of upgrade to dotnet-version: 7.0.x
    - name: Copy to S3
      id: copy-to-s3
      run: aws s3 sync cmartifacts/CloudManager/Client/bin/Debug/net7.0/publish/wwwroot/ s3://${{ needs.build-web-ui.outputs.final-bucket-public-ui }}/ --delete

    - name: Copy artifacts Zip File of Web UI source code to private bucket
      id: copy-artifacts-to-s3
      run: aws s3 cp cmartifacts/${{ needs.build-web-ui.outputs.final-web-ui-source-zip }} s3://${{ needs.build-web-ui.outputs.final-bucket-private }}/${{ needs.branch-name-return.outputs.final-trimmed-branch-name }}_build_artifacts/${{ needs.build-web-ui.outputs.final-image-tag }}/

    - name: Check Web-ui Deploy
      if: |
        steps.download-artifacts.outcome != 'success' 
        || steps.set-aws-credentials.outcome != 'success' 
        || steps.copy-to-s3.outcome != 'success' 
        || steps.copy-artifacts-to-s3.outcome != 'success'
      run: exit 1
