#!/bin/bash

# This script allows the user to add or remove Demo Cases/Tasks/Persons etc
# in the FLIMS Postgresql Database
# 
# PREREQUISITES TO RUNNING THIS SCRIPT:
#		Build, Seed and Start the FLIMS postgresql database
#		Build and Start the FLIMS Keycloak server
#		Build, Start and enter the Setup Parameters for the FLIMS Web server
#
# TO USE: 
#	1)  Open a GitBash Terminal Window (or some other terminal that can run a .sh script):
#
#	2)	Change directory to the cssp-nx/scripts directory
#			
#			cd /your/path/to/cssp-nx/scripts
#
#
#	3)	Run the manage_demo_db_records.sh script:
#
#			./manage_demo_db_records.sh
#
#	4)	NOTE: SEQUENCE IS IMPORTANT HERE:
#
#		If you HAVE NOT already loaded  Users into Keycloak, DO THAT FIRST.
#		That step will require a reboot of Keycloak, 
#		but you can move on to the DB work while it's coming back online.
# 
# Sam Mikitarian
# 27 Dec 2021

error_exit()
{
	echo ""
    echo "Error: $1"
	echo ""
	if [ $2 ]; then
		$2
	fi
    exit 1
}

get_keycloak_container_name(){

	printf "\n\n"
	read -ep "Enter the name of the Keycloak Docker Container [enter \"d\" for default of \"keycloak\"]: " keycloak_docker_container_name

		# check docker container name
		if [[ -z "$keycloak_docker_container_name" ]]; then
			printf "blank_container_name"
		else
			if [[ "$keycloak_docker_container_name" == "d" ]] 
			then
				keycloak_docker_container_name="keycloak"
			else
				keycloak_docker_container_name="$keycloak_docker_container_name"
			fi
			
			printf "${keycloak_docker_container_name}"
		fi
}

get_postgresql_container_name(){

	printf "\n\n"
	read -ep "Enter the name of the Postgresql Docker Container [enter \"d\" for default of \"postgresql-flims\"]: " postgresql_docker_container_name

		# check docker container name
		if [[ -z "$postgresql_docker_container_name" ]]; then
			printf "blank_container_name"
		else
			if [[ "$postgresql_docker_container_name" == "d" ]] 
			then
				postgresql_docker_container_name="postgresql-flims"
			else
				postgresql_docker_container_name="$postgresql_docker_container_name"
			fi
			
			printf "${postgresql_docker_container_name}"
		fi
}

get_os_type(){

	if [[ "$OSTYPE" == "linux-gnu"* || "$OSTYPE" == "darwin"* ||"$OSTYPE" == "freebsd"* ]]; then
		# Docker on Linux, Mac OSX, Unix systems doesn't need a "winpty" prefix
		os_type="linux"
	elif [[ "$OSTYPE" == "cygwin" || "$OSTYPE" == "msys" || "$OSTYPE" == "win32" ]]; then
		# cygwin = POSIX compatibility layer and Linux environment emulation for Windows
		# msys = Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
		# win32 = I'm not sure if this exists, but better safe than sorry
		os_type="windows"
	fi
	
	printf "${os_type}"

}

manage_keycloak_users(){

	printf "\nBegin adding users to Keycloak:"
	printf "\n\n"
	
	container_name=$(get_keycloak_container_name)
	
	if [ $container_name = 'blank_container_name' ]; then
		error_exit "Keycloak's Docker Container Name cannot be blank.  Please try again." main_menu
	else

		os_type=$(get_os_type)
		
		if [[ $os_type == 'linux' ]]; then
			command_prefix=""
		elif [[ $os_type == 'windows' ]]; then
			command_prefix="winpty"
		fi

		printf "\nCopying script to the %s Docker Container..." $container_name
		
		docker cp ./01_manage_keycloak_users/keycloak_user_management.sh $container_name:/opt/jboss/keycloak/bin/ || error_exit "Something went wrong with the copy process.  Please check the Docker Container Name and try again."

		printf "\n\nCopy complete"
		printf "\n\nStarting the Keycloak User Management script on the %s Docker Container..." $container_name
		printf "\n\n"
	
		$command_prefix docker exec -it $container_name bash -c "cd /opt/jboss/keycloak/bin;sh ./keycloak_user_management.sh"
		
		clear
		printf "\n\nLeaving Keycloak Container and Returning to Main Menu."
		printf "\n\nRestarting the %s Docker Container" $container_name
		printf "\n\n"
		
	fi
	
	restart_docker_container "${container_name}"
}

restart_docker_container(){
	docker container restart $1
	main_menu
}

export_user_list_from_keycloak(){

	container_name=$(get_keycloak_container_name)
	
	if [ $container_name = 'blank_container_name' ]; then
		error_exit "Keycloak's Docker Container Name cannot be blank.  Please try again." main_menu
	else
	
		os_type=$(get_os_type)
		
		if [ $os_type = 'linux' ]; then
			command_prefix=""
		elif [ $os_type = 'windows' ]; then
			command_prefix="winpty"
		fi
		
		printf "\n\nWhen the Keycloak User Management script starts on %s" $container_name
		printf "\nchoose the \"Export a list of Keycloak Users\" option\n\n"
		
		$command_prefix docker exec -it $container_name bash -c "cd /opt/jboss/keycloak/bin;sh ./keycloak_user_management.sh" || error_exit "Cannot connect to specified container.  Please check the name and try again."

		clear
		printf "\n\nLeaving Keycloak Container and Returning to Main Menu."
		printf "\n\n"

	fi
	
	main_menu
}

copy_user_list_from_keycloak(){
	
	container_name=$(get_keycloak_container_name)
	
	if [ $container_name = 'blank_container_name' ]; then
		error_exit "Keycloak's Docker Container Name cannot be blank.  Please try again." main_menu
	else
		
		printf "\n\nCopying the User List from %s" $container_name
		printf "\n\n"
		
		docker cp $container_name:/opt/jboss/keycloak/bin/demo_users_with_ids.txt ./02_insert_db_records || error_exit "There was a problem copying the demo_users_with_ids.txt file.  Please check the Container Name and try again." main_menu
		
		ls ./02_insert_db_records | grep -i "demo_users_with_ids.txt" || error_exit "Filename: \"demo_users_dddwith_ids.txt\" not found in ./02_insert_db_records.  Please try Exporting the User List from Keycloak again." main_menu
		
		clear
		printf "\n\nCopy complete."
		printf "\n\nReturning to Main Menu."
		printf "\n\n"

	fi
	
	main_menu	
}

insert_demo_data(){

	container_name=$(get_postgresql_container_name)
	
	if [ $container_name = 'blank_container_name' ]; then
		error_exit "The Postgresql Docker Container Name cannot be blank.  Please try again." main_menu
	else
	
		os_type=$(get_os_type)
		
		if [ $os_type = 'linux' ]; then
			command_prefix=""
		elif [ $os_type = 'windows' ]; then
			command_prefix="winpty"
		fi
		
		printf "\nCopying files to the %s Docker Container...\n\n" $container_name
		
		docker cp ./02_insert_db_records $container_name:/opt/app-root/src/ || error_exit "Something went wrong with the copy process.  Please check the Docker Container Name and try again." main_menu

		printf "Preparing to run Data Insert Script\n\n"

		$command_prefix docker exec -it $container_name bash -c "cd /opt/app-root/src/02_insert_db_records;sh ./run_db_inserts.sh" || error_exit "Cannot connect to specified container.  Please check the name and try again." main_menu
		
		clear
		printf "\n\nDemo Data Insert is complete"
		printf "\n\nReturning to the Main Menu"
		printf "\n\n"
	fi
	
	main_menu
	
}

delete_demo_data(){
	container_name=$(get_postgresql_container_name)
	
	if [ $container_name = 'blank_container_name' ]; then
		error_exit "The Postgresql Docker Container Name cannot be blank.  Please try again." main_menu
	else
	
		os_type=$(get_os_type)
		
		if [ $os_type = 'linux' ]; then
			command_prefix=""
		elif [ $os_type = 'windows' ]; then
			command_prefix="winpty"
		fi
		
		printf "\nCopying files to the %s Docker Container...\n\n" $container_name
		
		docker cp ./03_delete_db_records $container_name:/opt/app-root/src/ || error_exit "Something went wrong with the copy process.  Please check the Docker Container Name and try again." main_menu

		printf "Preparing to run Data Delete Script\n\n"

		$command_prefix docker exec -it $container_name bash -c "cd /opt/app-root/src/03_delete_db_records;sh ./run_db_deletes.sh" || error_exit "Cannot connect to specified container.  Please check the name and try again." main_menu
		
		clear
		printf "\n\nDemo Data Delete is complete"
		printf "\n\nReturning to the Main Menu"
		printf "\n\n"
	fi
	
	main_menu
}

unused_function(){
'FILES="./*.sql"
for f in $FILES
do
	echo "Processing $f file..."
	# take action on each file. $f store current file name
	# cat "$f"
	psql -d flims postgres -f $f >/dev/null 2>&1
done
';
}

main_menu()
{
	printf "\nThis is the FLIMS Manage Demo Data script."
	printf "\n\nPREREQUISITES TO RUNNING THIS SCRIPT:"
	printf "\n	-Build, Start and Seed the FLIMS postgresql database"
	printf "\n	-Build and Start the FLIMS Keycloak server"
	printf "\n	-Build and Start the FLIMS Web server"
	printf "\n	     (ensures all DB entities are built and available)"
	printf "\n\nRun each step in the order it appears below."
	printf "\n\nChoose your next action:"
	printf "\n\n     -Manage Keycloak Users (m)"
	printf "\n          Adding Demo Users to Keycloak is a prerequisite for adding Demo Data to the FLIMS DB."
	printf "\n\n          You will need the name of the Docker Container running Keycloak,"
	printf "\n          and Keycloak\'s Port Number, Admin Username, and Admin Password"
	printf "\n          for all Keycloak tasks."
	printf "\n"
	printf "\n     -Export the list of Users from Keycloak (e)"
	printf "\n          Assumes you finished the previous step"
	printf "\n          and the Keycloak restart is complete."
	printf "\n"
	printf "\n     -Copy User list from Keycloak (c)"
	printf "\n          Assumes you finished the previous two steps"
	printf "\n          and the Keycloak restart is complete."
	printf "\n"
	printf "\n     -Insert Demo Data to the FLIMS DB (i)"
	printf "\n"
	printf "\n     -Delete Demo Data from the FLIMS DB (d)"
	printf "\n"
	printf "\n     -Quit this script (q)"
	printf "\n\n"
	read -n1 -p "Enter your choice here: " decision
		case "$decision" in
			m)
				clear
				manage_keycloak_users
				;;
			e)
				clear
				export_user_list_from_keycloak
				;;
			c)
				clear
				copy_user_list_from_keycloak
				;;
			i)
				clear
				printf "\n\n     -you chose \"Insert Demo Data to the FLIMS DB\"\n\n"
				insert_demo_data
				;;
			d)
				clear
				printf "\n\n     -you chose \"Delete Demo Data from the FLIMS DB\"\n\n"
				delete_demo_data
				;;
			q)
				clear
				printf "\n\n     Quitting the Manage Demo Records script\n\n"
				exit 0
				;;
			*)
				clear
				printf "\n\n***********************************************************************************************************"
				printf "\n\nValid inputs for the Manage Demo Records script are:"
				printf "\n\n          \"a\", \"e\", \"c\", \"i\", \"d\" and \"q\"."
				printf "\n\nAnything else will bring you to this message."
				printf "\n\nIf those inputs are not being accepted, contact the Dev Team for help.\n\n"
				printf "\n***********************************************************************************************************\n"
				;;
		esac
}

main_menu
