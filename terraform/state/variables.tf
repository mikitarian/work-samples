variable "state_name_prefix" {
  description = "Name prefix to use for S3 state bucket and DynamoDB locking table"
  type        = string
}
