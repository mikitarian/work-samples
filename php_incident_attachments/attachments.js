/*************************************
*
*	functions supporting Incident Attachment  Management
*
*	Sam Mikitarian
* 	May 2017
*
**************************************/

function getSpinnerAtt(){
	return '<img src="./css/common/images/indicator.gif"  width="16"  height="16"  border="O"/>';
}

function initEditDialog(){
	$('#editAttachmentsDialogDiv').dialog({ 
		position: ['center',100),
		width: '750px', 
		autoOpen: false, 
		modal: true, 
		closeOnEscape: true, 
		title: 'Edit Attachment'	//edit Processed Attachments
	});
}


function openEditAttachment(att_incident_id, att_id){
	$('#editAttachmentsDialogDiv').dialog('open').load('attMgmt/attEdit.php?att_incident_id='+att_incident_id+'&att_id='+att_id);
}

 
function initUnprocessedAttachmentDialog{){
	$('#editUnprocessedAttachmentsDialogDiv').dialog({ 
		position:   ['center',100],
		width: '900px', 
		autoOpen: false, 
		modal:true, 
		closeOnEscape: true,
		title: 'Unprocessed Attachments'	//edit Unprocessed Attachments
	});
}

function  openUnprocessedAttachment(att_incident_id){
	$('#editUnprocessedAttachmentsDialogDiv').dialog('open').load('attMgmt/attEditUnprocessed.php?att_incident_id='+att_incident_id);
}
 
function attListTablesorter(att_incident_id,read_only){ 
	if(read_only == 'read_only'){
		${'#attachmentsTable'+ att_incident_id}.tablesorter({ headers:{
			0:{sorter:false}
			}
		}};
	}else{
		$('#attachmentsTable'+  att_incident_id}.tablesorter({ headers:{
			0:{sorter:false}, S:{sorter:false}, 6:{sorter:false}
			}
		}};
	}
	
	$('#attachmentsTable'+ att_incident_id+' .missingAttachment'}.children('td'}.css{'background­color','#FFE4C4'};
}// end attListTablesorter



function attUnprocessedTablesorter(att_incident_id, media_windows_folder){
	$('#attUnprocessedTable'+ att_incident_id}.tablesorter({ 
		theme: 'blue',
		headers:{
			0:{sorter:false},S:{sorter:false}
		}
	}};
	$('#attUnprocessedTable'+att_incident_id+' .isDirectory').children('td'}.css('background­ color','#FFE4C4'};
	${'#attTabsDiv'+att_incident_id+' #unprocessedFolderSpan'}.html('File Location: '+media_windows_folder};
}// end attListTablesorter

function refreshAttlist(att_incident_id, att_id, list_type, target_id, called_from}{

	//alert(target_id};
	 
	var tgt_url = '';
	var command = '';
	var filter_by_name = ''; 
	var filter_by_status = '';
	var filter_by_mime_type_group = '';
	var filter_by_attachment_category = ''; 
	var filter_by_meta_tag = '';
	var path_prefix = ''; 
	var read_only = '';

	if(called_from == 'jdbIncident'){ 
		path_prefix = '../';
		read_only = 'read_only';
	}
	
	if(list_type == 'attachments'){
		tgt_url = path_prefix+'attMgmt/attlist.php'; 
		command = 'displayAttachments';
	}else if(list_type == 'filter_attachments'){
		tgt_url = path_prefix+'attMgmt/attlist.php'; 
		command = 'filterAttachments';
		filter_by_name = ${'#attTabsDiv'+att_incident_id + ' #filter_by_name').val(); 
		filter_by_status = $('#attTabsDiv' + att_incident_id + ' #filter_by_status').val(); 
		filter_by_mime_type_group = $('#attTabsDiv' + att_incident_id + ' #filter_by_mime_type_group').val();
		filter_by_attachment_category = ${'#attTabsDiv' + att_incident_id + ' #filter_by_attachment_category').val();
		filter_by_meta_tag = ${'#attTabsDiv' + att_incident_id + ' #filter_by_meta_tag').val();
	}
	
	var attloadingMsg = '<div class="infoMsg incMsgText"> '+getSpinnerAtt()+' <strong>loading...</strong></div>';
	${'#attTabsDiv'+att_incident_id+'  #'+target_id).html{attloadingMsg);
	
	$.post(tgt_url,{ 
		command: command,
		att_incident_id: att_incident_id, 
		att_id: att_id,
		filter_by_name:  filter_by_name, 
		filter_by_status:  filter_by_status,
		filter_by_mime_type_group:  filter_by_mime_type_group, 
		filter_by_attachment_category:   filter_by_attachment_category, 
		filter_by_meta_tag:  filter_by_meta_tag,
		read_only: read_only
	},
	function( returnedData){
		//alert('#attTabsDiv'+att_incident_id+' #'+target_id);
		$('#attTabsDiv'+att_incident_id+' #'+target_id).html(returnedData);
	}};
	 
	attListTablesorter(att_incident_id, read_only};
 
}// end refreshAttlist


//on upload events

function  checkPii(att_incident_id){
	//alert('in checkPii,att_incident_id is: '+att_incident_id};
	var isChecked = $('#attTabsDiv' + att_incident_id + ' #phiPiiDisclaimer1').is(':checked');

	$('#attTabsDiv' + att_incident_id + ' #submit-all'}.removeAttr("disabled");
	$('#attTabsDiv' + att_incident_id + ' #submit-all').css( "opacity","1");
 
}else{
 
	$('#attTabsDiv' + att_incident_id + ' #submit-all').attr("disabled", true);
	$('#attTabsDiv' + att_incident_id + ' #submit-all'}.css("opacity","0.5");

}
//end on upload events


//begin meta tag mgmt scripts

//var origOptions = $('#metaTagValue').find{"opti on").clone();
function filterMetaTagValuelist(meta_tag_parent_container, child_select, att_id){

	var filter_meta_values_by = $('#' + meta_tag_parent_container + ' #metaTagKey option:selected').val();
	//alert{'meta_tag_parent_container id: '+meta_tag_parent_container+1\nfull parent id string: '+'#' + meta_tag_parent_container + ' #metaTagKey'+'\nchild id: '+child_select+'\nfilter meta values by: '+filter_meta_values_by);

	$.post{'attMgmt/attMetaList.php',{ 
		command: 'getAttMetaTagValues', 
		att_id: att_id,
		filter_meta_values_by:  filter_meta_values_by
		},
		function(returnedData}{
			//alert(target_id};
			${'#' + meta_tag_parent_container + ' #metaTagValue').html(returnedData};
		});
}
 
function addKeyValuePair(meta_tag_parent_container, parent_select, child_select, att_id){

	//alert('parent container:  '+meta_tag_parent_container+'\nparent select: '+parent_select+'\nchild select: '+child_select);
	$('#' + meta_tag_parent_container + ' #metaTagFeedbackDiv').html('');

	var selected_key = ${'#' + meta_tag_parent_container + ' #' + parent_select+ ' :selected').val(); 
	var selected_val = $('#' + meta_tag_parent_container + ' #' + child_select+ ' :selected').val(); 
	var new_val = $('#' + meta_tag_parent_container + ' #newMetaTagValue').val();
	var new_meta_tag_flag = '';

	//alert('type of selected_key: '+selected_key+'\nselected_key:  '+selected_key);
	//alert{'type of new_val:'+new_val+'\nnew_val: '+new_val);

	var clearMsgBtn = '<input id="clearMsgBtn" name="clearMsgBtn" type="button" class="button" style="margin-left:lOpx;" value="OK" onclick="$(\'#' + meta_tag_parent_container + ' #metaTagFeedbackDiv\').html(\'\');" />';
/*
selected_key.String; selected_val.String;
*/
if(selected_key == ''){
	$('#' + meta_tag_parent_container + ' #metaTagFeedbackDiv').html{'<div class="errorMsg" style="width:98%;">You must select a <strong>Keyword</strong> from the option list.</div>');
	
}else if(selected_val == '' && new_val == ''){
	$('#' + meta_tag_parent_container + ' #metaTagFeedbackDiv').html('<div class="errorMsg" style="width:98%;">You must select a <strong>Value</strong> from the option list or <strong>enter a new Value</strong> in the field provided.</div>');

}else if( new_val.length > 0 && selected_val.length > O){
	$('#' + meta_tag_parent_container + ' #metaTagFeedbackDiv').html('<div class="errorMsg" style="width:98%;">You cannot select an item from the value list and enter one manually at the same time. Please choose one or the other. '+clearMsgBtn+' </div>');

}else{
	var dupeFlag = 'noDupes';

	//check for dupes
	$('#' + meta_tag_parent_container + ' .kvPairDiv').each(function(){ 
		var myKvKey = $(this).find(' .kvKey').text();
		var myKvValue = $(this).find(' .kvValue').text{); 
		selected_key = selected_key.trim();
		
		if(new_val.length > O){ selected_val = new_val;}

		selected_val = selected_val.trim();
		if( selected_key === myKvKey && selected_val === myKvValue ){ 
			
			dupeFlag = 'yesDupes';
 
			$('#' + meta_tag_parent_container + ' #metaTagFeedbackDiv').html('<div class="errorMsg" style="font-size:12px;"><strong>'+selected_key+': '+selected_val+'</strong> has already been added. '+clearMsgBtn+'</div>');
		}
	});//end funciton

if(dupeFlag == 'noDupes'){

	if(new_val.length > O){ //add user-supplied value to t_ref_attachment_meta  table

	selected_val = new_val;//plug the user-supplied value into the selected_val variable 
	new_meta_tag_flag = 'new_meta_tag';
}

	$.ajax({	//submit the user-supplied value to the db url: 'attMgmt/attSave.php',
		type: 'POST', data:{
		command: 'addNewMetaTag', 
		meta_tag_type: selected_key, 
		meta_tag_dsc: selected_val, 
		att_id:  att_id,
		new_meta_tag_flag: new_meta_tag_flag
	},
	success: function(data){
		}
	});


	//count the total number of key value pairs
	var kvPairCount = $('#' + meta_tag_parent_container + '#metaTagDisplayDiv').children('.kvPairDiv').length.toString(); if(kvPairCount.length == 0 || kvPairCount == 'undefined'){
		kvPairCount = 1;
	}else{
		kvPairCount++;
	}
	
	var newCountVal = O;   //ensure a unique id for each kvPairDiv
		$('#' + meta_tag_parent_container + ' .kvPairDiv').each(function(){ 
				var myCountVal = $(this).data('countval ');
			if(newCountVal < myCountVal){newCountVal = myCountVal;}
		});
		newCountVal++;
	
		$('#' + meta_tag_parent_container + ' #metaTagDisplayDiv').append('<div id="kvPairDiv'+newCountVal+'" class="kvPairDiv" style="margin:3px 0 3px O; border-top:  1px gray solid; width:100%; position:relative;" data-countval="'+newCountVal+'"><span  class="kvKey" style="width:30%; display:inline-block; white-space: nowrap; text-align: right; margin:O 25px 0 0;11>'+selected_key+'</span><span class="kvValue" style="white-space: nowrap; display:inline-block; width:60%; text-align:left;11>'+selected_val+'</span><span  style="width: 10%; text-align:center; position:absolute; right:O;"><img title="remove  meta tag" src="../img/error.png" width="16" height="16" border="O"  style="cursor:pointer; vertical-align:top  !important;" onClick="removeKvPair(\''+meta_tag_parent_container+'\','+newCountVal+\');" /><span></ div>');
		 
		$('#' + meta_tag_parent_container + ' #metaTagTotalSpan'}.text(newCountVal};

	}
}


function removeKvPair(meta_tag_parent_container,  kv_pair_count, am_id, att_id, command}{
//alert('command is: '+command+'\nkv_pai r_count: '+kv_pair_count};
	$('#' + meta_tag_parent_container + ' #kvPairDiv'+kv_pair_count}.remove(); 
	var newKvCount = $('#' + meta_tag_parent_container + ' #metaTagDisplayDiv'}.children('.kvPairDiv'}.length.toString();
	$('#' + meta_tag_parent_container + ' #metaTagTotalSpan').text(newKvCount);

	if(command != ''){
		deleteMetaTags(meta_tag_parent_container, am_id, att_id, command);
	}

}

function deleteMetaTags(meta_tag_parent_container, am_id, att_id, command){ 
	if(att_id  != ''){
		$.post('attMgmt/attSave.php',  { 
			command: command,
			att_id: att_id, 
			am_id: am_id
		},
		function(returnedData){
			$('#' + meta_tag_parent_container + ' #metaTagDisplayDiv').html(''); 
			getMyMetaTags(meta_tag_parent_container, att_id};
		});
	}

}


function resetMetaTagFields(meta_tag_parent_container){
 
	$('#'+meta_tag_parent_container+'  #metaTagKey option:selected').removeAttr('selected');
	$('#'+meta_tag_parent_container+'  #metaTagValue option:selected').removeAttr('selected');
	$('#'+meta_tag_parent_container+'  #newMetaTagValue').val('');

}


function getMyMetaTags(meta_tag_parent_container, att_id){
//alert('in the getMyMetaTags function');
//alert('meta_tag_parent_container: '+meta_tag_parent_container+'\natt_id: '+att_id);

	$.ajax{{
		url:   'attMgmt/attMetalist.php', type: 'POST',
		data:{
			command:  'getMyMetaTags', 
			att_id: att_id
		},
		dataType: 'json', 
		success: function(data){
			//alert( data);
			//alert(data.total_meta_tags);
			//alert(data.my_meta_tags_list);
			$('#' + meta_tag_parent_container + ' #metaTagTotalSpan').text(data.total_meta_tags);
			$('#' + meta_tag_parent_container + ' #metaTagDisplayDiv').append(data.my_meta_tags_list);
		}
	});

}


function resizeMetaTagsDiv(parent_container, new_height, new_width){ 
	if(new_height != ''){
		$('#' + parent_container+' #attMetaTagsDi v').height(new_height);
	}
	if(new_width != ''){
		$('#' + parent_container+' #attMetaTagsDiv').width(new_width);
	}
}
//end meta tag mgmt scripts

function clearAttFilterFields(att_incident_id, command, handle){
	//alert{att_incident_id+'\n'+command+'\n'+handle); 
	if(command == 'clear_one'){
		var allInputs = $('#attTabsDiv'+att_incident_id+' #'+handle);
	}else if(command == 'clear_all'){
		var  allInputs = $('#attTabsDiv'+att_incident_id+'  #filterControlsSpan :input');
	}
 

	$(allInputs).each(function(){
		var myType = $(this).prop('type'); 
		var myId = $(this).prop('id');
		//alert(myType+' and '+myld);
		if(myType == 'select'  || myType == 'select-multiple'){
			$('#attTabsDiv'+att_incident_id+' #' + myId + ' option:selected').removeAttr('selected');
		}else if(myType == 'text'){
			$('#attTabsDiv'+att_incident_id+' #' + myId).val('');
		}else if(myType == 'textarea'){
			$('#attTabsDiv1'+att_incident_id+' #' + myId).html('');
		}
	});

}


//begin URL Reference scripts
function openAttUrlControls(att_incident_id){
	$('#attTabsDiv'+att_incident_id+' .attUrlCtrl').show();
}

function closeAttUrlControls(att_incident_id){
	$('#attTabsDiv'+att_incident_id+' #att_url').val('');
	$('#attTabsDiv'+att_incident_id+' .attUrlCtrl').hide();
}

function clearAttFeedbackDiv(att_incident_id){
	$('#attTabsDiv'+att_incident_id+' #attUrlFeedbackDiv').html('');
}

function attSaveUrl(att_incident_id){

	var att_url = $('#attTabsDiv'+att_incident_id+' #att_url').val{);
	//att_url = encodeURl(att_url);

	$.ajax{{
		url:'attMgmt/attSave.php', type: 'POST',
		data:{
			command: 'saveAttUrl', 
			att_incident_id: att_incident_id, 
			att_url: att_url
	},
	dataType: 'json', 
	success: function(data){
		//alert(data.is_url); 
		if(data.is_url == 'is_not_url'){
			$('#attTabsDiv'+att_incident_id+' #attUrlFeedbackDiv').html(data.feedback_msg);
		}else if(data.is_url == 'file_name_already_exists'){
			var urlOkBtn = '<input id="urlOkBtn" name="urlOkBtn" type="button"  class="button" value="OK" onclick="closeAttUrl Controls('+att_incident_id+'); clearAttFeedbackSpan('+att_incident_id+');" />';
			$('#attTabsDiv'+att_incident_id+' #attUrlFeedbackDiv').html(data.feedback_msg+' '+urlOkBtn);
		}else{
			$('#attTabsDiv'+att_incident_id+' #attUrlFeedbackDiv').html(data.feedback_msg); 
			closeAttUrlControls(att_incident_id);
			
			$('#attTabsDiv'+att_incident_id+' #filterAttlist').click(); 
			setTimeout( function(){
				clearAttFeedbackDiv(att_incident_id);
			},2000);
		}

	});
}
//end URL Reference scripts


//begin Edit Attachments
function editAttachment(att_incident_id, att_id){

	var att_file_name = $('#editAttFileName').val();
	var att_status = $('#editAttStatus option:selected').val();
	
	//alert('status: '+att_status+'\ncategory: '+au_category_id+'\nfile name: '+att_file_name);
	
	$.post('attMgmt/attSave.php',  { 
			command: 'editAttachment', 
			att_incident_id: att_incident_id, 
			att_id: att_id,
			att_status: att_status, 
			att_file_name:  att_file_name
	},
	function(returnedData){
		//alert(returnedData.final_file_name);
		$('#editAttFileName').val  (returnedData.final_file_name);
		$('#editAttMsgsDiv').html(returnedData.feedback_message);
		$('#reloadAttachmentsBtn').click();
		
		setTimeout(function(){
			$('#editAttachmentsDialogDiv').dialog('close');
		},3000
		);
	 
	
	},'json');

}
//end Edit Attachments





//begin Unprocessed Attachments

function selectAllUnprocessed{att_incident_id, command){
	//alert(command); 
	if(command == 'check_all'}{
		$('#attUnprocessedTable'+att_incident_id+' .unprocessedCheckBox').prop('checked', true};
	}else{
		$('#attUnprocessedTable'+att_incident_id+' .unprocessedCheckBox').prop('checked', false);
	}
	$('#unprocessedAttachmentFeedbackDiv'}.html('');
}

function checkUnprocessed{att_incident_id}{

	var checkedCount = 0;
	${'#attUnprocessedTable'+att_incident_id+' .unprocessedCheckBox').each(function(){ 
		var myStatus = ${this).is(':checked');
		if(myStatus == true){ 
			checkedCount++;
		}
	});

	//alert(checkedCount); 
	if(checkedCount > O){
		openUnprocessedAttachment(att_incident_id);
	}else{
		var okBtn = '<input id="unprocessedErrorOkBtn"  name="unprocessedErrorOkBtn"  type="button" class="button" style="margin:5px  0px 5px  0px;" value="OK"
onclick="$(\'#attTabsDiv'+att_incident_id+' #unprocessedAttachmentFeedbackDiv\').html(\'\');" />';
		var checkBoxMsg = '<div class="errorMsg" style="font-size: 14px;">You must <strong>select at least one</strong> attachment to continue.' + okBtn + '</div>';
		$('#attTabsDiv'+att_incident_id+' #unprocessedAttach mentFeedbackDiv').htmI(checkBoxMsg);
	}

}

function getUnprocessedFileNames(att_incident_id, list_output_target){ 
	var feedbackMsg = '';
	var fileNamelist = '';
 

	$('#attUnprocessedTable'+att_incident_id+' .unprocessedCheckBox').each(function(){ 
		var myStatus = $(this).is(':checked');
		var myld = $(this).attr('id'); 
		
		if( myStatus == true){
			var myFile = $('#attUnprocessedTable'+att_incident_id+' #'+myld).closest('td').next('td').next('td').text();
			feedbackMsg += 'ID is: '+myld+' and file name is: '+myFile+'\n'; 
			fileNamelist += '"'+myFile+'"';

	});
 
	fileNamelist.trim();
 

	//alert(fileNamelist);
	
	$.post('attMgmt/attUnprocessedlist.php',{ 
		   command: 'displayForProcessing', 
		   att_incident_id: att_incident_id, 
		   file_name_list: fileNamelist
	},
	function(returnedData){
	//alert(returnedData);
		$('#' + list_output_target).html(returnedData);
	});

}
//end Unprocessed Attachments

function reloadUnprocessed(att_incident_id, focus_dir){

//alert(att_incident_id);

	var attloadingMsg = '<div class="infoMsg incMsgText"> '+getSpinnerAtt()+' <strong>loading...</strong></div>';
	$('#attTabsDiv'+att_incident_id+' #attUnprocessedlistDiv').html(attloadingMsg);
	
	$.post('attMgmt/attUnprocessedlist.php',{ 
		   command: 'reloadUnprocessedlist', 
		   att_incident_id: att_incident_id, 
		   focus_dir: focus_dir
	},
	function(returnedData){
		$('#attTabsDiv'+att_incident_id+' #attUnprocessedlistDiv').html(returnedData);
	});

}

function toggleThemeName(){
 

	var isChecked = ${'#themeNameConfirm').is(':checked');
	//alert('is Checked '+isChecked); 
	if( isChecked === true ){
		$('#themeNameFieldDiv').show();
		$('.newNameSpan').hide();
	}else{
		$('#themeNameFieldDiv').hide();
		$('.newNameSpan').show();
		$('#theme_name').val ('');
	}

}

function saveUnprocessed(att_incident_id){

	$('#unprocessedMsgsDiv').html('');
	var error_msg = '';
	var unprocessed_file_location = $('#unprocessedFilelocation').text();

	var att_status = $('#att_status option:selected').val(); 
	if(att_status.length  < 1){
		error_msg = error_msg+'&bull; please select a Status for these attachments<br />';
		var use_theme_name = $('#themeNameConfirm').is(':checked'); 
		var theme_name = $('#att_theme_name').val();

		if(use_theme_name === true && theme_name.length < 1){
		error_msg = error_msg+'&bull; please enter a Theme Name for these attachments<br />';

		//alert(use_theme_name);

		var unprocessed_file_names = ''; 
		var blank_file_name_count = 0;
		
		$('.unprocessed FilesDiv').each(function(){
			var myCurrentName = $(this).find('.unprocessedCurrentName').text(); 
			if(use_theme_name === true){
				var myNewValue = theme_name;
				var myExt = myCurrentName.substr(myCurrentName.lastlndexOf('.')); 
				myNewValue = myNewValue+myExt;
			}else{
				var myNewValue = $(this).find('.unprocessedNewName').val();
				$(this).find(' .missingNameSpan').remove();
		 
			}
		
			if(use_theme_name === false && myNewValue.length < 1){
				$(this).find('.unprocessedNewName').closest('span').append('<span class="missingNameSpan" style="font-size:14px; font-weight:bold;  color:red;  padding-left:lOpx;">!</span>');
				blank_file_name_count++;
			}
			unprocessed_file_names += myCurrentName+', '+myNewValue+'; ';
			//alert('myCurrentName: '+myCurrentName+'\nmyNewValue:  '+myNewValue);
		});

		unprocessed_file_names = unprocessed_file_names.replace(/;\s*$/,"");	//	/ = begin & end of regex;, = comma; \s = whitespace chars (tab,space, etc); * = 0 or more;$ = end of string

		if(blank_file_name_count > O){
			error_msg = error_msg+'&bull;  please enter a New Name for each attachment<br />';
		}

		var unprocessed_att_meta_tags = '';
		
		$('#unprocessedFilesParentDiv .kvPairDiv').each(function(){

			var myKvKey = $(this).find('.kvKey').text();
			var myKvValue = $(this).find(' .kvValue').text(); 
			unprocessed_att_meta_tags += myKvKey+','+myKvValue+';';
		});
		
		if(unprocessed_att_meta_tags.length    > O){
		unprocessed_att_meta_tags = unprocessed_att_meta_tags.replace(/;  \s*$/,"");	//	/ = begin & end of regex;, = comma; \s = whitespace chars (tab,space, etc); * = O or more; $ = end of string
		}

		$('#unprocessedFilesParentDiv .kvPairDiv').html(''); 
		if(error_msg.length < 1){
			
			$.post('attMgmt/attSave.php',{ 
				command: 'saveUnprocessed', 
				att_incident_id: att_incident_id, 
				att_status: att_status,
				unprocessed_att_meta_tags: unprocessed_att_meta_tags, 
				unprocessed_file_names:   unprocessed_file_names, 
				unprocessed_file_locati on: unprocessed_file_location
			},
			function(data){
				//alert('here i am');
				//alert(data.completion_status); 
				if(data.completion_status === 'errors'){
					$('#unprocessedMsgsDiv').html(data.feedback_message);
				}else{
					$('#unprocessedMsgsDiv').html(data.feedback_message);
					$('#reloadUnprocessedBtn').click();
					$('#reloadAttachmentsBtn').click();

					setTimeout(function(){
						$('#editUnprocessedAttachmentsDi alogDiv').dialog('close');
					},2500);
				}
			},'json');



		}else{

			error_msg = '<span class="errorMsg" style="width: inherit; display:inline-block; padding:O Spx 0 Spx;">'+error_msg+'</span>';
			$('#unprocessedMsgsDi v').html(error_msg);
	}

}

function deleteAttachment(att_incident_id, att_id, att_category_id, att_file_name, unprocessed_file_location, command){

	if(confirm("Are you sure you want to delete this attachment?")){

		$.post("attMgmt/attSave.php", { 
				command: command, 
				att_incident_id: att_incident_id, 
				att_id: att_id,
				att_category_id:  
				att_category_id, 
				att_file_name:  
				att_file_name,
				unprocessed_file_location: unprocessed_file_location
		},
		function(returnedData){
			if(command == 'deleteAttachment'){
				$('#attTabsDiv'+att_incident_id+'  #reloadAttachmentsBtn').click();
			}else if(command == 'deleteUnprocessed'){
				$('#attTabsDiv'+att_incident_id+' #reloadUnprocessedBtn').click();
			}
		});
	
	}

}

function  updateAttachmentCounts(att_incident_id){

	var countSpanExists = $('#incidentTabsDiv'+att_incident_id+' #attCountSpan');
 
	if(typeof(countSpanExists)  != 'undefined' && countSpanExists.length  > 0 ){

		$.post('attMgmt/attUnprocessedlist.php',{ 
				command:  'getAttachmentCounts', 
				att_incident_id: att_incident_id
		},
		function(returnedData){

			$('#incidentTabsDiv'+att_incident_id+' #attCountSpan').text(returnedData);

		});
	}

}
