# update_repo_origin.sh

The impetus for this script was the relocation of Git Repositories from one domain to another.

Once the move was made, locally stored copies of those repositories would need to be pointed to the new domain.

This project had quite a few repositories, and the developers would be tasked to work on any or all of them.

Rather than asking the developers to re-point their local repos one at a time, I decided to provide them with a script that would automatically re-point all local repositories to the new domain.

If a local repo is found on the user's machine, the script will point it to its new origin.

If a local repo is NOT found on the user's machine, the script will skip it and move on to the next one.

Here is the comment block included in the script:
<pre>
# This script will run the `git remote set-url origin` command
# on the list of repositories in the `repos_to_update` array
# 
# It will allow the user to:
#     preview the list of repos to be updated
#     update or revert those repos
# 
# To use, place this script in the directory where
# the user's local repositories are stored
# then run the script in a terminal window that supports bash scripts
# 
# Sam Mikitarian
# 02 June 2022
</pre>
  
Here is a screenshot of the script's main menu:  
<img src="./main_menu.jpg" height="250"/>