### End User Management Server Installation

from [https://docs.appdynamics.com/appd/onprem/23.1/en/eum-server-deployment](https://docs.appdynamics.com/appd/onprem/23.1/en/eum-server-deployment)

**EUM Server Overview**
The EUM Server receives data from EUM agents, processes and stores that data, and makes it available to the AppDynamics Controller. Certain EUM features—specifically, Browser Request Analytics and Mobile Request Analytics, features of Application Analytics that extend the functionality of Browser and Mobile Analyze—require access to the AppDynamics Events Service.

##### Determine EUM Version Compatibility with other AppDynamics Components

from [https://docs.appdynamics.com/appd/onprem/23.1/en/planning-your-deployment/installation-and-upgrade-overview#id-.PlanningYourDeploymentv23.1-platform-compatibility](https://docs.appdynamics.com/appd/onprem/23.1/en/planning-your-deployment/installation-and-upgrade-overview#id-.PlanningYourDeploymentv23.1-platform-compatibility)

* EUM Server version 21.4.6 is compatible with Controller version 23.1.1 or beyond.

**Get Version of AppDynamics Components via the CLI:**

Log in to the server via terminal and `su appdynamics`
Run the following commands to get version numbers by component:


| Component          | Command                                                                              | Output                                                      |
| :------------------- | -------------------------------------------------------------------------------------- | ------------------------------------------------------------- |
| Enterprise Console | platform-admin.sh show-platform-admin-version                                        | Enterprise Console Application version: 23.11.1-10113 build |
| Controller         | platform-admin.sh get-available-versions --platform-name CIRRAS --service controller | Versions available: [23.11.1-10118 (latest)] |
| Events Service     | platform-admin.sh get-available-versions --platform-name CIRRAS --service events-service | Versions available: [23.10.0-37 (latest)] |  
  
  
**NOTE:**  In our architecture, the EUM Server is intended to be installed on the same server as Enterprise Console, Controller and Events Service.  However, it can be installed on another server if necessary.
  
### Further instructions to follow once we get licensed for this component  

In the meantime, see the following documents:  
[https://docs.appdynamics.com/appd/onprem/23.1/en/eum-server-deployment/eum-server-requirements](https://docs.appdynamics.com/appd/onprem/23.1/en/eum-server-deployment/eum-server-requirements)
[https://docs.appdynamics.com/appd/onprem/23.1/en/eum-server-deployment/install-a-production-eum-server](https://docs.appdynamics.com/appd/onprem/23.1/en/eum-server-deployment/install-a-production-eum-server)



<br/>  
<br/>  

### Useful Commands  

**List Services in designated platform:**  
```
[appdynamics@ip-10-205-199-41 bin]$ platform-admin.sh list-supported-services --platform-name CIRRAS
  
Services supported by the current platform:
        Controller (controller)
        Events Service (events-service)
```  
  
