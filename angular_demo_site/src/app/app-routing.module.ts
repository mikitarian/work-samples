import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BreedsComponent } from './components/breeds/breeds.component';
import { UserListComponent } from './components/user-list/user-list.component';
// import { DataLoadToFormComponent } from './components/data-load-to-form/data-load-to-form.component';
import { HomeComponent } from './components/home/home.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
// import { MapTutorialComponent } from './components/map-tutorial/map-tutorial.component';
import { RatingsComponent } from './components/ratings/ratings.component';
// import { ReactiveFormComponent } from './components/reactive-form/reactive-form.component';
// import { TemplateDrivenFormComponent } from './components/template-driven-form/template-driven-form.component';
import { VeterinariansComponent } from './components/veterinarians/veterinarians.component';

const routes: Routes = [
  { path: 'login', component: LoginFormComponent },
  { path: 'breeds', component: BreedsComponent },
  { path: 'ratings', component: RatingsComponent },
  { path: 'veterinarians', component: VeterinariansComponent },
  { path: 'user-list', component: UserListComponent},
  // { path: 'template-driven-form', component: TemplateDrivenFormComponent },
  // { path: 'reactive-form', component: ReactiveFormComponent },
  // { path: 'data-load-to-form', component: DataLoadToFormComponent },
  // { path: 'map-tutorial', component: MapTutorialComponent },
  { path: '', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],

  exports: [RouterModule]
})
export class AppRoutingModule { }
