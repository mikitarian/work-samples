from app_defaults import *
from controllers.datatools import *
from controllers.ntctaskgrid import *
from controllers.ntclisteditor import *
from controllers.ntcreorderlist import *

class NtcMain(BoxLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    #-------- BEGIN Class Vars --------#
    ok_button_callback_command = 'dismiss'  #General - default value for calling different functions from the ok_button on the ntc_ok_cancel panel
    original_task_list_file_name = ''       #General - gets set each time a Task List is selected from the dropdown
    previous_task_list_dir = ''             #General - provides user feedback during renaming of file
    current_task_list = []                  #General - so we can grab from the same data object from anywhere in the program
    current_task_index = ''                 #General - so we can grab a specific task from self.current_task_list
    dupe_task_sequence = ''                 #General - so user can precisely place a duplicated task into target list
    current_dropdown_list = ''              #General - so we can grab the same dropdown list from anywhere in the code
    error_msg = ''                          #General 
    feedback_msg = ''                       #General

    edit_existing_task_flag = False          #Edit Task - used to control field behavior in task-create vs edit mode
    task_status_val = ''            #Edit Task - from self.ids.task_status.spinner.text
    task_name_val = ''              #Edit Task - from self.ids.task_name.text
    task_type_val = ''              #Edit Task - from self.ids.task_type_spinner.text
    task_format_val = ''            #Edit Task - from self.ids.task_format_spinner.text
    task_format_value_val = ''      #Edit Task - from self.ids.task_format_value.text
    ip_val = ''                     #Edit Task - from self.ids.ip_address_value.text
    uname_val = ''                  #Edit Task - from self.ids.username_value.text
    pwd_val = ''                    #Edit Task - from self.ids.password_value.text
    
    powershell_script_extensions = ['.ps1','.psd1','.psm1','.ps1xml']
                                    #Edit Task - to validate PowerShell path+script string
                                    #Taken from: https://docs.microsoft.com/en-us/powershell/scripting/components/ise/how-to-write-and-run-scripts-in-the-windows-powershell-ise?view=powershell-6
    powershell_script_extensions_as_string = ', '.join(powershell_script_extensions)

    column_widths = []              #Task Grid - uses windowWidth as basis to set column widths

    #-------- END Class Vars --------#


    #-------- BEGIN Page Set-up --------#
    def add_headers_to_task_grid(self):

        task_grid_width = windowWidth * .95 #self.ids.task_scroll_view.size[0]
        self.column_widths = [task_grid_width * .025, 
                            task_grid_width * .08,
                            task_grid_width * .135, 
                            task_grid_width * .10, 
                            task_grid_width * .13,
                            task_grid_width * .15,
                            task_grid_width * .058,
                            task_grid_width * .058,
                            task_grid_width * .058]

        task_list_headers = ['#','Status','Task Name','Type','Format','Value','IP','User','Pwd']

        column_width_index = 0
        for i in task_list_headers:

            item_label = NtcTaskHeaderLabel(text=i.strip(),width=self.column_widths[column_width_index])
            self.ids.view_task_header_label_box.add_widget(item_label)
            column_width_index += 1

    #-------- END Page Set-up --------#


    #-------- BEGIN Common Tools functions --------#
    def set_feedback_message(self, message_type, message_string, message_styling, time_out):

        self.clear_feedback_message()   # clear previous message

        if message_type == 'error':
            msg_format = error_text_color
        elif message_type == 'feedback':
            msg_format = feedback_text_color

        self.ids.feedback_message_box.text=str(message_string)
        self.ids.feedback_message_box.color=msg_format

        if message_styling == 'large':
            self.ids.feedback_message_box.font_size=large_font_size
            self.ids.feedback_message_box.text='[b]'+ str(message_string) + '[/b]'

        if time_out > 0:
            Clock.schedule_once(lambda a:self.clear_feedback_message(), time_out)


    def clear_feedback_message(self):

        self.ids.feedback_message_box.text=''
        self.ids.feedback_message_box.color=''


    def ntc_ok_cancel_box(self, message_type, message_styling, time_out, command, info_string):

        # message_type default is 'ok_cancel'
        # message_type of 'ok_only' removes Cancel button and centers OK button
        # time_out default is 0, larger than 0 intended for auto-close of "ok_only" messages

        # initialize the new dialog here so we can add widgets to it based on the command
        self.ntc_dialog = NtcOkCancel()

        if command == 'rename_task_list':
            dialog_title='Confirm Renaming of Task List ...'
            dialog_message = 'You are about to rename Task List\n"'+self.original_task_list_file_name+'" to "'+info_string+'".\n\nIs this really what you want to do?'
            dialog_text_color = error_text_color
            self.ok_button_callback_command = 'rename_task_list'
        elif command == 'rename_task_list_incomplete':
            dialog_title='Task List Renaming is Incomplete...'
            dialog_message = 'You were about to rename Task List\n"'+self.original_task_list_file_name+'" to "'+info_string+'".\n\nClick "OK" to finish renaming the file.\nClick "Cancel" to return to the main screen.'
            dialog_text_color = error_text_color
            self.ok_button_callback_command = 'rename_task_list_incomplete'
        elif command == 'delete_task_list_prompt':
            dialog_title = 'Deleting Task List...'
            dialog_message = 'You are about to delete Task List "'+info_string+'".\nIs this really what you want to do?'
            dialog_text_color = error_text_color
            self.ok_button_callback_command = 'delete_task_list_now'
        elif command == 'loading_task_list':
            dialog_title = 'Loading Task List...'
            dialog_message = 'Loading Task List...please wait'
            dialog_text_color = feedback_text_color
            self.ok_button_callback_command = ''
        elif command == 'dupe_prompt':
            self.task_to_dupe = json.loads(self.current_task_list[info_string])
            task_num = info_string + 1

            message_styling = 'large'
            dialog_title = 'Duplicate Task'
            dialog_message = 'Duplicate Task '+str(task_num)+': "'+self.task_to_dupe["task_name"]+'"\n\nEnter a Sequence Number below to locate duplicate task in the list.\nLeave the field [u]blank[/u] to append it to the [u]end of the list[/u].'

            self.sequence_box = NtcDuplicateTaskBox()
            self.sequence_box.ids.dupe_task_sequence.bind(on_text=lambda a:self.set_dupe_task_sequence())
            self.ntc_dialog.ids.ntc_popup_content_box.add_widget(self.sequence_box)

            dialog_text_color = feedback_text_color
            self.ok_button_callback_command = 'save_duplicated_task'
        elif command == 'bad_dupe_seq_num':
            dialog_title = 'Duplicate Task'
            dialog_message = 'Text and Negative Numbers not allowed as Sequence Number\nwhen duplicating a Task.\n\nPlease enter a Number or leave the field [u]blank[/u].'

            dialog_text_color = error_text_color
            self.ok_button_callback_command = ''
        elif command == 'delete_task_prompt':
            self.task_to_delete = json.loads(self.current_task_list[info_string])
            self.current_task_index = info_string
            task_num = info_string + 1

            dialog_title = 'Delete Task'
            dialog_message = '[b]You are about to DELETE\nTask '+str(task_num)+': "'+self.task_to_delete["task_name"]+'"\n\nIs this really what you want to do?[/b]'
            dialog_text_color = error_text_color
            self.ok_button_callback_command = 'delete_task'
        elif command == 'select_script_path':
            dialog_title = 'Select a Script Path...'
            dialog_message = 'You must select or enter a Script Path in order to save.'
            dialog_text_color = error_text_color
            self.ok_button_callback_command = ''
        elif command == 'delete_list_item_prompt':
            # self.target_list, list_item_index, self.list_contents[list_item_index]
            self.list_item_to_delete = info_string[1]
            if str(info_string[0]) == str(script_path_file_name):
                dialog_title = 'Delete Script Path...'
                dialog_message = '[b]You are about to DELETE\nScript Path '+str(info_string[1] + 1)+': "'+info_string[2]+'"\n\nIs this really what you want to do?[/b]'
            elif str(info_string[0]) == str(ip_address_file_name):
                dialog_title = 'Delete IP Address...'
                dialog_message = '[b]You are about to DELETE\nIP Address: '+str(info_string[1] + 1)+': "'+info_string[2]+'"\n\nIs this really what you want to do?[/b]'

            dialog_text_color = error_text_color
            self.ok_button_callback_command = 'delete_list_item'
            
        else:
            dialog_title = 'Unassigned Command...'
            dialog_message = 'Unassigned Command is: '+command
            dialog_text_color = feedback_text_color
            self.ok_button_callback_command = ''

        self.ntc_dialog.title=dialog_title
        self.ntc_dialog.ids.ntc_popup_label.color=dialog_text_color
        if message_styling == 'large':
            self.ntc_dialog.ids.ntc_popup_label.font_size=large_font_size
            self.ntc_dialog.ids.ntc_popup_label.text='[b]'+ dialog_message + '[/b]'
        else:
            self.ntc_dialog.ids.ntc_popup_label.text=dialog_message

        self.ntc_dialog.ids.ntc_popup_ok.bind(on_press=lambda a:self.ok_button_callback())
        if message_type == 'ok_only':
            self.ntc_dialog.ids.ntc_popup_button_box.remove_widget(self.ntc_dialog.ids.ntc_popup_cancel)
            self.ntc_dialog.ids.ntc_popup_ok.pos_hint={'center_x': .5, 'center_y': .5}
        self.ntc_dialog.open()

        if time_out > 0:
            Clock.schedule_once(self.dismiss_popup, time_out)


    def dismiss_popup(self):
        if isinstance(App.get_running_app().root_window.children[0], Popup):
            App.get_running_app().root_window.children[0].dismiss()


    def ok_button_callback(self):
        
        global edit_list_popup

        if self.ok_button_callback_command == 'rename_task_list':
            self.save_task_list('rename')

        elif self.ok_button_callback_command == 'rename_task_list_incomplete':
            self.save_task_list('rename')
            self.toggle_edit_task_controls('disable')

        elif self.ok_button_callback_command == 'delete_task_list_now':
            self.delete_task_list('delete_task_list_now')

        elif self.ok_button_callback_command == 'save_duplicated_task':
            self.dupe_task_sequence = self.sequence_box.ids.dupe_task_sequence.text
            if len(self.dupe_task_sequence) > 0 and self.dupe_task_sequence.isnumeric() == False:
                self.ntc_ok_cancel_box('ok_only', 'large', 0, 'bad_dupe_seq_num', '')
            else:
                self.save_task('duplicate_task')

        elif self.ok_button_callback_command == 'delete_task':
            self.delete_task()

        elif self.ok_button_callback_command == 'delete_list_item':
            App.get_running_app().root_window.children[1].delete_list_item(self.list_item_to_delete)
            # root_window.children[1] = edit_list_popup instance of NtcListEditor            

    def open_list_editor_popup(self, target_list):
        if target_list == str(script_path_file_name):
            edit_list_popup_title = 'Edit Script Paths'
        elif target_list == str(ip_address_file_name):
            edit_list_popup_title = 'Edit IP Addresses'
        else:
            edit_list_popup_title = 'Edit Dropdown List'

        self.current_dropdown_list = target_list

        self.edit_list_popup = NtcListEditor(str(self.current_dropdown_list))
        self.edit_list_popup.title=edit_list_popup_title
        self.edit_list_popup.open()


    def close_list_editor_popup(self):
        self.edit_list_popup.dismiss()
        if str(self.current_dropdown_list) == str(script_path_file_name):
            self.update_task_format_value_controls('Script File')
        elif str(self.current_dropdown_list) == str(ip_address_file_name):
            self.get_ip_address_list()
        self.current_dropdown_list = ''


    def open_reorder_list_popup(self):
        target_list = self.ids.task_list_file_spinner.text
        # target_list = 'test.tasklist.json'
        self.reorder_list_popup = NtcReorderList(target_list)
        self.reorder_list_popup.open()

    def close_reorder_list_popup(self):
        self.reorder_list_popup.dismiss()
        self.ids.task_grid.clear_widgets()
        # self.display_tasks()
        self.get_tasks()

    #-------- END Common Tools functions --------#


    #-------- BEGIN Select Task List Directory functions --------#
    def task_list_directory_selection(self, directory, filename):
        if os.path.dirname(filename) != "\\":
            self.ids.selected_path.text = os.path.dirname(filename)

        onlyfiles = next(os.walk(directory))[2] #directory is the directory path as string

        tasklist_files = []

        if len(onlyfiles) > 0:
            for file in onlyfiles:
                if file.endswith(".tasklist.json"):
                    tasklist_files.append(file.rstrip('\n'))

        if len(tasklist_files) > 0:
            tasklist_files.insert(0,'Add New Task List')
            tasklist_files.insert(0,'')

            self.ids.task_list_file_spinner.values=tasklist_files

            # keeps the spinner and the file name field populated after save
            selected_filename = ''
            if len(self.original_task_list_file_name) > 0:
                # self.ids.task_list_file_spinner.text
                selected_filename=self.original_task_list_file_name+'.tasklist.json'
            
            if len(selected_filename) > 0:
                self.ids.task_list_file_spinner.text=selected_filename
            else:
                self.ids.task_list_file_spinner.text='Available Task Lists ('+str(len(tasklist_files)-2)+'):'

        else:
            self.ids.task_list_file_spinner.text='Available Task Lists (0):'
            self.ids.task_list_file_spinner.values={'Add New Task List'}


    def get_selected_task_list(self, value, command):
        self.clear_tasks_from_grid()
        self.clear_task_fields()
        self.set_feedback_message('feedback','','',0)
        if self.ids.edit_tasks_button.disabled == False:
            self.toggle_edit_task_controls('disable')
        
        if value.endswith(".tasklist.json") == True:
            self.ids.task_list_name.text=value.replace(".tasklist.json","")
            self.original_task_list_file_name=value.replace(".tasklist.json","")
            self.ids.task_list_name.disabled=False

            self.toggle_task_list_buttons('rename_task_list')
            self.get_tasks()

        elif value =='Add New Task List':
            self.ids.task_list_name.text=''
            self.original_task_list_file_name=''
            self.ids.task_list_name.hint_text='Enter new Task List name'
            self.ids.task_list_file_spinner.text=''
            self.ids.task_list_name.disabled=False

            self.toggle_task_list_buttons('add_new_task_list')


    def toggle_task_list_buttons(self, command):
        
        if command == 'cancel_task_list_edits':
            
            try:
                self.save_task_list_button
                self.delete_task_list_button
                self.cancel_task_list_edits_button
            except AttributeError:
                pass
            else:
                self.ids.edit_task_list_box.remove_widget(self.save_task_list_button)
                self.ids.edit_task_list_box.remove_widget(self.delete_task_list_button)
                self.ids.edit_task_list_box.remove_widget(self.cancel_task_list_edits_button)

                self.enable_task_fields('')
                self.set_feedback_message('feedback','','',0)

        elif command == 'only_remove_buttons':

            try:
                self.save_task_list_button
                self.delete_task_list_button
                self.cancel_task_list_edits_button
            except AttributeError:
                pass
            else:
                self.ids.edit_task_list_box.remove_widget(self.save_task_list_button)
                self.ids.edit_task_list_box.remove_widget(self.delete_task_list_button)
                self.ids.edit_task_list_box.remove_widget(self.cancel_task_list_edits_button)

        elif command == 'enable_buttons':
            self.save_task_list_button.disabled=False
            self.delete_task_list_button.disabled=False
            self.cancel_task_list_edits_button.disabled=False

            self.save_task_list_button.background_color=buttons_and_controls_color
            self.delete_task_list_button.background_color=buttons_and_controls_color
            self.cancel_task_list_edits_button.background_color=buttons_and_controls_color

        elif command == 'disable_buttons':
            self.save_task_list_button.disabled=True
            self.delete_task_list_button.disabled=True
            self.cancel_task_list_edits_button.disabled=True

            self.save_task_list_button.background_color=disabled_controls_color
            self.delete_task_list_button.background_color=disabled_controls_color
            self.cancel_task_list_edits_button.background_color=disabled_controls_color

        else:
            
            if command == 'add_new_task_list':
                save_task_list_button_text = 'Save'
                save_task_list_command = 'save_new'
            elif command == 'rename_task_list':
                save_task_list_button_text = 'Rename'
                save_task_list_command = 'rename'

            self.save_task_list_button = NtcSaveTaskListButton(text=save_task_list_button_text)
            self.save_task_list_button.bind(on_release=lambda a:self.validate_save_task_list(save_task_list_command))
            self.ids.edit_task_list_box.add_widget(self.save_task_list_button)
            
            self.delete_task_list_button = NtcDeleteTaskListButton()
            self.delete_task_list_button.bind(on_release=lambda a:self.delete_task_list('delete_task_list_prompt'))
            self.ids.edit_task_list_box.add_widget(self.delete_task_list_button)
            
            self.cancel_task_list_edits_button = NtcCancelTaskListEditsButton()
            self.cancel_task_list_edits_button.bind(on_release=lambda a:self.cancel_task_list_edits())
            self.ids.edit_task_list_box.add_widget(self.cancel_task_list_edits_button)
            

    def reset_file_chooser_to_root(self):
        self.previous_task_list_dir = self.ids.selected_path.text
        self.ids.task_list_file_chooser.path="\\"
        self.ids.selected_path.text=''
        if str(self.ids.edit_tasks_button.text) == str('Add Task'):
            self.ids.edit_tasks_button.disabled=True


    def file_chooser_to_previous_dir(self):
        target = self.previous_task_list_dir
        self.ids.task_list_file_chooser.path=target

    #-------- END Select Task List Directory functions --------#


    #-------- BEGIN Task List functions --------#
    def clear_task_list_fields(self):
        self.ids.task_list_name.text=''
        self.ids.task_list_name.hint_text=''
        self.original_task_list_file_name=''
        self.ids.task_list_file_spinner.text=''


    def get_task_list_file(self):

        task_list_data = []
        path = self.ids.selected_path.text
        filename = self.ids.task_list_name.text
        fullname = path + '/' + filename + '.tasklist.json'

        try:            # check if file exists
            if os.path.exists(fullname):
                task_list_data.insert(0, path)
                task_list_data.insert(1, filename)
                task_list_data.insert(2, fullname)
                task_list_data.insert(3, 'file_exists')
            else:
                task_list_data.insert(0, path)
                task_list_data.insert(1, filename)
                task_list_data.insert(2, fullname)
                task_list_data.insert(3, 'file_not_found')
        except IndexError:
            task_list_data.insert(0, path)
            task_list_data.insert(1, filename)
            task_list_data.insert(2, fullname)
            task_list_data.insert(3, 'file_not_found')

        return task_list_data


    def validate_save_task_list(self, command):

        # clear previous messages
        self.set_feedback_message('feedback','','',0)
        self.error_msg = ''

        task_list_to_validate = self.get_task_list_file()

        # if path is NOT OK, display error message
        if task_list_to_validate[0] == "\\" or len(task_list_to_validate[0]) < 1:
            self.error_msg='Please select a Task List Directory (above) before saving.\n(Location of "'+task_list_to_validate[0]+'" is not allowed.)'
            self.set_feedback_message('error', self.error_msg,'',0)

        # if path is OK (error_msg is '') but filename is blank, print error message
        if len(self.error_msg) < 1 and len(task_list_to_validate[1]) < 1:   
            self.error_msg='Please enter a Task List Name.\n'
            self.set_feedback_message('error', self.error_msg,'',0)
        
        # if path is OK, and file exists(error_msg is ''), prompt for rename
        elif command == 'rename': #len(self.error_msg) < 1 and task_list_to_validate[3] == 'file_exists':
            if task_list_to_validate[1] == self.original_task_list_file_name:
                self.error_msg = 'Please change the Task List Name before saving.'
                self.set_feedback_message('error', self.error_msg,'',0)
            else:
                self.ntc_ok_cancel_box('ok_cancel','',0,'rename_task_list', task_list_to_validate[1])

            self.error_msg='stop'

        # if path is OK, file is not found, but filename has been filled-in (error_msg is ''), go save the new file
        elif len(self.error_msg) < 1  and task_list_to_validate[3] == 'file_not_found':
            self.save_task_list('save_new')


    def save_task_list(self, command):

        task_list_to_save = self.get_task_list_file()

        if command == 'save_new':
            tasklist = open(task_list_to_save[2], "a")
            self.feedback_msg=task_list_to_save[1]+'.taskslist has been saved\n'
        elif command == 'rename':
            original_file = task_list_to_save[0] + '/' + self.original_task_list_file_name + '.tasklist.json'
            new_file = task_list_to_save[0] + '/' + task_list_to_save[1] + '.tasklist.json'
            os.rename(original_file, new_file)
        
            self.feedback_msg=self.original_task_list_file_name+' has been renamed to '+task_list_to_save[1]+'\n'

        # os.chmod(task_list_to_save[0] + '/' + task_list_to_save[1] + '.tasklist.json', 0o664
        self.set_feedback_message('feedback',self.feedback_msg,'',5)
        self.cancel_task_list_edits()
        self.ids.selected_path.text=task_list_to_save[0]
        self.file_chooser_to_previous_dir()
        self.ids.task_list_file_spinner.text=task_list_to_save[1] + '.tasklist.json'


    def delete_task_list(self, command):

        task_list_to_delete = self.get_task_list_file()

        if len(task_list_to_delete) > 0:
            if command == 'delete_task_list_prompt':
                self.ntc_ok_cancel_box('ok_cancel','',0,'delete_task_list_prompt',task_list_to_delete[1])

            if command == 'delete_task_list_now':
                # os.chmod(task_list_to_delete[2], 0o664)
                os.remove(task_list_to_delete[2])

                self.feedback_msg=task_list_to_delete[1]+'.taskslist has been deleted'
                self.ids.feedback_message_box.text=self.feedback_msg
                self.ids.feedback_message_box.color=feedback_text_color

                Clock.schedule_once(lambda a:self.clear_feedback_message(), 5)
                self.clear_task_list_fields()
                self. reset_file_chooser_to_root()
                self.file_chooser_to_previous_dir()


    def cancel_task_list_edits(self):
        self.previous_task_list_dir = self.ids.selected_path.text
        self.toggle_task_list_buttons('cancel_task_list_edits')
        self.clear_task_list_fields()
        # self.task_list_directory_selection(self.previous_task_list_dir,'')

    #-------- END Task List functions --------#


    #-------- BEGIN Task functions --------#

    def edit_tasks_callback(self):
        # check if file rename is complete
        # if NOT, then prompt user
        if self.ids.task_list_name.text != self.original_task_list_file_name:
            self.ntc_ok_cancel_box('ok_cancel','', 0, 'rename_task_list_incomplete', self.ids.task_list_name.text)
        elif self.ids.edit_tasks_button.text == 'Cancel Edits':
            self.ids.task_name.text=''
            self.toggle_common_controls('disable_fields')
            self.toggle_remote_only_controls('disable_fields')
            self.toggle_edit_task_controls('disable')
        else:   # if complete, then disable Task List buttons and enable Edit Task controls
            self.toggle_edit_task_controls('enable')
        

    def toggle_task_type_spinner(self, text):
        if len(text) > 0:
            self.ids.task_type_spinner.disabled=False
            self.ids.task_type_spinner.background_color=buttons_and_controls_color
            if self.edit_existing_task_flag == False:
                self.ids.task_type_spinner.text=''
        elif len(text) < 1 and self.edit_existing_task_flag == False:
            self.ids.task_type_spinner.disabled=True
            self.ids.task_type_spinner.background_color=disabled_controls_color
            self.ids.task_type_spinner.text='[i](disabled)[/i]'

    def toggle_edit_task_controls(self,command):
        if command == 'enable':
            self.toggle_task_list_buttons('disable_buttons')
            self.ids.edit_tasks_button.text='Cancel Edits'
            self.ids.task_status_spinner.disabled=False
            self.ids.task_status_spinner.background_color=buttons_and_controls_color
            self.ids.task_name.disabled=False

        elif command == 'disable':
            self.toggle_task_list_buttons('enable_buttons')
            self.ids.edit_tasks_button.text='Add Task'
            self.ids.task_status_spinner.disabled=True
            self.ids.task_status_spinner.background_color=disabled_controls_color
            self.ids.task_name.disabled=True


    def enable_task_fields(self, text):
        renaming_a_file = self.ids.task_list_file_spinner.text.endswith(".tasklist.json")
        if len(text) > 0 or renaming_a_file == True:
            self.ids.task_list_name.disabled=False
            self.ids.edit_tasks_button.disabled=False
            self.ids.edit_tasks_button.background_color=buttons_and_controls_color
        else:
            self.ids.task_list_name.disabled=True
            self.ids.edit_tasks_button.disabled=True
            self.ids.edit_tasks_button.background_color=disabled_controls_color


    def get_selected_list_file(self, target_list, selected_text):
        if selected_text == '[i](Edit List)[/i]':
            self.open_list_editor_popup(target_list)
        
        if str(target_list) == 'data_model\\scriptpaths' and selected_text != '[i](Edit List)[/i]' and len(selected_text) > 0:
            self.ids.task_format_value.text=selected_text


    def update_task_variables(self, value_to_update):
        if value_to_update == 'task_status_val':
            self.task_status_val = self.ids.task_status_spinner.text
        
        if value_to_update == 'task_name':
            self.task_name_val = self.ids.task_name.text

        if value_to_update == 'task_type_val':
            self.task_type_val = self.ids.task_type_spinner.text

        elif value_to_update == 'task_format_val':
            self.task_format_val = self.ids.task_format_spinner.text

        elif value_to_update == 'task_format_value_val':
            self.task_format_value_val = self.ids.task_format_value.text

        elif value_to_update == 'ip_val':
            self.ip_val = self.ids.ip_address_value.text

        elif value_to_update == 'uname_val':
            self.uname_val = self.ids.username_value.text

        elif value_to_update == 'pwd_val':
            self.pwd_val = self.ids.password_value.text


    def update_task_format_value_controls(self, selected_text):
        if selected_text == 'Command String' or selected_text == '-1':
            self.ids.task_format_value_label.text='Command String'

            try:
                self.script_path_list_spinner
                self.script_path_info_label
                self.task_format_value
            except AttributeError:
                pass
            else:
                self.ids.edit_task_parent_box.remove_widget(self.script_path_list_spinner)
                self.ids.edit_task_parent_box.remove_widget(self.script_path_info_label)
                self.ids.task_format_value.text=''
                self.ids.edit_task_parent_box.remove_widget(self.task_format_value)

            self.ids.task_format_value.size=(580,75)
            self.ids.task_format_value.pos_hint={'x': .19, 'y': .43}
            self.ids.task_format_value.text_size=(580,75)

        elif selected_text == 'Script File':
            self.ids.task_format_value_label.text='Select Path, and [b][i]append[/i][/b] Script File Name [b][i]below[/i][/b], or...'

            self.script_path_list_spinner = NtcScriptPathSpinner()
            self.script_path_list_spinner.bind(text=lambda a,b: self.get_selected_list_file(script_path_file_name, self.script_path_list_spinner.text))
            script_paths = SpinnerListInfo.get_list_contents(self,script_path_file_name,True)
            self.script_path_list_spinner.values=script_paths

            self.script_path_info_label = NtcScriptPathInfoLabel()
            self.task_format_value = NtcTaskFormatValueTextInput()

            self.ids.edit_task_parent_box.add_widget(self.script_path_list_spinner)
            self.ids.edit_task_parent_box.add_widget(self.script_path_info_label)
            self.ids.edit_task_parent_box.add_widget(self.task_format_value)

            self.ids.task_format_value.size=(580,45)
            self.ids.task_format_value.pos_hint={'x': .19, 'y': .4}
            self.ids.task_format_value.text_size=(580,45)

        else:
            self.ids.task_format_value_label.text='Task Format Value'


    def toggle_edit_task_fields(self, task_type_spinner_text):
        
        if len(task_type_spinner_text) > 0 and task_type_spinner_text != '[i](disabled)[/i]':
            self.ids.task_format_spinner.text=''

        if task_type_spinner_text == 'Local' or task_type_spinner_text == 'PowerShell':
            self.ids.task_format_spinner.values=['','Command String','Script File']
            self.toggle_common_controls('enable_fields')
            self.toggle_remote_only_controls('disable_fields')
        elif task_type_spinner_text == 'Remote':
            self.ids.task_format_spinner.values=['','Command String']
            self.toggle_common_controls('enable_fields')
            self.toggle_remote_only_controls('enable_fields')
            self.ids.task_format_spinner.text='Command String'
        elif len(task_type_spinner_text) == 0 or task_type_spinner_text == '[i](disabled)[/i]':
            self.ids.task_format_spinner.values=['','Command String','Script File']
            self.toggle_common_controls('disable_fields')
            self.toggle_remote_only_controls('disable_fields')


    def toggle_common_controls(self, command):
        common_controls = ['task_type_spinner','task_format_spinner','task_format_value','save_task_button','clear_task_button']
        # 'task_status_spinner',
        for control_index, control in enumerate(common_controls):
            if command == 'enable_fields':
                evaled_control = eval('self.ids.' + control)
                evaled_control.disabled=False

                if control != 'task_format_value':
                    evaled_control.background_color=buttons_and_controls_color

                if control == 'task_format_spinner':
                    evaled_control.text=''

            elif command == 'disable_fields':
                evaled_control = eval('self.ids.' + control)
                evaled_control.disabled=True

                if control != 'task_format_value':
                    evaled_control.background_color=disabled_controls_color
                else:
                    evaled_control.text=''

                if control == 'task_format_spinner':
                    evaled_control.text='[i](disabled)[/i]'

        self.update_task_format_value_controls('-1')
        self.ids.task_format_value_label.text='Task Format Value'

    def toggle_remote_only_controls(self, command):

        remote_only_controls = ['ip_address_spinner','ip_address_value','username_value','password_value']
        text_inputs = ['task_format_value','ip_address_value','username_value','password_value']

        for control_index, control in enumerate(remote_only_controls):
            if command == 'enable_fields':
                evaled_control = eval('self.ids.' + control)
                evaled_control.disabled=False

                if control not in text_inputs:
                    evaled_control.background_color=buttons_and_controls_color

                if control == 'task_format_spinner':
                    evaled_control.text=''

            elif command == 'disable_fields':
                evaled_control = eval('self.ids.' + control)
                evaled_control.disabled=True

                # if control != 'task_format_value':
                if control not in text_inputs:
                    evaled_control.background_color=disabled_controls_color

                if control == 'task_format_spinner':
                    evaled_control.text='[i](disabled)[/i]'

            self.get_ip_address_list()

    def get_ip_address_list(self):
        ip_addresses = SpinnerListInfo.get_list_contents(self,ip_address_file_name,True)
        self.ids.ip_address_spinner.values=''
        self.ids.ip_address_spinner.values=ip_addresses

        # TODO:  Get this to work so dropdown is resized around longest list item
        # longest_item = max(len(x) for x in ip_addresses)
        # new_width = round(longest_item * 6.9)
        # print(longest_item)
        # print(new_width)
        # if new_width < 300:
        #     self.ids.ip_address_spinner.option_cls.size=(225,25)
        #     self.ids.ip_address_spinner.option_cls.text_size=(225,25)
        #     self.ids.ip_address_spinner.dropdown_cls.width=225


    def autofill_ip_address(self, selected_text):
        if str(selected_text) != '[i](Edit List)[/i]' and len(selected_text) > 0:
            self.ids.ip_address_value.text=str(selected_text)
        else:
            self.ids.ip_address_value.text=''

    def validate_save_task(self):
        error_msg = ''
        
        if len(self.ids.task_type_spinner.text) < 1:
            error_msg = 'You must select a [b]Task Type[/b] in order to save.'
        
        elif len(self.ids.task_format_spinner.text) < 1:
            error_msg = 'You must select a [b]Task Format[/b] in order to save.'
        
        elif len(self.ids.task_format_value.text) < 1:
            error_msg = 'You must enter a [b]'+self.ids.task_format_value_label.text+'[/b] in order to save.'

        elif self.ids.task_type_spinner.text == 'PowerShell' and self.ids.task_format_spinner.text == 'Script File' and Path(self.ids.task_format_value.text).suffix not in self.powershell_script_extensions:
            error_msg = '[b]PowerShell scripts must end in one of the following file extensions:\n'+self.powershell_script_extensions_as_string+'[/b]'

        elif self.ids.task_type_spinner.text == 'Remote' and len(self.ids.ip_address_value.text) > 0:

            validip4 = iptools.ipv4.validate_ip(self.ip_val)
            validip4cidr = iptools.ipv4.validate_cidr(self.ip_val)
            validip6 = iptools.ipv6.validate_ip(self.ip_val)
            validip6cidr = iptools.ipv6.validate_cidr(self.ip_val)

            if validip4 == False and validip6 == False and validip4cidr == False and validip6cidr and False:
                error_msg = "IP Address is not in a [b]valid format[/b] (ipv4 or ipv6, dotted quad or hex).\nNumbers, Periods, Slashes and Colons are allowed. '[b]localhost[/b]' is also allowed."

        if len(error_msg) < 1:
        
            if self.ids.task_type_spinner.text == 'Remote' and len(self.ids.ip_address_value.text) < 1:
                error_msg = "Remote Tasks must have an [b]IP Address[/b].\nPlease enter an [b]IP Address or Address Range[/b]."

            elif self.ids.task_type_spinner.text == 'Remote' and len(self.ids.username_value.text) < 1:
                error_msg = "Remote Tasks must have a [b]Username[/b] for logging in to the target system.\nPlease enter a [b]Username[/b]."

            elif self.ids.task_type_spinner.text == 'Remote' and len(self.ids.password_value.text) < 1:
                error_msg = "Remote Tasks must have a [b]Password[/b] for logging in to the target system.\nPlease enter a [b]Password[/b]."

        if len(error_msg) < 1:
            if self.original_task_list_file_name != self.ids.task_list_name.text:
                error_msg = "Task List Rename is Incomplete"
                self.ntc_ok_cancel_box('ok_cancel','', 0, 'rename_task_list_incomplete', self.ids.task_list_name.text)

        if len(error_msg) > 0:
            self.set_feedback_message('error', error_msg, '', 5)
        else:
            if self.edit_existing_task_flag == False:
                self.save_task('new_task')
            else:
                self.save_task('edit_task')

    def save_task(self, command):

        if len(self.dupe_task_sequence) < 1:
            saving_task_msg = 'Saving Task...Please Wait'
            self.set_feedback_message('feedback', saving_task_msg , 'large', 0)

        target_file = self.ids.task_list_file_chooser.path + '/' + self.ids.task_list_file_spinner.text

        if command == 'new_task' or command == 'edit_task':
            task = {
                "task_status":self.ids.task_status_spinner.text,
                "task_name":self.ids.task_name.text,
                "task_type":self.ids.task_type_spinner.text,
                "task_format":self.ids.task_format_spinner.text,
                "task_format_value":self.ids.task_format_value.text,
                "ip_address":self.ids.ip_address_value.text,
                "username":self.ids.username_value.text,
                "password":self.ids.password_value.text
            }
        elif command == 'duplicate_task':
            task = {
                "task_status":self.task_to_dupe["task_status"],
                "task_name":self.task_to_dupe["task_name"],
                "task_type":self.task_to_dupe["task_type"],
                "task_format":self.task_to_dupe["task_format"],
                "task_format_value":self.task_to_dupe["task_format_value"],
                "ip_address":self.task_to_dupe["ip_address"],
                "username":self.task_to_dupe["username"],
                "password":self.task_to_dupe["password"]
            }
        elif command == 'change_task_status':
            task_to_change =  json.loads(self.current_task_list[self.current_task_index])
            current_task_status = task_to_change['task_status']

            if current_task_status == 'Active':
                new_task_status = 'Inactive'
            else:
                new_task_status = 'Active'

            task = {
                "task_status":new_task_status,
                "task_name":task_to_change["task_name"],
                "task_type":task_to_change["task_type"],
                "task_format":task_to_change["task_format"],
                "task_format_value":task_to_change["task_format_value"],
                "ip_address":task_to_change["ip_address"],
                "username":task_to_change["username"],
                "password":task_to_change["password"]
            }

        if command == 'edit_task' or command == 'change_task_status':

            with open(target_file, 'w') as the_file:    # empty the file before writing new list
                pass

            for index, task_from_list in enumerate(self.current_task_list):
                if index == self.current_task_index:
                    self.current_task_list.pop(index)
                    self.current_task_list.insert(index, json.dumps(task))

            for index, task_from_list in enumerate(self.current_task_list):
                with open(target_file, 'a') as the_file:    # write new list to file
                    the_file.write(task_from_list.strip('\n'))
                    the_file.write('\n')

        elif command == 'duplicate_task' and len(self.dupe_task_sequence) > 0:

            if int(self.dupe_task_sequence) > len(self.current_task_list):
                task_list_index = len(self.current_task_list)
            elif int(self.dupe_task_sequence) <= 0:     # just in case we have a wise-guy user out there...
                task_list_index = 0
            else:
                task_list_index = int(self.dupe_task_sequence) - 1

            self.current_task_list.insert(task_list_index, json.dumps(task)+'\n')


            with open(target_file, 'w') as the_file:    # empty the file before writing new list
                pass

            for index, task_from_list in enumerate(self.current_task_list):
                with open(target_file, 'a') as the_file:    # write new list to file
                    the_file.write(task_from_list.strip('\n'))
                    the_file.write('\n')


        else:
            with open(target_file, 'a') as the_file:
                json.dump(task,the_file)
                the_file.write('\n')

        self.clear_tasks_from_grid()
        self.clear_task_fields()
        if command == 'edit_task':
            self.edit_tasks_callback()
        self.get_tasks()
        Clock.schedule_once(lambda a:self.clear_feedback_message(), 1.25)


    def change_task_status(self, task_index):
        self.current_task_index = task_index
        self.save_task('change_task_status')


    def toggle_all_task_statuses(self,command):
        if command == 'activate_all':
            new_status = 'Active'
        else:
            new_status = 'Inactive'

        target_file = self.ids.task_list_file_chooser.path + '/' + self.ids.task_list_file_spinner.text

        with open(target_file, 'w') as the_file:    # empty the file before writing new list
            pass

        for index, task_from_list in enumerate(self.current_task_list):
            original_data = json.loads(task_from_list.strip('\n'))
            original_data['task_status'] = new_status

            altered_data = json.dumps(original_data)

            with open(target_file, 'a') as the_file:    # write new list to file
                the_file.write(altered_data)
                the_file.write('\n')

        self.edit_tasks_callback()
        self.clear_tasks_from_grid()
        self.get_tasks()

    def clear_task_fields(self):
        self.update_task_format_value_controls('-1')
        self.ids.task_status_spinner.text='Active'
        self.ids.task_name.text=''
        self.ids.task_type_spinner.text='[i](disabled)[/i]'
        self.ids.task_format_spinner.text='[i](disabled)[/i]'
        self.ids.task_format_value.text=''
        self.ids.ip_address_value.text=''
        self.ids.ip_address_spinner.text=''
        self.ids.username_value.text=''
        self.ids.password_value.text=''
        self.toggle_edit_existing_task_flag(False)


    def delete_task(self):
        target_file = self.ids.task_list_file_chooser.path + '/' + self.ids.task_list_file_spinner.text

        self.current_task_list.pop(self.current_task_index)
        
        with open(target_file, 'w') as the_file:    # empty the file before writing new list
            pass

        with open(target_file, 'a') as the_file:    # write new list to file
            for task_index, data in enumerate(self.current_task_list):
                the_file.write(data)

        self.clear_tasks_from_grid()
        self.get_tasks()
        Clock.schedule_once(lambda a:self.clear_feedback_message(), 1.25)

    #-------- END Task functions --------#


    #-------- BEGIN Task Grid functions --------#
    def set_task_grid_feedback(self,command):

        if command == 'empty_list':
            self.set_feedback_message('error','Task List is empty\nClick "Add Task" to add items to the list.', 'large', 5)
        elif command == 'loading':

            self.ntc_ok_cancel_box('ok_only', 'large', 0, 'loading_task_list', '')

    def toggle_edit_existing_task_flag(self,status):
        self.edit_existing_task_flag = status

    def load_task_fields(self,task_index):
        self.toggle_edit_existing_task_flag(True)
        self.current_task_index = task_index
        task_as_json = json.loads(self.current_task_list[task_index])

        self.ids.task_status_spinner.text=task_as_json["task_status"]
        self.ids.task_name.text=task_as_json["task_name"]
        self.ids.task_type_spinner.text=task_as_json["task_type"]
        self.ids.task_format_spinner.text=task_as_json["task_format"]

        self.ids.task_format_value.text=task_as_json["task_format_value"]
        # setting task_format_value twice due to results of update_task_format_value_controls() method
        # TODO:  untangle that mess!

        if str(task_as_json["task_format"]) == 'Script File':
            self.script_path_list_spinner.text=str(Path(self.ids.task_format_value.text).parent)

        self.ids.task_format_value.text=task_as_json["task_format_value"]
        # setting task_format_value twice due to results of update_task_format_value_controls() method
        # TODO:  untangle that mess!

        if task_as_json["ip_address"] is not None:
            self.ids.ip_address_value.text=task_as_json["ip_address"]
            self.ids.ip_address_spinner.text=task_as_json["ip_address"]
        else:
            self.ids.ip_address_value.text=''

        if task_as_json["username"] is not None:
            self.ids.username_value.text=task_as_json["username"]
        else:
            self.ids.username_value.text=''

        if task_as_json["password"] is not None:
            self.ids.password_value.text=task_as_json["password"]
        else:
            self.ids.password_value.text=''


    def get_tasks(self):

        task_list_file_info = self.get_task_list_file()
        all_tasks = TaskListInfo.get_task_list_contents(self, task_list_file_info[2])

        if os.path.getsize(task_list_file_info[2]) == 0:
            self.set_task_grid_feedback('empty_list')
        else:
            self.set_task_grid_feedback('loading')
            self.current_task_list = all_tasks
            Clock.schedule_once(lambda a:self.display_tasks(), 1)


    def display_tasks(self):
        
        for task_index, data in enumerate(self.current_task_list):
            task = json.loads(data)
            seq_num = task_index + 1
            task_seq_label = NtcTaskLabel(text=str(seq_num),width=self.column_widths[0])
            self.ids.task_grid.add_widget(task_seq_label)

            column_width_index = 0
            for item_index, item in enumerate(task):

                column_width_index+=1
                label_width=self.column_widths[column_width_index]

                if len(task[item]) > 0:
                    label_text = task[item]
                else:
                    label_text = '-'

                if item_index == 0:
                    if label_text == 'Inactive':
                        status_button_color = delete_button_color
                    else:
                        status_button_color = green_button_color
                    status_button = NtcTaskStatusButton(text=label_text,background_color=status_button_color)
                    status_button.bind(on_press=lambda a,b=task_index:[self.change_task_status(b)])
                    self.ids.task_grid.add_widget(status_button)
                else:
                    item_label = NtcTaskLabel(text=label_text,width=label_width)
                    self.ids.task_grid.add_widget(item_label)

            edit_button = NtcTaskEditButton()
            edit_button.bind(on_release=lambda a,b=task_index:[self.load_task_fields(b)])
            edit_button.bind(on_press=lambda a:[self.toggle_edit_task_controls('enable')])
            
            duplicate_button = NtcTaskDuplicateButton()
            duplicate_button.bind(on_press=lambda a,b=task_index:[self.ntc_ok_cancel_box('ok_cancel', '', 0, 'dupe_prompt', b)])

            delete_button = NtcTaskDeleteButton()
            delete_button.bind(on_press=lambda a,b=task_index:[self.ntc_ok_cancel_box('ok_cancel', 'large', 0, 'delete_task_prompt', b)])

            self.ids.task_grid.add_widget(edit_button)
            self.ids.task_grid.add_widget(duplicate_button)
            self.ids.task_grid.add_widget(delete_button)

            if task_index == len(self.current_task_list) - 1:
                self.toggle_bulk_task_list_controls('enable_controls')
                self.dismiss_popup()

    def clear_tasks_from_grid(self):
        self.ids.task_grid.clear_widgets()
        self.toggle_bulk_task_list_controls('disable_controls')

    def toggle_bulk_task_list_controls(self,command):
        if command == 'disable_controls':
            self.ids.reorder_tasks_button.disabled=True
            self.ids.activate_all_tasks_button.disabled=True
            self.ids.deactivate_all_tasks_button.disabled=True

            self.ids.reorder_tasks_button.background_color=disabled_controls_color
            self.ids.activate_all_tasks_button.background_color=disabled_controls_color
            self.ids.deactivate_all_tasks_button.background_color=disabled_controls_color
            
        elif command == 'enable_controls':
            self.ids.reorder_tasks_button.disabled=False
            self.ids.activate_all_tasks_button.disabled=False
            self.ids.deactivate_all_tasks_button.disabled=False

            self.ids.reorder_tasks_button.background_color=buttons_and_controls_color
            self.ids.activate_all_tasks_button.background_color=buttons_and_controls_color
            self.ids.deactivate_all_tasks_button.background_color=buttons_and_controls_color

    #-------- END Task Grid functions --------#

#--------- END NtcMain Class ---------#


#--------- BEGIN Other Classes ---------#
class NtcSaveTaskListButton(Button):
    pass

class NtcDeleteTaskListButton(Button):
    pass

class NtcCancelTaskListEditsButton(Button):
    pass

class NtcOkCancel(Popup):
    pass

class NtcDuplicateTaskBox(FloatLayout):
    pass

class NtcScriptPathSpinner(Spinner):
    pass

class NtcIpAddressSpinnerOption(SpinnerOption):
    pass

class NtcIpAddressSpinnerDropdown(DropDown):
    pass

class NtcScriptPathInfoLabel(Label):
    pass

class NtcTaskFormatValueTextInput(TextInput):
    pass

class FileChooserMain(FileChooserListView):
    pass
#--------- END Other Classes --------#


class NtcMainApp(App):
    def build(self):
        return NtcMain()
    
if __name__ == '__main__':
    NtcMainApp().run()