/* DELETE records from tool_request TABLE
	
	Sam Mikitarian 04 Jan 2022
*/

DO $$
DECLARE

BEGIN

	DELETE FROM tool_request;
	
END;
$$