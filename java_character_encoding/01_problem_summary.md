**PROBLEM SUMMARY**  

PROBLEM 1: 	Special Characters Omitted during SPL import 
(ex: trade mark, degree, copyright)

PROBLEM 2:	Characters are in a CORRUPTED/TRANSFORMED state PRIOR to import

PROBLEM 3: 	Paragraph Formatting Jumbled when comparing documents in the web browser via the CDP application

PROBLEM 4: 	Page Load Failure - in some cases when comparing documents in the web browser via the CDP application

PROBLEM 5:	Proprietary Name Does Not Display - when comparing documents in the web browser via the CDP application
<br/>
<br/>
**SOLUTION SUMMARY**  

SOLUTION 1:	For omitted characters:  update parsing code to retain special characters

SOLUTION 2:	For already corrupted characters:  perform character or string replacement

SOLUTION 3:	Paragraph Formatting Jumbled
Transfer to Requirement 1175 “DPL Screen Rendering Issues (Comparison Tool)”

SOLUTION 4:	Page Load Failure
Transfer to Requirement 1175 “DPL Screen Rendering Issues (Comparison Tool)”

SOLUTION 5:	Proprietary Name Does Not Display
Transfer to Requirement 608 “Proprietary Name field is blank in SPL Search”
