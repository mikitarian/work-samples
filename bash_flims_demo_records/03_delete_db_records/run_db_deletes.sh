#!/bin/bash
# NOTE : Quote it else use array to avoid problems #
FILES="./*.sql"
for f in $FILES
do
  echo "Running $f"
  psql postgres -d flims -f ./$f 1> /dev/null
  sleep 1.25;
  # take action on each file. $f store current file name
  # cat "$f"
done