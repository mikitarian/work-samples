### AppDynamics Enterprise Console Server Preparation

##### Access to Software:

Create an account at appdynamics.com

All software is available from [https://accounts.appdynamics.com/](https://accounts.appdynamics.com/)

##### AWS Server Selection:

We need 1 AWS EC2 instance (t3.2xlarge) running RHEL 8 patched to version 8.9 (or greater)

AppDynamics guidelines for server sizing are taken from these sites:

* https://docs.appdynamics.com/appd/onprem/latest/en/enterprise-console/enterprise-console-requirements
* https://docs.appdynamics.com/appd/onprem/23.9/en/events-service-deployment/events-service-requirements
* https://docs.appdynamics.com/appd/onprem/23.1/en/eum-server-deployment/eum-server-requirements

Recommendations for...  
Small profile Controller installation:  

* CPU: 4 Cores, 1.5GHz minimum  
* RAM: 16GB  
* Disk Storage: 400GB  
  
End User Management Server:  
* 50 GB extra disk space  
* Processing: 4 cores  
* Minimum 8 GB memory total (4 GB is defined as max heap in JVM)

**Specs for our server in AWS are as follows:***

* CPUs: 8
* RAM: 32GB  (because "The Enterprise Console requires an additional memory of one GB of free RAM for Java and MySQL processes.")
* Disk Space: 800GB
* Ports: (see AWS Security Group below)

Once the server has been created in AWS EC2, proceed with preparations listed below.

##### RHEL 8 Updates for server:

Run `dnf update -y` to update the OS to RHEL 8.9

cat the /etc/redhat-release file to confirm patch level:

```
cat /etc/redhat-release
Red Hat Enterprise Linux release 8.9 (Ootpa)
```

##### Add the following ports to the server's firewall

Log in to the server via terminal window and switch to an account with root privileges

Enable and start firewalld:

```
systemctl enable firewalld
systemctl start firewalld
```

Optional - check firewalld status:
`systemctl status firewalld`

Add the following ports and reload the firewall daemon:

```
firewall-cmd --zone=public --add-port=3388/tcp --permanent
firewall-cmd --zone=public --add-port=3700/tcp --permanent
firewall-cmd --zone=public --add-port=4848/tcp --permanent
firewall-cmd --zone=public --add-port=8020/tcp --permanent
firewall-cmd --zone=public --add-port=8021/tcp --permanent
firewall-cmd --zone=public --add-port=8090/tcp --permanent
firewall-cmd --zone=public --add-port=8181/tcp --permanent
firewall-cmd --zone=public --add-port=9080/tcp --permanent
firewall-cmd --zone=public --add-port=9081/tcp --permanent
firewall-cmd --zone=public --add-port=9191/tcp --permanent
firewall-cmd --zone=public --add-port=9200/tcp --permanent
firewall-cmd --zone=public --add-port=9300/tcp --permanent
firewall-cmd --reload
```

Confirm ports are open:

```
firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: eth0
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 9081/tcp 9080/tcp 9191/tcp 8090/tcp 8080/tcp 443/tcp 3388/tcp 4848/tcp 8020/tcp 8030/tcp 8181/tcp 8021/tcp 3700/tcp 9200/tcp 9300/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

If you make a mistake, just remove the port in question:

```
firewall-cmd --zone=public --permanent --remove-port 9191/tcp
firewall-cmd --reload
```

##### AWS Security Group:

The AppDynamics Server needs to be in the following AWS Security Groups:

* **rhel-internal** Security Group for basic accessibility in our network
* **AppDynamicsAccess-Temp** Security Group so AppDynamics can communicate with all hosts

Inbound Rules for AppDynamicsAccess-Temp Security Group are:


| Type       | Protocol | Port | IP Range   | Description                                       |
| ------------ | ---------- | ------ | ------------ | --------------------------------------------------- |
| Custom TCP | TCP      | 8080 | 10.0.0.0/8 | Java Agent                                        |
| Custom TCP | TCP      | 9080 | 10.0.0.0/8 | Events Service API Store Port                     |
| HTTPS      | TCP      | 443  | 10.0.0.0/8 | SSL                                               |
| Custom TCP | TCP      | 9191 | 10.0.0.0/8 | Enterprise Console Port                           |
| Custom TCP | TCP      | 9081 | 10.0.0.0/8 | Events Service API Store Admin PortController Por |
| Custom TCP | TCP      | 8090 | 10.0.0.0/8 | Controller Port                                   |

##### Server Software Prerequisites:

Check for and install prerequisite packages
from: [https://docs.appdynamics.com/appd/onprem/latest/en/enterprise-console/enterprise-console-requirements](https://docs.appdynamics.com/appd/onprem/latest/en/enterprise-console/enterprise-console-requirementshttps:/)

Check for Required Libraries:


| Library                 | Command                     | Result if Installed*                           |
| ------------------------- | ----------------------------- | ------------------------------------------------ |
| libaio                  | rpm -qa\| grep libaio       | libaio-0.3.112-1.el8.x86_64                    |
| glibc2.12               | rpm -qa\| grep glib         | glibc-2.28-236.el8.7.x86_64                    |
|                         |                             | glibc-devel-2.28-236.el8.7.x86_64              |
|                         |                             | glibc-langpack-en-2.28-236.el8.7.x86_64        |
|                         |                             | glibc-headers-2.28-236.el8.7.x86_64            |
|                         |                             | glibc-gconv-extra-2.28-236.el8.7.x86_64        |
|                         |                             | glibc-common-2.28-236.el8.7.x86_64             |
| tzdata                  | rpm -qa\| grep tzdata       | tzdata-2023c-1.el8.noarch                      |
|                         |                             | tzdata-java-2023c-1.el8.noarch                 |
| ncurses-libs-5.x        | rpm -qa\| grep ncurses-libs | ncurses-libs-6.1-10.20180224.el8.x86_64        |
| ncurses-compat-libs-6.x | rpm -qa\| grep compat-lib   | ncurses-compat-libs-6.1-10.20180224.el8.x86_64 |
| nss                     | rpm -qa\| grep nss-         | nss-util-3.90.0-4.el8_9.x86_64                 |
|                         |                             | nss-sysinit-3.90.0-4.el8_9.x86_64              |
|                         |                             | nss-softokn-freebl-3.90.0-4.el8_9.x86_64       |
|                         |                             | nss-3.90.0-4.el8_9.x86_64                      |
|                         |                             | nss-softokn-3.90.0-4.el8_9.x86_64              |

*if a package is **NOT** installed, there will be no return at all from the `rpm -qa` command

To install a missing package, run `sudo dnf install -y <package_name>`


##### Create an appdynamics user to perform the installation and run the Enterprise Console

**NOTE:**  The Elastic Search Service that is part of the AppDynamics Events Service **CANNOT run as root**.  It must run under another account, which is why we're creating the `appdynamics` user now.

`useradd appdynamics`

##### Add appdynamics user to sudoers

`vi /etc/sudoers`

example of edited sudoers file:

```
## Allow root to run any commands anywhere

root ALL=(ALL) ALL
appdynamics ALL=(ALL) NOPASSWD: ALL
```

##### Create an installation directory and give the appdynamics user full permissions on it

```
mkdir /opt/appdynamics
chown -R appdynamics: /opt/appdynamics
chmod 700 /opt/appdynamics
```

##### Configure user limits for OS connections and file access

Run the following commands and make note of the outputs:

```
su – appdynamics
ulimit -S -n
```

The recommended value is 65535.

`ulimit -S -u`

The recommended value is 8192.

If those values do not exist for the appdynamics user, become root and edit the /etc/security/limits.d file:

`vi /etc/security/limits.conf`

Add the following lines to the file:

```
appdynamics hard nofile 65535
appdynamics soft nofile 65535
appdynamics hard nproc 8192
appdynamics soft nproc 8192
```

Enable the new file descriptors by editing the /etc/pam.d/common-session file:

`vi /etc/pam.d/common-session`

Add the following to the file

```
#appdynamics
session required pam_limits.so
```

Validate the changes.

Switch over to the appdynamics user.
`sudo su - appdynamics`

Check the user limits:
`ulimit -S -n`
The recommended value is 65535.
`ulimit -S -u`
The recommended value is 8192.

Update server swapping for performance.

Check current setting (as sudo user):

`sudo /sbin/sysctl -a | grep swappiness`

If the value is different than 10, adjust it:

`sudo bash -c "echo 10 >> /proc/sys/vm/swappiness"`

Also update the sysctl.conf file:

`sudo vi /etc/sysctl.conf`

Add the following line:

`vm.swappiness = 10`

Check the setting to validate the changes:

`sudo /sbin/sysctl -a | grep swappiness`

Output should be like this:

```
vm.force_cgroup_v2_swappiness = 0
vm.swappiness = 10
```
