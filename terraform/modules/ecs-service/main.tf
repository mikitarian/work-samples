resource "aws_ecs_task_definition" "task" {
  family                   = var.task_name
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.container_cpu
  memory                   = var.container_memory
  execution_role_arn       = var.execution_role_arn
  task_role_arn            = var.task_role_arn
  container_definitions = jsonencode([
    {
      "name" : var.service_name,
      "image" : "${var.repository_uri}:${var.image_tag}",
      "cpu" : var.container_cpu,
      "memory" : var.container_memory,
      "essential" : var.container_is_essential,
      "HealthCheck" : {
        "Command" : var.health_check_command,
        "Interval" : 30,
        "Retries" : 3,
        "StartPeriod" : 60,
        "Timeout" : 5
      },
      "portMappings" : [
        for p in var.container_ports : { "containerPort" = p }
      ]
      "environment" : var.environment_variables,
      "secrets" : var.secrets,
      "LogConfiguration" : {
        "LogDriver" : "awslogs",
        "Options" : {
          "awslogs-region" : var.region,
          "awslogs-group" : var.log_group,
          "awslogs-stream-prefix" : "/ecs"
        }
      }
    }
  ])
}

resource "aws_ecs_service" "this" {
  name            = var.service_name
  cluster         = var.ecs_cluster
  task_definition = aws_ecs_task_definition.task.arn
  launch_type     = "FARGATE"
  desired_count   = var.desired_count

  network_configuration {
    subnets         = var.subnets
    security_groups = var.security_groups
  }

  dynamic "load_balancer" {
    for_each = var.load_balancer_blocks

    content {
      target_group_arn = load_balancer.value.target_group_arn
      container_name   = load_balancer.value.container_name
      container_port   = load_balancer.value.container_port
    }
  }

  service_registries {
    registry_arn = aws_service_discovery_service.this.arn
  }
}

resource "aws_service_discovery_service" "this" {
  name = var.service_name

  dns_config {
    namespace_id = var.service_discovery_namespace_id

    dns_records {
      ttl  = 10
      type = "A"
    }
  }

  health_check_custom_config {
    failure_threshold = 1
  }
}
