/* DELETE records from task TABLE
	
	Sam Mikitarian 29 Dec 2021
*/

DO $$
DECLARE

BEGIN

	DELETE FROM task;
	
END;
$$