output "vpc_id" {
  description = "The ID of the VPC"
  value       = module.vpc.vpc_id
}

output "vpc_cidr_block" {
  description = "The CIDR of the VPC"
  value       = module.vpc.vpc_cidr_block
}

output "private_subnets" {
  description = "The list of private subnets within the VPC"
  value       = module.vpc.private_subnets
}

output "public_subnets" {
  description = "The list of public subnets within the VPC"
  value       = module.vpc.public_subnets
}

output "iam_role_ecs_execution_arn" {
  description = "The ARN of the ECS execution role"
  value       = module.ecs_execution_role.iam_role_arn
}