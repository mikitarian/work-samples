/* DELETE records from artifact TABLE
	
	Sam Mikitarian 04 Jan 2022
*/

DO $$
DECLARE

BEGIN

	DELETE FROM artifact;
	
END;
$$