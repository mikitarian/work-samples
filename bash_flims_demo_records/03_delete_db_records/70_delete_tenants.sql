/*	DELETE records from tenant TABLE

	Sam Mikitarian 04 Jan 2022
*/

DO $$
DECLARE

	flic_tenant_id INTEGER := (SELECT "id" FROM tenant WHERE LOWER("name")='flic core team');
	
	sql_stmt VARCHAR;
	
BEGIN

		sql_stmt := concat('DELETE FROM tenant WHERE "id" NOT IN (', flic_tenant_id , ');');
		EXECUTE sql_stmt;
	
END;
$$