PROBLEM 1:  	Special Characters Omitted:

Analysis –Existing SPL parsing code in SplDocumentService.java will accept the set of Printable ASCII Characters.
    - SPL documents are received in XML format and contain Unicode Transformation Format – 8 bit (UTF-8) Characters.
    - The mismatch of ASCII Character filtering with UTF-8 data is causing special characters to be dropped during the import process.

Solution Approach:
    - Refine the existing SPL parsing code to 
        - accept the full range of Printable UTF-8 Characters
        - remove ASCII and UTF-8 Control Characters as a defense against Cross Site Scripting (XSS)
    - Coding Details
        - Remove the following code from the “processSPL()” method in SplDocumentService.java:
            - // only accepts ASCII printable characters
            - xmlContent = xmlContent.replaceAll("[^\\x20-\\x7e]", "");

        - Replace it with:
            - // erases all the ASCII control characters
            - xmlContent = xmlContent.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", "");

            - // erases all Unicode control characters
            - xmlContent = xmlContent.replaceAll("\\p{C}", "");


PROBLEM 2:	Characters Corrupted/Transformed Prior to Import:

Analysis –Certain characters are already corrupted when they are received from their source
    - Examples:
        - Trademark is received as:	â„¢	instead of:	™
        - Micro symbol is received as:	Î¼	instead of:	μ
        - etc, etc, etc
    - Here are good explanations of the problem:
        - https://www.justinweiss.com/articles/how-to-get-from-theyre-to-theyre/
        - https://www.thesitewizard.com/html-tutorial/pound-sign-not-showing-up-correctly.shtml

Corruption is most probably caused by different default character encoding standards in place on different computing systems when the file is being processed
    - Example Scenario:
        - File originates on Windows system with CP1252 encoding
            - no corruption yet
        - File is passed to Unix system with UTF encoding
            - 1st potential for corruption/character set transformation
        - File is zipped from Unix system and downloaded to Windows system with CP1252
            - 2nd potential for corruption/character set transformation
        - File is unzipped
            - Corrupted/transformed characters are present in file AT THIS TIME
        - File is then read by the CDP DPL application and stored in an Oracle NCLOB column and corrupted/transformed characters remain
