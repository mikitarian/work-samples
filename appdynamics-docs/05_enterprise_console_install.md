### AppDynamics Enterprise Console Installation

The AppDynamics Enterprise Console is a Web Application with robust command-line tools that can manage the installation, configuration, and administration of many AppDynamics components.

**NOTE:**  Procedures on this page assume all steps in Server Preparation are complete

Download the installation package

* Log in to accounts.appdynamics.com.
* Click Downloads from the left-hand menu
* Click the Platforms tab
* Click the Type dropdown and select Enterprise Console
* Then select Latest Version, and Linux Operating System
* Download the platform setup script
  `ex: platform-setup-x64-linux-23.11.1.10113.sh`

**Log in to the target server with an account that has root privileges**

You'll `su` to the appdynamics user when you actually run the install script.  Until then it's easier to use root privileges to set things up.

**Create an installation directory and give the appdynamics user full permissions on it**

```
mkdir /opt/appdynamics
chown -R appdynamics: /opt/appdynamics
```

**Copy AppDynamics installation script to the target machine.**
Use either WinSCP or the linux command line.
linux command line example:

scp <filename> <user_name>@<server_name>:<destination_directory>
scp platform-setup-x64-linux-23.11.1.10113.sh root@<server_ip_address>:/home/appdynamics/appd-install-files

**Set permissions on the installation script file:**

`chmod 775 /home/appdynamics/appd-install-files/platform-setup-x64-linux-23.11.1.10113.sh`

**Create an installation variables file to automate the install process.**

**NOTE 1:**  When installing the Enterprise Console and the Controller on the same machine, they must be in different directories in order to keep their data separate. For example, if the Enterprise Console is in /opt/appdynamics, the Controller could be installed in /opt/appd-controller.

**NOTE 2:**  The `serverHostName` value must match the machine's IP Address ***OR*** the hostname added during server prep.  Use the `hostnamectl` command to view the machine's hostname and update your response.varfile accordingly.

**NOTE 3:**  A copy of the response.varfile used in our intstallation is in AWS Systems Manager -> Parameter Store.  Just filter for "appdynamics".

`touch /home/appdynamics/appd-install-files/response.varfile`

**Add the following to your response.varfile:**

```
serverHostName=<ip address of host machine>
sys.languageId=en
disableEULA=true

platformAdmin.port=9191
platformAdmin.databasePort=3377
platformAdmin.dataDir=/opt/appdynamics/enterpriseconsole/mysql/data
platformAdmin.databasePassword=<your db password>
platformAdmin.databaseRootPassword=<your db root password>
platformAdmin.adminPassword=<your admin password>
platformAdmin.useHttps$Boolean=true
sys.installationDir=/opt/appdynamics/enterpriseconsole
```

**Switch User to the account that will run the installation**
`su appdynamics`

**cd to the directory containing the install script and run it.**
example:  `cd /home/appdynamics/appd-install-files`
install command syntax is:  `./<install_script_name>.sh -q -varfile ~/response.varfile`
example:  `./platform-setup-x64-linux-23.11.1.10113.sh -q -varfile ./response.varfile`

Output is like this:

```
Unpacking JRE ...
Preparing JRE ...
Starting Installer ...
The installation directory has been set to /opt/appdynamics/enterpriseconsole.
Verifying if the package manager is installed. /opt/appdynamics/enterpriseconsole/installer/checkPkgMgr.sh
Verifying if the libaio package is installed. /opt/appdynamics/enterpriseconsole/installer/checkLibaio.sh
Verifying if the libnuma package is installed. /opt/appdynamics/enterpriseconsole/installer/checkLibnuma.sh
Verifying the libc version. /opt/appdynamics/enterpriseconsole/installer/checkLibc.sh
Verifying if the zoneinfo directory exists. if not, check tzdata package
Verifying if the libncurses5 package is installed. /opt/appdynamics/enterpriseconsole/installer/checkNcurses.sh
Verifying if curl is installed. /opt/appdynamics/enterpriseconsole/installer/detect_os_packages.sh
Verifying if the port 9191 is in use.
Verifying if the port 3377 is in use.
Extracting files ...
Installing Enterprise Console Database...
Installing Enterprise Console Application...
Finishing installation ...
```

**Switch back to root for the remaining steps**
<br/>
<br/>
<br/>
**Add $APPD_HOME (AppDynamics CLI directory) as environment variable and append to system path:**

```
cd /etc/profile.d
vi appd_home.sh
```

**Add the following lines to appd_home.sh:**

```
# sets $APPD_HOME as system variable and adds it to $PATH

export APPD_HOME=/opt/appdynamics/enterpriseconsole/platform-admin/bin

PATH=$PATH:$APPD_HOME
```

**Enter the following command to load the updated path into your session:**
`source /etc/profile.d/appd_home.sh`

**To see the change enter:**
`echo $PATH`

Output is like this:
`/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin:/opt/appdynamics/enterpriseconsole/platform-admin/bin`

**Create a service to start AppDynamics automatically on system reboot:**

```
cd /etc/systemd/system
vi appdconsole.service

```

**Add the following info to the file and save it:**

```
[Unit]
Description=AppDynamics Enterprise Console start on system boot.

[Service]
Type=oneshot
RemainAfterExit=yes
User=appdynamics
WorkingDirectory=/opt/appdynamics/enterpriseconsole/platform-admin/bin/
ExecStart=/bin/bash /opt/appdynamics/enterpriseconsole/platform-admin/bin/platform-admin.sh start-platform-admin
TimeoutStartSec=0

[Install]
WantedBy=multi-user.target
```

**Set permissions on the file:**
`chmod 644 appdconsole.service`

**Reload the systemctl daemon:**
`systemctl daemon-reload`

**Tell systemd to enable the service:**
`systemctl enable appdconsole.service`

**Confirm service was created successfully:**
`systemctl --all | grep appdconsole.service`

**Result should be something like this:**
`appdconsole.service		loaded		inactive	dead	AppDynamics Enterprise Console start on system boot.`

**Start the service:**
`systemctl start appdconsole.service`

**Test with a reboot.**
After reboot you should be able to open the login page at https://\<server ip address or DNS name\>:9191 with your browser

Enterprise Console Default username:  admin  
Enterprise Console Default password:  \<whatever we set in the response.varfile\>  

---

### Useful CLI Commands:

**Log in at Command Line (Session expired error):**

When working with the CLI, you may see this error:  
```
Error code 401
Message: Session expired. Please login and run the command again.
```
To log in at the command line:  

```
platform-admin.sh login --user-name admin
Enter password: (same password used to log into GUI as admin)
```

**Manually start and stop the Enterprise Console:**

```
platform-admin.sh start-platform-admin
platform-admin.sh stop-platform-admin
```

**Uninstall Enterprise Console**

```
cd /opt/appdynamics/enterpriseconsole/installer/
./uninstallPlatform -q
```
