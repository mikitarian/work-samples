~~## Setting up the environment

The following files will need to be modified to run and 
migrate the data from 1x to develop
```sh
db_credentials.py
variables.py
```

## DB_CREDENTIALS

The DB Credentials will be updated accordingly to allow the system to
both query the old system and insert data into the new system

#### datawarehouse_db_config
This is where the data will be going (destination)

| Attribute   | Purpose                                |
|-------------|----------------------------------------|
| driver      | The DB Driver                          |
| server      | The SQL Server                         |
| database    | The DB Name                            |
| user        | The username for the DB                |
| password    | The password for the DB                |
| autocommit  | Are we committing without transaction  |

Driver if run on Windows is '{SQL Server}', if run on Linux '{ODBC Driver 17 for SQL Server}'.

#### sqlserver_db_config

This is the same as the previous table; however, this is where the 
data is initially coming from (The Soruce)

## Running the script

First make sure the requirements are all installed
```sh
pip install -r requirements.txt
```

Then run the thing
```commandline
python main.py
```
