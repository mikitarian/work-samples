This script allows the user to add or remove Demo Cases/Tasks/Persons etc
in the FLIMS Postgresql Database

PREREQUISITES TO RUNNING THIS SCRIPT:
		Build, Seed and Start the FLIMS postgresql database
		Build and Start the FLIMS Keycloak server
		Build, Start and enter the Setup Parameters for the FLIMS Web server

TO USE: 
	1)  Open a GitBash Terminal Window (or some other terminal that can run a .sh script):

	2)	Change directory to the cssp-nx/scripts directory
			
			cd /your/path/to/cssp-nx/scripts


	3)	Run the manage_demo_db_records.sh script:

			./manage_demo_db_records.sh

	4)	NOTE: SEQUENCE IS IMPORTANT HERE:

		Follow the steps shown on the manage_demo_db_records.sh menu in order to ensure success.

Sam Mikitarian
27 Dec 2021