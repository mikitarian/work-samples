### Configure Applications in the Controller

This prepares the Controller to receive calls from AppDynamics Agents installed on our application servers.
Most of the settings you make here will be referenced on the target servers in the configuration files for the:
* Machine Agent  
* Java Agent  
  
**Launch the Controller and log in**

This can be done from the Enterprise Console Web Interface.
You can also launch the Controller directly via at http://\<ip address or DNS name of server\>:8090
  
**Create Applications**  
Once logged in...  
* Click Applications -> Create Application
* Choose "Create an Application Manually"  
* Enter an application name (ex: CIRRAS-Test)  
* Click "OK"
  
Here's an example of a Controller which monitors 2 application instances:  
![Controller with 2 apps](images/03_controller_two_apps.jpg)
  

**Create Application Tiers & Nodes**  
On the Application Dashboard...  
* Click Tiers & Nodes -> Actions -> Create App Server Tier  
* Select the Type of Tier to be created (in our case it's the Java Application Server)  
* Click "Next"  
* Name the Tier (ex:  test-app-server, uat-proxy-server, you get the idea...)  
* Click "Finish"

Here's an example of an application with 2 Tiers:  
![Application with 2 Tiers](images/06_controller_app_tiers_and_nodes.jpg)