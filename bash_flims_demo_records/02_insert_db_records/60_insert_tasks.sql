/* INSERT records into task table

	"id" is generated by DB Sequence,
	and is not included in the SQL below
	
	Sam Mikitarian 28 Dec 2021
*/

/* task table structure
 assignmentDate | createDate | creatorId | description | id | name | slug | status | typeId | year | yearSequence
----------------+------------+-----------+-------------+----+------+------+--------+--------+------+--------------
*/

DO $$
DECLARE

	/* tasks_array contents shown for readability:
	 assignmentDate | creatorId |                        description                                   |               name                | status | typeId (as string)
	----------------+-----------+----------------------------------------------------------------------+-----------------------------------+--------+---------------------
						1		  Ask if they have any surveillance video along HWY 1 from that night.     Contact local Sheriff''s Office     0      criminal investigation
	*/
	
	tasks_array VARCHAR[] := ARRAY[
		'"";;"1";;"Ask if they have any surveillance video along HWY 1 from that night.";;"Contact local Sheriff''s Office";;"0";;"criminal investigation"',
		'"";;"1";;"";;"Submit Incident Analysis Report to NIWC LANT";;"0";;"reporting"',
		'"";;"1";;"See disks salvaged from NAS-001 in Coast Guard Cyber Command.";;"Run heuristics analysis on infected disk drives";;"0";;"malware analysis"',
		'"";;"1";;"Current logging tools are nearing end-of-life.  We need an OS-agnostic replacement for them.";;"Identify replacement product candidates for router logging tools.";;"0";;"research"'
		];

	task VARCHAR;
	
	assignment_date TIMESTAMP WITH TIME ZONE := NULL;
	create_date TIMESTAMP WITH TIME ZONE;
	creator_id INTEGER;
	description VARCHAR := NULL;
	task_name VARCHAR;
	status task_status_enum;
	type_id_string VARCHAR;
	type_id INTEGER;
	
	last_slug_id VARCHAR;
	next_slug_id INTEGER;
	slug VARCHAR;
	the_year INTEGER;
	year_seq INTEGER;
	
BEGIN

	FOREACH task IN ARRAY tasks_array LOOP
	
		IF LENGTH(TRIM(BOTH '"' FROM split_part(task,';;',1))) > 0 THEN
		  assignment_date := TRIM(BOTH '"' FROM split_part(task,';;',1));
		END IF;
		
		create_date := CURRENT_TIMESTAMP;
		creator_id := TRIM(BOTH '"' FROM split_part(task,';;',2));
		description := TRIM(BOTH '"' FROM split_part(task,';;',3));
		task_name := TRIM(BOTH '"' FROM split_part(task,';;',4));
		status := TRIM(BOTH '"' FROM split_part(task,';;',5));
		
		type_id_string := TRIM(BOTH '"' FROM split_part(task,';;',6));
		type_id := (SELECT id FROM task_type WHERE lower(task_type."name") = lower(type_id_string));
		
		
		last_slug_id = (SELECT MAX(CAST(SUBSTRING(task."slug",8) AS INTEGER)) FROM task);
		IF last_slug_id IS NULL THEN
			next_slug_id := 1;
		ELSE
			next_slug_id := (SELECT MAX(CAST(SUBSTRING(task."slug",8) AS INTEGER))+1 FROM task);
		END IF;
		slug := CONCAT('T-', date_part('year', CURRENT_DATE), '-', next_slug_id);

		the_year := date_part('year', CURRENT_DATE);

		year_seq := (SELECT MAX("yearSequence")+1 FROM task);
		IF year_seq IS NULL THEN
			year_seq := 1;
		END IF;
		
		INSERT INTO public.task ("assignmentDate", "createDate", "creatorId", description, name, slug, status, "typeId", "year", "yearSequence") 
			VALUES (assignment_date, create_date, creator_id, description, task_name, slug, status, type_id, the_year, year_seq);
			
	END LOOP;
	
END;
$$