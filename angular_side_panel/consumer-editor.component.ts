import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ViewsQuery } from '@arsenal/features/ironsky/views/state/views.query';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Consumer } from '../../state/consumer.model';
import { ConsumersQuery } from '../../state/consumers.query';
import {
  ConsumersService,
  CONSUMERS_SERVICE_TOKEN,
} from '../../state/consumers.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'ironsky-consumer-editor',
  styleUrls: ['./consumer-editor.component.scss'],
  templateUrl: './consumer-editor.component.html',
})
export class ConsumerEditorComponent implements OnInit {
  consumer$!: Observable<Consumer | undefined>;
  readonly loading$: Observable<boolean> = combineLatest([
    this.consumersQuery.selectLoading(),
    this.viewsQuery.selectLoading(),
  ]).pipe(map((loadingArray) => loadingArray.some((l) => l)));

  constructor(
    private activatedRoute: ActivatedRoute,
    private consumersQuery: ConsumersQuery,
    @Inject(CONSUMERS_SERVICE_TOKEN) private consumersService: ConsumersService,
    private viewsQuery: ViewsQuery,
    private router: Router,
  ) {}

  ngOnInit(): void {
    const id: number = +this.activatedRoute.snapshot.params.id;
    if (id > 0) {
      this.consumer$ = this.consumersQuery.selectEntity(id);
      this.consumersService.get(id).subscribe({
        error: () => void this.router.navigateByUrl('/page-not-found'),
      });
    } else {
      this.router.navigateByUrl('/page-not-found');
    }
  }
}
