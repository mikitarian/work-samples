/*	INSERT records into inventory_location table

	"id" and "createDate" are generated by DB Sequence,
	and are not included in the SQL below
	
	Data for "contactId" and "organizationId" will be added
	after those features are implemented within the application.
	For now their values are assumed to be NULL.
	
	Sam Mikitarian 21 Dec 2021
*/

/* inventory_location table structure
 area | building | contactId | createDate | externalId | id | organizationId | name | slug | room | year | yearSequence
------+----------+-----------+------------+------------+----+----------------+------+------+------+------+--------------
*/

DO $$
	DECLARE
	
		/* inventory_locations_array contents shown for readability:
			 area        | building | externalId     | name                   | room 
			 ------------+----------+----------------+------------------------+------
		    North Side    3113       Building 3113    NIWC LANT Warehouse A    A 453
		*/
		
		inventory_locations_array VARCHAR[] := ARRAY[
			'"North Side";;"3113";;"Building 3113";;"NIWC LANT Warehouse A";;"A 453"',
			'"South Side";;"3114";;"Building 3114";;"NIWC LANT Warehouse B";;"B 127"',
			'"DHA HQ Complex";;"1023";;"DHA PCF";;"DHA Property Control Facility";;""',
			'"";;"C-271";;"Cyber Command SCIF";;"Cyber Command SCIF";;"A 250"',
			'"";;"A-820";;"FCC-IC";;"FCC Inventory Control";;"2542"',
			'"";;"Merriweather Hall";;"NSA CD-EB";;"NSA CD Evidence Branch";;"1006"'
			];
		
		location VARCHAR;
		
		area VARCHAR;
		
		building VARCHAR(64);
		contact_id INTEGER := NULL;
		external_id VARCHAR(128);
		name VARCHAR(128);
		organization_id INTEGER := NULL;
		
		last_slug_seq INTEGER;
		next_slug_seq INTEGER;
		slug VARCHAR(128);
		
		room VARCHAR(64);
		
		the_year INTEGER;
		year_seq INTEGER;
		

BEGIN
	FOREACH location IN ARRAY inventory_locations_array LOOP

		area := trim(both '"' from split_part(location,';;',1));

		building := trim(both '"' from split_part(location,';;',2));

		external_id := trim(both '"' from split_part(location,';;',3));
		
		name := trim(both '"' from split_part(location,';;',4));
		
		last_slug_seq = (SELECT MAX(CAST(SUBSTRING(inventory_location."slug",9) AS INTEGER)) FROM inventory_location);
		IF last_slug_seq IS NULL THEN
			next_slug_seq := 1;
		ELSE
			next_slug_seq := (SELECT MAX(CAST(SUBSTRING(inventory_location."slug",9) AS INTEGER))+1 FROM inventory_location);
		END IF;
		slug := CONCAT('IL-', DATE_PART('year', CURRENT_DATE), '-', next_slug_seq);
		
		room := trim(both '"' from split_part(location,';;',5));

		
		the_year := DATE_PART('year', CURRENT_DATE);
		
		year_seq := (SELECT MAX("yearSequence")+1 FROM inventory_location);
		IF year_seq IS NULL THEN
			year_seq := 1;
		END IF;

		INSERT INTO public.inventory_location ("area", "building", "contactId", "externalId", "name", "organizationId", "slug", "room", "year", "yearSequence" )
			VALUES (area, building, contact_id, external_id, name, organization_id, slug, room, the_year, year_seq );

			
	END LOOP;
END;
$$