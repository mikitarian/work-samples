#!/bin/bash

# This script allows the user to deploy or revert Demo User Accounts
# in the Keycloak Development Realm
#
# It is designed to be called by the manage_demo_db_records.sh script 
# found in the cssp-nx/scripts directory
# 
# TO USE: 
#	1)  Open a GitBash Terminal Window (or some other terminal that can run a .sh script):
#
#	2)	Change directory to the cssp-nx/scripts directory
#			
#			cd /your/path/to/cssp-nx/scripts
#
#	3)	Run the manage_demo_db_records.sh script:
#
#			./manage_demo_db_records.sh
#
#	4)	Choose one of the Keycloak actions at the prompt
# 
# Sam Mikitarian
# 14 Dec 2021


# Declare users array
declare a demo_users=("daniel.baggeroer" "joseph.bagwell" "justin.boucher" "david.brady" "sean.brennan" "grover.carleton" \
						"rodney.cesar" "michael.gerba" "suzanne.ham" "jessie.henline" "joshua.hindman" "garrett.johnson" \
						"donald.lane" "bill.littleton" "sam.mikitarian" "rick.norwood" "alexis.phillips" "chase.riera" \
						"craig.rosenberger" "dee.smith" "christopher.thomas" "kyle.wukawitz")


error_exit()
{
	echo ""
    echo "Error: $1"
	echo ""
    exit 1
}


# Login to Keycloak:
login_to_keycloak(){

	read -ep "Enter the Keycloak admin Username [enter \"d\" for default]: " keycloak_admin_username

	read -ep "Enter the Keycloak admin Password [enter \"d\" for default]: " keycloak_admin_password
	
	read -ep "Enter the Keycloak Port Number (ex: 8080)  [enter \"d\" for default] " keycloak_port_number

		# check admin username
		if [[ -z "$keycloak_admin_username" ]]
		then
			error_exit "Keycloak admin Username cannot be blank.  Please try again."
		elif [[ "$keycloak_admin_username" -eq "d" ]] 
		then
			keycloak_admin_username='admin'
		fi
		
		#check admin password
		if [[ -z "$keycloak_admin_password" ]]
		then
			error_exit "Keycloak admin Password cannot be blank.  Please try again."
		elif [[ "$keycloak_admin_password" -eq "d" ]]
		then
			keycloak_admin_password='admin'
		fi
		
		#check port number
		if [[ "$keycloak_port_number" == 'd' ]]
		then
			keycloak_port_number='8080'
		elif [[ -z "$keycloak_port_number" ]]
		then
			error_exit "Keycloak Port Number cannot be blank.  Please try again."
		elif ! [[ $keycloak_port_number =~ ^[0-9]+$ ]]
		then
			error_exit "Keycloak Port Number must be a number.  Please try again."
		fi

		./kcadm.sh config credentials --server http://localhost:"$keycloak_port_number"/auth --realm master --user "$keycloak_admin_username" --password "$keycloak_admin_password" \
			2> /dev/null || error_exit "Invalid Username, Password or Port Number entered.  Please try again."
}
						
						
# Add Demo Users to Keycloak Development Realm
add_demo_users(){

	# arr=("$@") is the "${demo_users[@]}" array after being passed into the function from case statement below
	arr=("$@")
	count=0
	
	# Turn OFF Update Password on first login for all Development realm users
	./kcadm.sh update /authentication/required-actions/UPDATE_PASSWORD -r Development -s alias=UPDATE_PASSWORD -s defaultAction=false -s enabled=false -s name="Update Password" -s priority=30 -s providerId=UPDATE_PASSWORD
	
	for u in "${arr[@]}";
	do

		let count++
		./add-user-keycloak.sh -r Development -u "$u" -p password
		
		if [ $count -eq ${#arr[@]} ]
		then
			printf "\n\n     Finished adding users to Keycloak."
			printf "\n\n"
			printf "\n\n     After Keycloak restarts, login to Keycloak and confirm the users have been added."
			printf "\n       Then Export the list of Users from the FLIMS Manage Demo Data script"
		fi
		
	done
	
	# Turn ON Update Password on first login for all Development realm users (It was turned off when users were added.  Now we need to re-set it.)
	./kcadm.sh update /authentication/required-actions/UPDATE_PASSWORD -r Development -s alias=UPDATE_PASSWORD -s defaultAction=true -s enabled=true -s name="Update Password" -s priority=30 -s providerId=UPDATE_PASSWORD
	
}

# Export Users from Keycloak Development Realm:
export_demo_users(){

	# arr=("$@") is the "${demo_users[@]}" array after being passed into the function from case statement below
	arr=("$@")
	if [ -f "./demo_users_with_ids.txt" ]
	then
		rm  ./demo_users_with_ids.txt
	fi
	touch ./demo_users_with_ids.txt
	chmod 664 ./demo_users_with_ids.txt
	count=0
	
	for u in ${arr[@]};
	do
		let count++
		
		id=$(./kcadm.sh get users -r Development -q username="$u" | grep -E '"id" :' | sed 's/  "id" : "//' | sed 's/",//')
		
		echo "appending '$u,$id' to file"
		
		echo "$u,$id" >> ./demo_users_with_ids.txt

	done
	
}


# Delete All Users from Keycloak Development Realm:
delete_demo_users(){
	arr=("$@")
	for u in ${arr[@]};
	do
		
		id=$(./kcadm.sh get users -r Development -q username="$u" | grep -E '"id" :' | sed 's/  "id" : "//' | sed 's/",//')
		
		if [[ -n "$id" ]]
		then
			echo "Deleting user account: $u"
			./kcadm.sh delete users/"$id" -r Development
		else
			echo "id is null.  Login to Keycloak and delete $u manually."
		fi
		
	done
}

main_menu()
{
	
	printf "\nThis is the Keycloak User Management script."
	printf "\n"
	printf "\nChoose your next action:"
	printf "\n     -Add Users to Keycloak (a)"
	printf "\n     -Export a list of Keycloak Users (e)"
	printf "\n     -Delete Users from Keycloak (d)"
	printf "\n     -Quit this script (q)"
	printf "\n"
	read -n1 -p "Enter your choice here: " decision
		case "$decision" in
			a)
				printf "\n\n     -you chose \"Add Users to Keycloak\"\n\n"
				login_to_keycloak
				add_demo_users "${demo_users[@]}"
				;;
			e)
				printf "\n\n     -you chose \"Export list of Keycloak Users\"\n\n"
				login_to_keycloak
				export_demo_users "${demo_users[@]}"
				;;
			d)
				printf "\n\n     -you chose \"Delete Users from Keycloak\"\n\n"
				login_to_keycloak
				delete_demo_users "${demo_users[@]}"
				;;
			q)
				printf "\n\n     Quitting script\n\n"
				exit 0
				;;
			*)
				printf "\n\n***********************************************************************************************************"
				printf "\n\nValid inputs are \"a\", \"e\", \"r\" and \"q\"."
				printf "\n\nAnything else will bring you to this message."
				printf "\n\nIf those inputs are not being accepted, contact the Dev Team for help.\n\n"
				printf "\n***********************************************************************************************************\n"
				;;
		esac
}

main_menu