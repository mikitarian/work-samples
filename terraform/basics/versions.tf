terraform {
  required_version = ">= 1.0.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }

  backend "s3" {
    encrypt        = true
    bucket         = "cloud-manager-terraform-state"
    dynamodb_table = "cloud-manager-terraform-lock"
    key            = "basics.tfstate"
    region         = "us-gov-east-1"
  }
}

provider "aws" {
  region = "us-gov-east-1"
}
