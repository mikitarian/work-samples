### AppDynamics Controller and Events Service Installation

##### Prepare a target directory for the Controller database

Log in to the Enterprise Console via a terminal window.

Create the `/opt/appd-controller` directory and grant permissions to the `appdynamics` user.

```
cd /opt
mkdir appd-controller
chown -R appdynamics: appd-controller
chmod -R 755 appd-controller
```

##### Install Components via the Enterprise Console web interface

This process will install:

* Platform
* Host
* Controller
* Events Service

**NOTE:** command-line equivalents are shown at the bottom of the page.

A Platform is the unique identifier for the entity you want to monitor.

In our case, the Platform is called CIRRAS.

The Host, Controller and Events Service will be installed in the CIRRAS Platform.

**Log in to the Enterprise Console via the web interface**

https://\<server ip address or DNS name\>:9191
Enterprise Console Default username:  admin
Enterprise Console Default password:  <whatever we set in the response.varfile>

Click "Custom Install"

**Step 1:  Create the Platform**  
Name = CIRRAS  
Description = CIRRAS Application Performance Management Platform  
Installation Path = <accept default value>  
Click "Create Platform"  

**Step 2:  Specify Hosts**
Click "+ Add Enterprise Console Host"  
(should already be selected by default)  
DO NOT add a second host, and DO NOT add any credentials  
Click "Add Hosts"  

**Step 3:  Install Controller**
Click the "Install" button (should already be selected)  
Target Version = accept default  
Profile = Small  
Controller Primary Host = select the host value entered in Step 2 above  
Controller Secondary Host = leave it blank  
Tenancy Mode = single  
DB Data Directory = /opt/appd-controller  
Controller Admin Username = admin  
Controller Admin Password = (same password created for Enterprise Console admin user)  
Controller Root Password\*\* = (new value applies only to Controller Root User, NOT host root)  
DB Root Password = (new value applies only to Controller DB, NOT Enterprise Console DB)  
Port Values = accept defaults for all of them  
Click "Submit"  

\*\*The Controller Root User is a built-in AppDynamics account with global administrator privileges in the AppDynamics Controller environment (but NOT in the server's OS)

**Step 4:  Install Events Service**
Click the "Install" button (should already be selected)  
Target Version = accept default  
Profile = Prod  
Data Directory = accept default  
Port Values = accept defaults for all of them  
JVM Temporary Directory Override = /opt/tmp  
Host = select the host value entered in Step 2 above  
Click "Submit"  


##### Command Line Interface examples for this process

The following commands will accomplish the same thing as the Web Interface procedures above.
(These can also be incorporated into an Ansible Playbook to automate deployments.)

**REMEMBER**  Elastic Search CANNOT run under `root` ownership, so do the install as the `appdynamics` user.

**CLI: Create the Platform**
```
mkdir /opt/<platform-name>
chown -R appdynamics: /opt/<platform-name>
su appdynamics
platform-admin.sh create-platform --name <platform-name> --installation-dir /opt/<platform-name>
```

to delete a platform:
`platform-admin.sh delete-platform --name <platform-name>`


**CLI: Specify Hosts**

`platform-admin.sh add-hosts --platform-name <platform-name> --hosts <ip address or hostname>`

to remove a host:
`platform-admin.sh remove-hosts --platform-name <platform-name> --hosts <ip address or hostname>`


**CLI: Install Controller**

```
su appdynamics
cd /opt/<platform-name>

platform-admin.sh submit-job --platform-name <platform-name> --service controller --job install --args controllerProfile=small controllerPrimaryHost=<ip address or hostname> mysqlDataDir=/opt/<platform-name>/controller/db/data controllerAdminUsername=admin controllerAdminPassword=<your controller admin password> controllerRootUserPassword=<your controller root user password> newDatabaseRootPassword=<your controller db root password> newDatabaseUserPassword=<your controller db user password> controllerDBHost=<ip address or hostname>
```
Here's the same command in an easier-to-read format:

	platform-admin.sh submit-job --platform-name <platform-name> --service controller --job install --args 
		controllerProfile=small
		controllerPrimaryHost=<ip address or hostname>
		mysqlDataDir=/opt/<platform-name>/controller/db/data
		controllerAdminUsername=admin
		controllerAdminPassword=<your controller admin password>
		controllerRootUserPassword=<your controller root user password>
		newDatabaseRootPassword=<your controller db root password>
		newDatabaseUserPassword=<your controller db user password>
		controllerDBHost=<ip address or hostname>

**NOTE:** Instead of listing all `--args` on the command line, you can use the `--arg-file <arg-file>` option to store arguments in a file.  Arguments should be specified in the format 'key=value', one per line. The same key may be used with multiple values.  See the `response.varfile` used with the Enterprise Console as an example.

to uninstall the Controller:
`platform-admin.sh submit-job --platform-name CIRRAS --service controller --job uninstall`


**CLI: Install Events Service**

```
platform-admin.sh submit-job --platform-name <platform-name> --service events-service --job install --args profile=Prod eventsServiceDataDir=/opt/<your-target-directory>/data eventsServiceElasticSearchPort=9200 eventsServiceRestApiPort=9080 eventsServiceRestApiAdminPort=9081 eventsServiceUnicastPort=9300 jvmTempDir=/opt/tmp serviceActionHost=<ip address or hostname>
```

Here's the same command in an easier-to-read format:
```
	platform-admin.sh submit-job --platform-name <platform-name> --service events-service --job install --args 
		profile=Prod 
		eventsServiceDataDir=/opt/<your-target-directory>/data 
		eventsServiceElasticSearchPort=9200 
		eventsServiceRestApiPort=9080 
		eventsServiceRestApiAdminPort=9081 
		eventsServiceUnicastPort=9300 
		jvmTempDir=/opt/tmp 
		serviceActionHost=<ip address or hostname>
```

to uninstall the Events Service:
`platform-admin.sh submit-job --platform-name CIRRAS --service events-service --job uninstall`

---

### Useful CLI Commands:

**Verify Controller Status**

In a web browser, go to:
http://\<server ip address or hostname\>:\<http-listener-port\>/controller/rest/serverstatus
example:  `http://10.205.199.41:8090/controller/rest/serverstatus`

Output will be like this:
```
<serverstatus vendorid="" version="1">
  <available>true</available>
  <serverid/>
  <serverinfo>
    <vendorname>AppDynamics</vendorname>
    <productname>AppDynamics Application Performance Management</productname>
    <serverversion>023-011-001-000</serverversion>
    <implementationVersion>Controller v23.11.1.0 Build 23.11.1-10118 Commit 2418cebd766899336583536a8f2f4e73984c0a2f</implementationVersion>
  </serverinfo>
  <startupTimeInSeconds>5</startupTimeInSeconds>
</serverstatus>
```