/* DELETE records from user TABLE

	Delete all users EXCEPT FOR Admin user.
	
	Sam Mikitarian 29 Dec 2021
*/

DO $$
DECLARE

BEGIN

	-- DELETE all users EXCEPT FOR Admin;
	DELETE FROM public.user WHERE LOWER("username") NOT IN ('admin');
	
END;
$$