# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias python='python3'
alias python3='/usr/bin/python3.9'
alias cddev='cd ~/dev'
alias cddevwin='cd /mnt/c/devwin'
alias cddownloads='cd /mnt/c/Users/d-sam.mikitarian/Downloads'
alias cddocs='cd /mnt/c/Users/d-sam.mikitarian/OneDrive\ -\ AFS\ Defense/Documents'
alias checkecs='~/myscripts/check-ecs-exec.sh'
alias conffile='sudo ~/myscripts/makeconffile.sh'
alias dockerlogin='printf "Be very patient - Docker Login can take a loooooong time ...\n" && cat ~/.docker/docker | docker login --username mikitarian --password-stdin'
alias pc='TAG=v1.76.0; docker run -v $(pwd):/lint -w /lint ghcr.io/antonbabenko/pre-commit-terraform:${TAG} run -a'

alias awsctdev='export AWS_PROFILE=ctdev; export AWS_DEFAULT_REGION=us-east-1; export AWS_REGION=us-east-1; export AWS_ACCESS_KEY_ID=$( aws --profile ctdev configure get aws_access_key_id ); export AWS_SECRET_ACCESS_KEY=$( aws --profile ctdev configure get aws_secret_access_key )'
alias awsctsb='export AWS_PROFILE=ctsb; export AWS_DEFAULT_REGION=us-east-1; export AWS_REGION=us-east-1;'
alias awscm='export AWS_PROFILE=cm; export AWS_DEFAULT_REGION=us-gov-east-1; export AWS_REGION=us-gov-east-1; \
			export ASPNETCORE_ENVIRONMENT=Development; \
			export AWS_ACCESS_KEY_ID=$( aws --profile cm configure get aws_access_key_id ); \
			export AWS_SECRET_ACCESS_KEY=$( aws --profile cm configure get aws_secret_access_key );'
alias awscmiu='export AWS_PROFILE=cm-installation-user; \
			export AWS_DEFAULT_REGION=us-gov-east-1; \
			export AWS_REGION=us-gov-east-1; \
			export ASPNETCORE_ENVIRONMENT=Development; \
			export AWS_ACCESS_KEY_ID=$( aws --profile cm-installation-user configure get aws_access_key_id ); \
			export AWS_SECRET_ACCESS_KEY=$( aws --profile cm-installation-user configure get aws_secret_access_key );'
alias awsvars='echo "AWS_PROFILE is: $AWS_PROFILE"; echo "AWS_DEFAULT_REGION is: $AWS_DEFAULT_REGION"; \
				echo "AWS_REGION is: $AWS_REGION"; echo "ASPNETCORE_ENVIRONMENT is: $ASPNETCORE_ENVIRONMENT"; \
				echo "AWS_ACCESS_KEY_ID IS: $AWS_ACCESS_KEY_ID"; \
				echo "AWS_SECRET_ACCESS_KEY is: $AWS_SECRET_ACCESS_KEY";'
alias awsclear='export AWS_PROFILE=; export AWS_DEFAULT_REGION=; export AWS_REGION=; export ASPNETCORE_ENVIRONMENT=; export AWS_ACCESS_KEY_ID=; export AWS_SECRET_ACCESS_KEY=;'
# alias awsberico='export AWS_PROFILE=berico; export AWS_DEFAULT_REGION=us-east-1; export AWS_REGION=us-east-1'


# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# stolen from tjisblogging.blogspot.com/2020/06/how-to-set-terminal-title-in-ubuntu.html
function stitle(){
 if [ -z "$PS1_BACK" ];  # set backup if it is empty
 then
  PS1_BACK="$PS1"
 fi

 TITLE="\[\e]0;$*\a\]"
 PS1="${PS1_BACK}${TITLE}"
}

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
export CM2DIR=/mnt/c/devwin/CloudManager
export JAVA_HOME=/opt/jdk-18
export MAVEN_HOME=/opt/apachemaven
export DOTNET_ROOT=~/.dotnet
export SWISS_ARMY_KNIFE=/mnt/c/dev/eaads-devops/cfn/tools/swissArmyKnife.sh
export MSSQLCMD=/opt/mssql-tools/bin
export PYODBC=/etc
export PYODBC_DATA_SOURCES=/etc/ODBCDataSources
export PATH=$PATH:$DOTNET_ROOT:$DOTNET_ROOT/tools:$JAVA_HOME/bin:$MAVEN_HOME/bin:$SWISS_ARMY_KNIFE:~/.local/bin:$MSSQLCMD:$PYODBC:$PYODBC_DATA_SOURCES
#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

complete -C /usr/bin/terraform terraformexport
