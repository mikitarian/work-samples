# demo_data_tools.sh #

<span style="margin-left: 5%;">**NOTE:** This script will NOT run as-is.</span>  
<span style="margin-left: 5%;">AWS ARNs have been removed and some data sanitization has been done</span>  
<span style="margin-left: 5%;">so the script can be reviewed outside of proprietary channels.</span>  

This `bash` script will reload application data on demand to SQL Server and to DynamoDB

- SQL Server records include Customer Service Orders, Funding Allocations, Purchase Agreements, etc.
- DynamoDB records include Application User Accounts. 

### Requirements ###

Requirements to run this script are:

- **AWS Command Line Interface (CLI)** installed locally and an account in the target AWS environment  
    <span style="margin-left: 5%;">aws cli info here: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html</span>  

- **sqlcmd** and **jq** utilities installed locally  
    <span style="margin-left: 5%;">sqlcmd info here: https://learn.microsoft.com/en-us/sql/linux/sql-server-linux-setup-tools</span>  
    <span style="margin-left: 5%;">jq info here: https://confluence.accenturefederal.com/display/EAAD/Miscellaneous+Utilities</span>  

### To Run the Script ###

- Ensure you are logged in to AWS  
- From a linux/unix command prompt, cd into the `demo_data_tools` directory
- Launch the `demo_data_tools.sh` script  
- follow the prompts  

### Script Inputs and Outputs ###

<span style="margin-left: 5%;">SQL Server scripts are found in:</span>  
<span style="margin-left: 10%;">`demo_data_tools/scripts`</span>  

<span style="margin-left: 5%;">DynamoDB outputs are found in:</span>  
<span style="margin-left: 10%;">`demo_data_tools/ouputs`</span>  


```  
./demo_data_tools/
├── outputs
│   ├── dev
│   │   ├── schemas
│   │   └── users
│   └── test
│       └── users
└── scripts
    ├── add_tables
    ├── delete_records
    ├── drop_tables
    ├── insert_records
    └── misc
```

