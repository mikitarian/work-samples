# Terraform Work Sample

This is the Terraform file and directory structure used to deploy an application into the AWS environment.

It demonstrates:  
 - task organization
 - use of community modules
 - AWS integration
 - moderate level of complexity

Actual code samples to follow.

```
.
├── README.md
├── basics
│   ├── README.md
│   ├── main.tf
│   ├── outputs.tf
│   ├── templates
│   ├── variables.tf
│   └── versions.tf
├── community-modules
│   ├── terraform-aws-dynamodb-table
│   ├── terraform-aws-rds
│   └── terraform-aws-vpc
├── deploy
│   ├── README.md
│   ├── data
│   ├── env
│   ├── main.tf
│   ├── out.txt
│   ├── outputs.tf
│   ├── roll_ecs.sh
│   ├── run_terraform.sh
│   ├── variables.tf
│   └── versions.tf
├── modules
│   ├── aws-iam-role
│   ├── aws-lb
│   └── ecs-service
└── state
    ├── README.md
    ├── main.tf
    ├── providers.tf
    └── variables.tf
```