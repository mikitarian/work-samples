import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BreedsComponent } from './components/breeds/breeds.component';
import { BreedDialogComponent } from './components/breed-dialog/breed-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home.component';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSliderModule } from '@angular/material/slider';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RatingsComponent } from './components/ratings/ratings.component';
import { VeterinariansComponent } from './components/veterinarians/veterinarians.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { UserListComponent } from './components/user-list/user-list.component';

@NgModule({
  declarations: [
    AppComponent,
    BreedsComponent,
    BreedDialogComponent,
    HomeComponent,
    RatingsComponent,
    VeterinariansComponent,
    LoginFormComponent,
    UserListComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatDialogModule,
    MatSliderModule,
    NgxDatatableModule,
    ReactiveFormsModule,
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
