SET NOCOUNT ON;

-- -----------Prereq Records for AlertRoleXWalk and Alerts-------------------

SET IDENTITY_INSERT $(dbname).dbo.AlertRoles ON

INSERT INTO $(dbname).dbo.AlertRoles (Id,[Role]) VALUES
	 (1,N'Cloud_Approver'),
	 (2,N'Cloud_Admin'),
	 (3,N'KO');
	 
SET IDENTITY_INSERT $(dbname).dbo.AlertRoles OFF


SET IDENTITY_INSERT $(dbname).dbo.AlertType ON

INSERT INTO $(dbname).dbo.AlertType (Id,[Type]) VALUES
	 (1,N'CSO Submitted'),
	 (2,N'CSO Approved'),
	 (3,N'CSO Rejected'),
	 (4,N'Purchase Request Submitted'),
	 (5,N'Purchase Request Approved'),
	 (6,N'Purchase Request Rejected');
	 
SET IDENTITY_INSERT $(dbname).dbo.AlertType OFF

-- -----------End Prereq Records for AlertRoleXWalk and Alerts-------------------

SET IDENTITY_INSERT $(dbname).dbo.AlertRoleXWalk ON

INSERT INTO $(dbname).dbo.AlertRoleXWalk (Id,AlertTypeId,AlertRoleId) VALUES
	 (1,1,1),
	 (2,2,3),
	 (3,2,2),
	 (4,4,3),
	 (5,5,2);

SET IDENTITY_INSERT $(dbname).dbo.AlertRoleXWalk OFF


/*
INSERT INTO $(dbname).dbo.AlertRoleXWalk (AlertTypeId,AlertRoleId) VALUES
	 ((SELECT Id FROM $(dbname).dbo.AlertType WHERE TYPE = 'CSO Submitted'),(SELECT Id FROM $(dbname).dbo.AlertRoles WHERE Role = 'Cloud_Approver')),
	 ((SELECT Id FROM $(dbname).dbo.AlertType WHERE TYPE = 'CSO Approved'),(SELECT Id FROM $(dbname).dbo.AlertRoles WHERE Role = 'KO')),
	 ((SELECT Id FROM $(dbname).dbo.AlertType WHERE TYPE = 'CSO Approved'),(SELECT Id FROM $(dbname).dbo.AlertRoles WHERE Role = 'Cloud_Admin')),
	 ((SELECT Id FROM $(dbname).dbo.AlertType WHERE TYPE = 'Purchase Request Submitted'),(SELECT Id FROM $(dbname).dbo.AlertRoles WHERE Role = 'KO')),
	 ((SELECT Id FROM $(dbname).dbo.AlertType WHERE TYPE = 'Purchase Request Approved'),(SELECT Id FROM $(dbname).dbo.AlertRoles WHERE Role = 'Cloud_Admin'));
*/

SET NOCOUNT OFF;