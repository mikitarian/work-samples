# Python 3 code to rename multiple files in a directory
# Sam Mikitarian
# 21 June 2023

# importing modules
import os
from sys import platform
import time

# general utils
def clear_console():
	if platform == "linux" or platform == "linux2":
		# linux
		os.system('clear')
	elif platform == "darwin":
		# OS X
		os.system('clear')
	elif platform == "win32":
		# Windows
		os.system('cls')

def go_nogo():
	
	decision = input("\nWould you like to proceed? [y/n] ")
  	
	if decision == "y":
		clear_console()
		get_target_dir()
	else:
		print("\nBye!\n")
		time.sleep(1.25)
		clear_console()
		quit()

# Tell the user what the script does
def info():

	print("\nThis script will rename files in a target directory through string substitution.\n")
	print("You provide the directory, the old substring to remove, and the new substring that will take it\'s place.")
	go_nogo()

def get_target_dir():
	
	global target_dir
	target_dir = os.getcwd()
	
	print("\nCurrent directory is {}\n".format(target_dir))
	alt_folder = input("Enter path to directory where renaming will occur (relative paths are OK), or hit Enter to use the current directory:\n")
	
	if len(alt_folder) > 0:
		target_dir = alt_folder
  
	clear_console()
 
	verify_dir()

def verify_dir():

	if not os.path.exists(target_dir):
		print("There is no directory named \"{}\".\nPlease check the spelling and try again.\n".format(target_dir))
		get_target_dir()
	else:
		get_old_string()

def get_old_string():
	
	global old_string
	old_string = input('Enter the part of the filename to be replaced - remember - this is CaSe SeNsItIvE:  ')
 
	clear_console()

	verify_string(old_string, get_old_string, get_new_string)

	
def get_new_string():

	global new_string
	new_string = input('Enter the new part of the filename - remember - this is CaSe SeNsItIvE:  ')

	clear_console()
 
	verify_string(new_string, get_new_string, review_it)
 
 
def verify_string(the_string, calling_function, next_function):
    
    bad_chars = re.findall(r'[\\\\/<>:"\'|?*]', the_string)
    
    if len(bad_chars) > 0:
        print("\nYou entered {}, which contains these special characters...{}".format(the_string, bad_chars))
        print("\nThe following special characters are not allowed in file names: \\ / < > : \" \' | ? *")
        print("\nPlease check the string and try again.\n")
        calling_function()
    elif len(the_string) < 1:
        print("string cannot be blank")
        calling_function()
    else:
        next_function()
        
def review_it():
	print("\nFor every file in this directory:")
	print("{}\n".format(target_dir))
	print("Files with \"{}\" in the name...".format(old_string))
	print("Will have that replaced with \"{}\"\n".format(new_string))
	
	decision = input("\nWould you like to proceed? [r = rename now, s = start over, q = quit] ")
  	
	if decision == "r":
		print("\n")
		rename_now()
	elif decision == "s":
		clear_console()
		get_target_dir()
	else:
		print("\nBye!\n")
		time.sleep(1.25)
		clear_console()
		quit()
	
def rename_now():
	total_files = 0
	renamed_files = 0
	print("Renaming files in {}...\n".format(target_dir))
	for count, filename in enumerate(os.listdir(target_dir)):
		if os.path.isfile(f"{target_dir}/{filename}"):
			total_files += 1
			if old_string in filename:
				renamed_files +=1
				newname = filename.replace(old_string, new_string)
				print('{} -> {}'.format(filename, newname, 'right aligned'))
				
				src =f"{target_dir}/{filename}"
				dst =f"{target_dir}/{newname}"
	
				os.rename(src, dst)
   
	print("\nOf the {} files in \"{}\"".format(total_files, target_dir))
	print("{} files were renamed".format(renamed_files))
  

# Check if this script is being called directly by the python interpreter
if __name__ == '__main__':
	
	# Kick things off by calling info() function
	info()