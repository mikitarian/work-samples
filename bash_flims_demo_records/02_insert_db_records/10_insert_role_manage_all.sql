/* INSERT records into role TABLE

	Create the MANAGE_ALL role and apply it to Admin user.
	
	NOTE: tenantId of 1 and admin_user_id of 1 
	come from initial seeding of FLIMS DB at app deployment
	
	"id" is generated by DB Sequence,
	and is not included in the SQL below

	Sam Mikitarian 15 Dec 2021
*/

/*
role table structure
 active | id |          name           | tenantId
--------+----+-------------------------+----------
 t      |  1 | FLIC_ADMIN              |        1


user_roles_role table structure
 userId | roleId
--------+--------
      1 |      1
      1 |     26
*/

DO $$
<<check_for_existing_role>>
DECLARE
	
	--Using a nested block structure here because in Development I noticed duplicate entries for the 'MANAGE_ALL' role
	--It was probably developer error, but better safe than sorry
	
	existing_role_id INTEGER := (SELECT id FROM role WHERE UPPER(name)='MANAGE_ALL');
	
	BEGIN
	
		IF existing_role_id IS NULL THEN

			-- Add the MANAGE_ALL role;
			INSERT INTO role ("active","name","tenantId") VALUES (true,'MANAGE_ALL',1);


			DECLARE
				permission_ids integer[] := array[(SELECT ARRAY(
													SELECT id::integer
													FROM permission
													WHERE "name" LIKE 'MANAGE_%'))];
				permission_id integer;
				new_role_id integer := (SELECT id FROM role WHERE UPPER(name)='MANAGE_ALL');
				admin_user_id integer := (SELECT id FROM public.user WHERE LOWER("username")='admin');
			BEGIN
				FOREACH permission_id IN ARRAY permission_ids LOOP

					INSERT INTO role_permissions_permission ("roleId", "permissionId")
						VALUES (new_role_id, permission_id);

				END LOOP;
				
				INSERT INTO user_roles_role ("userId","roleId") VALUES (admin_user_id, new_role_id);
				
			END;
			
		END IF;

END check_for_existing_role $$