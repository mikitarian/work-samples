/*	INSERT records into public.user TABLE
	
	Keycloak Usernames and Ids are imported from
	demo_users_with_ids.txt into a temporary table named "keycloak_users_tbl"
	
	User table columns "createDate" and "id" are populated by DB
	and do not need to be specifically set in this script
	
	Sam Mikitarian 15 Dec 2021
*/

/*
public.user table structure
 active |  createDate  |  externalId  | id | personId | slug | tenantId | username | year | yearSequence
--------+--------------+--------------+----+----------+------+----------+----------+------+--------------
*/

SET client_min_messages TO WARNING;

DROP TABLE IF EXISTS keycloak_users_tbl;
CREATE TABLE IF NOT EXISTS keycloak_users_tbl(username varchar, id varchar);
ALTER TABLE keycloak_users_tbl OWNER TO "user";
COPY keycloak_users_tbl(username, id) FROM '/opt/app-root/src/02_insert_db_records/demo_users_with_ids.txt' delimiter ',';
SELECT * FROM keycloak_users_tbl;

SET client_min_messages TO NOTICE;

DO $$

	DECLARE
		temprow RECORD;
		keycloak_id VARCHAR;
		last_slug_seq INTEGER;
		next_slug_seq INTEGER;
		slug VARCHAR;
		tenant_count INTEGER := (SELECT COUNT(*) FROM tenant);
		offset_count INTEGER :=0;
		tenant_id INTEGER;
		keycloak_username VARCHAR;
		the_year INTEGER;
		year_seq INTEGER := 0;
BEGIN
	
	FOR temprow IN
		SELECT ROW_NUMBER () OVER (ORDER BY id), keycloak_users_tbl.username, keycloak_users_tbl.id FROM keycloak_users_tbl
	LOOP
		
		keycloak_id := temprow.id;
		last_slug_seq = (SELECT MAX(CAST(SUBSTRING(public.user."slug",8) AS INTEGER)) FROM public.user);
		IF last_slug_seq IS NULL THEN
			next_slug_seq := 1;
		ELSE
			next_slug_seq := last_slug_seq + 1;
		END IF;
		slug := CONCAT('U-', date_part('year', CURRENT_DATE), '-', next_slug_seq);
		
		tenant_id := (SELECT "id" FROM tenant ORDER BY "id" LIMIT 1 OFFSET offset_count);
		IF offset_count = (tenant_count -1) THEN
			offset_count :=0;
		ELSE
			offset_count := offset_count + 1;
		END IF;
			
		keycloak_username := temprow.username;
		the_year := date_part('year', CURRENT_DATE);
		year_seq := (SELECT MAX(public.user."yearSequence")+1 FROM public.user);
		IF year_seq IS NULL THEN
			year_seq := 1;
		END IF;
		
		INSERT INTO public.user("active","externalId","slug","tenantId","username","year","yearSequence") 
			VALUES (true, keycloak_id, slug, tenant_id, keycloak_username, the_year, year_seq);
		
	END LOOP;

END;
$$