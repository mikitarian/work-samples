output "this_lb_id" {
  description = "The ID and ARN of the load balancer we created."
  value       = concat(aws_lb.this.*.id, [""])[0]
}

output "this_lb_arn" {
  description = "The ID and ARN of the load balancer we created."
  value       = concat(aws_lb.this.*.arn, [""])[0]
}

output "this_lb_dns_name" {
  description = "The DNS name of the load balancer."
  value       = concat(aws_lb.this.*.dns_name, [""])[0]
}

output "this_lb_arn_suffix" {
  description = "ARN suffix of our load balancer - can be used with CloudWatch."
  value       = concat(aws_lb.this.*.arn_suffix, [""])[0]
}

output "this_lb_zone_id" {
  description = "The zone_id of the load balancer to assist with creating DNS records."
  value       = concat(aws_lb.this.*.zone_id, [""])[0]
}

output "target_groups" {
  description = "Target group objects"
  value       = aws_lb_target_group.main
}

output "http_listeners" {
  description = "HTTP load balancer listener objects"
  value       = aws_lb_listener.frontend_http_tcp
}

output "https_tcp_listeners" {
  description = "HTTPS load balancer listener objects"
  value       = aws_lb_listener.frontend_https
}
