/*	DELETE records from person TABLE
		
	Sam Mikitarian 04 Jan 2022
*/

DO $$
DECLARE

BEGIN

	DELETE FROM person;
	
END;
$$