/*	DELETE records from inventory_location TABLE
	
	Sam Mikitarian 04 Jan 2022
*/

DO $$
	DECLARE
	
		tables_array VARCHAR[] := ARRAY['inventory_item_type', 'task_type'];
		
		the_table VARCHAR;
		
		sql_stmt VARCHAR;
		
BEGIN
	FOREACH the_table IN ARRAY tables_array LOOP
	
		sql_stmt := concat('DELETE FROM ', the_table , ';');
		EXECUTE sql_stmt;
		
	END LOOP;
END;
$$