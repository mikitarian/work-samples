# WHERE WE'RE SENDING THE DATA
datawarehouse_db_config = {
  'driver': '{SQL Server}',
  'server': 'proxy.cm2.cloudtracker.net',
  'database': 'CM_TEST_INSTALL',
  'user': 'cloud_manager',
  'password': '%%%%%',
  'autocommit': True,
}

# SOURCE_DB - Where we're pulling the initial data from
sqlserver_db_config = [
  {
    'driver': '{SQL Server}',
    'server': 'devdb.cm.cloudtracker.net',
    'database': 'PR-20221221-1',
    'user': 'ecmauser',
    'password': '%%%%%!',
    'autocommit': True,
  }
]
