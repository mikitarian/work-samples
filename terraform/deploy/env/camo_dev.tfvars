# the environment name, used to prefix resource names along with cm
# for example, cm-dev-<resource>
# also passed to services as ASPNETCORE_ENVIRONMENT environment varible to pull secrets and parameters
environment = "dev"

# the FQDN (used for S3 redirects)
fqdn = "<placeholder>"

# name used for the UI S3 bucket
# if we need to create a CNAME (for ZPA), this should be the FQDN of the CNAME
# otherwise, it can be any S3 bucket name
ui_fqdn = "<placeholder>"

# image tag to use when pulling images
image_tag = "latest"

# option prefix list for ingress into api-gateway
# web_ingress_prefix_list = ["<placeholder>"]

# optional list of CIDR for ingress into api-gateway
# web_ingress_cidr_list = ["<placeholder>", "<placeholder>"]

# following values come from the resources created in BASICS
# currently we are using the same role for execution (creating the service) and task (ongoing running of service)
ecs_execution_role = "<placeholder>"
ecs_task_role      = "<placeholder>"

vpc_id = "<placeholder>"

# the CIDR block of the VPC
# used for NLB health check ingress and other security group rules
vpc_cidr_block = "<placeholder>"

# subnets to create the ECS services
private_subnets = [
  "<placeholder>",
  "<placeholder>",
  "<placeholder>"
]

# subnets to create the external NLB
public_subnets = [
  "<placeholder>",
  "<placeholder>",
  "<placeholder>"
]
