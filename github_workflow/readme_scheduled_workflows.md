# About Scheduled Workflows

Scheduled Workflows run on the *latest commit* on the *default* (or *base*) branch.  
In MyApp's case, that's the `develop` branch.

[`General Info (from GitHub Docs)`](https://docs.github.com/en/actions/using-workflows/events-that-trigger-workflows#schedule)  
 
[`Specify Branch (from stackoverflow)`](https://stackoverflow.com/questions/58798886/github-actions-schedule-operation-on-branch)  

> __Note:__ The `schedule` event can be delayed during periods of high loads of GitHub Actions workflow runs. High load times include the start of every hour. To decrease the chance of delay, schedule your workflow to run at a different time of the hour.    

### All times are UTC and they DO NOT adjust for Daylight Saving Time    

__*During Daylight Saving Time*__ (spring & summer) there is a 4 hour difference between UTC and Eastern Time.  
So during Daylight Saving Time, a job starting at 10(AM) in UTC will start at 6(AM) Eastern Time.

__*During Standard Time*__ (fall & winter) there is a 5 hour difference between UTC and Eastern Time.  
So during Standard Time, a job starting at 10(AM) in UTC will start at 5(AM) Eastern Time.


__Workflows use POSIX cron syntax.__  
Cron fields are ordered and defined as follows:<br/>
<span style="margin:0;padding-left:20px;">Minute [0,59]</span>  
<span style="margin:0;padding-left:20px;">Hour [0,23]</span>  
<span style="margin:0;padding-left:20px;">Day of the month [1,31]</span>  
<span style="margin:0;padding-left:20px;">Month of the year [1,12]</span>  
<span style="margin:0;padding-left:20px;">Day of the week ([0,6] with 0=Sunday)</span>  
<span style="margin:0;padding-left:80px;">0=Sun, 1=Mon, 2=Tue, 3=Wed, 4=Thu, 5=Fri, 6=Sat</span>  

__Examples:__  
The cron entry below triggers the workflow at 5:30AM and 6:30PM UTC on Mon and Thu  
<span style="margin:0;padding-left:20px;">cron: '30 5,18 * * 1,4'</span>  

The cron entry below triggers the workflow at 5:30AM and 17:30PM UTC every day  
<span style="margin:0;padding-left:20px;">cron:  '30 5,17 * * *'</span>  

The cron entry below triggers the workflow every 5 minutes  
<span style="margin:0;padding-left:20px;">cron: '*/5 * * * *'</span>  
<span style="margin:0;padding-left:80px;font-size:12px;">_The SHORTEST interval a scheduled job can run is every 5 minutes_</span>
