from app_defaults import *
from ntcmain import *
from controllers.datatools import SpinnerListInfo

Builder.load_file('./views/reorder_list/ntcreorderlistmain.kv')

class NtcReorderList(Popup):
	def __init__(self, task_list_file_name, **kwargs):
		super().__init__(**kwargs)
		
		self.task_list_file_name = task_list_file_name
		self.file_name_with_path = str(ntc_task_list_directory)+'\\'+self.task_list_file_name
		self.original_list = []
		self.reordered_list = []
		self.ids.reorder_list_title_label.text='Reorder "'+self.task_list_file_name[:-14]+'" Task List'

		self.grid_width = windowWidth * .95
		self.reorder_list_label_widths = [self.grid_width * .02, 
								self.grid_width * .065,
								self.grid_width * .175, 
								self.grid_width * .09,
								self.grid_width * .12,
								self.grid_width * .175,
								self.grid_width * .07,
								self.grid_width * .07,
								self.grid_width * .07,
								self.grid_width * .065]

		self.populate_original_lists()
		self.display_reordered_list()

		self.ids.ntc_reorder_list_start_over_button.bind(on_press=lambda a:[self.start_over()])
		self.ids.ntc_reorder_list_save_button.bind(on_press=lambda a:[self.validate_list()])


	def clear_feedback_label(self):
		self.ids.reorder_list_feedback_label.text=''


	def populate_original_lists(self):

		prelim_original_list = TaskListInfo.get_task_list_contents(self, self.file_name_with_path)
		
		for task_index, task in enumerate(prelim_original_list):
			
			task_as_dict = json.loads(task)	# Python string to Python dict
			
			task_as_dict['original_task_index'] = str(task_index)	# append key/value pair to task_as_dict

			task_as_string = json.dumps(task_as_dict)	# Python dict to Python string

			self.original_list.append(task_as_string+'\n')	# append modified task & newline character

		self.display_original_list()


	def display_original_list(self):

		for task_index, data in enumerate(self.original_list):

			task = json.loads(data)

			if 'move' in task['task_status'].lower():
				move_button = NtcReorderListLabel(text='',width='50')
			else:
				move_button = NtcMoveListItemButton(text='Move')
				move_button.bind(on_press=lambda a,b=task_index,c=task_index,d='move':[self.move_to_new_list(b,c,d)])
			
			self.ids.original_list_grid.add_widget(move_button)

			task_order = task_index+1

			list_order_label = NtcReorderListLabel(text=str(task_order),width=self.reorder_list_label_widths[0])
			self.ids.original_list_grid.add_widget(list_order_label)

			for item_index, item in enumerate(task):
				if item != 'original_task_index':
					next_label = NtcReorderListLabel(text=task[item], width=self.reorder_list_label_widths[item_index+1])
					self.ids.original_list_grid.add_widget(next_label)


	def move_to_new_list(self,task_list_index, original_task_index, command):
		self.ids.original_list_grid.clear_widgets()
		self.ids.reordered_list_grid.clear_widgets()
		if command == 'move':
			self.reordered_list.append(json.loads(self.original_list[task_list_index]))
			self.original_list.pop(task_list_index)
			self.original_list.insert(task_list_index, json.dumps({'task_status':'Moved ...','2':'','3':'','4':'','5':'','6':'','7':'','8':'','original_task_index':''}))
		elif command == 'return':
			self.original_list.pop(int(original_task_index))
			self.original_list.insert(int(original_task_index), json.dumps(self.reordered_list[task_list_index]))
			self.reordered_list.pop(task_list_index)
		self.display_original_list()
		self.display_reordered_list()
		self.clear_feedback_label()


	def display_reordered_list(self):
		
		for task_index, data in enumerate(self.reordered_list):

			original_task_index = data['original_task_index']
			task = json.dumps(str(data))

			return_button = NtcMoveListItemButton(text='Return',background_color=delete_button_color)
			return_button.bind(on_press=lambda a,b=task_index,c=original_task_index,d='return':[self.move_to_new_list(b,c,d)])
			self.ids.reordered_list_grid.add_widget(return_button)

			task_order = task_index+1
			list_order_label = NtcReorderListLabel(text=str(task_order)+' ('+str(int(original_task_index)+1)+')',width=self.reorder_list_label_widths[0]*2)
			self.ids.reordered_list_grid.add_widget(list_order_label)

			for item_index, item in enumerate(data):

				if item != 'original_task_index':
					next_label = NtcReorderListLabel(text=data[item], width=self.reorder_list_label_widths[item_index+1])
					self.ids.reordered_list_grid.add_widget(next_label)


	def validate_list(self):
		error_msg = ''
		if len(self.original_list) != len(self.reordered_list):
			error_msg = '[b]Move all Tasks from Original List to Reordered List before saving.[/b]'

		if len(error_msg) > 0:
			self.ids.reorder_list_feedback_label.text=error_msg
			self.ids.reorder_list_feedback_label.color=error_text_color
			cleanup_result = 'list_is_dirty'
		else:
			self.remove_original_task_index()
			cleanup_result = 'list_is_clean'

		if cleanup_result == 'list_is_clean':
			self.save_list()


	def remove_original_task_index(self):

		for task_index, task in enumerate(self.reordered_list):
			del task['original_task_index']

			
	def save_list(self):
		with open(self.file_name_with_path, 'w') as the_file:    # empty the file before writing new list
			pass

		for index, task_from_list in enumerate(self.reordered_list):

			task_as_string = json.dumps(task_from_list)	# Python dict to Python string

			with open(self.file_name_with_path, 'a') as the_file:    # write new list to file
				the_file.write(task_as_string+'\n')
				# the_file.write('\n')

		self.start_over()

		info_msg = '[b]List has been reordered, saved and displayed above.[/b]'
		self.ids.reorder_list_feedback_label.text=info_msg
		self.ids.reorder_list_feedback_label.color=feedback_text_color
		self.ids.reorder_list_feedback_label.font_size=large_font_size

		Clock.schedule_once(lambda a:self.clear_feedback_label(), 4.5)

	def start_over(self):
		self.ids.original_list_grid.clear_widgets()
		self.ids.reordered_list_grid.clear_widgets()
		self.original_list = []
		self.reordered_list = []
		self.populate_original_lists()
		self.clear_feedback_label()


class NtcReorderListLabel(Label):
	pass

class NtcMoveListItemButton(Button):
	pass