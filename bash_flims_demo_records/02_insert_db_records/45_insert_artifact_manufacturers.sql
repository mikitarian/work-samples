/*	INSERT records into artifact_manufacturer table

	"id", "createDate" and "deleteDate" are generated by DB Sequence,
	and are not included in the SQL below
	
	Sam Mikitarian 21 Dec 2021
*/

/*
artifact_manufacturer table structure
 active | createDate | deleteDate | description | id | name | shortName | slug | year | yearSequence
--------+------------+------------+-------------+----+------+-----------+------+------+--------------
*/

DO $$
	DECLARE
	
		/* artifact_manufacturer_array contents shown for readability:
		 active |                         description                          |       name        | shortName 
		--------+--------------------------------------------------------------+-------------------+-----------
		   t        Manufacturer of data storage devices.                         Western Digital     WD
		*/
		
		artifact_manufacturer_array VARCHAR[] := ARRAY[
			 '"t";;"Manufacturer of data storage devices.";;"Western Digital";;"WD"',
			 '"t";;"Maker of data storage devices.";;"Seagate";;"SG"',
			 '"t";;"Maker of servers, computers, printers and other peripherals.";;"Hewlett-Packard";;"HP"',
			 '"t";;"Maker of servers, computers, printers and other peripherals.";;"Dell";;"Dell"',
			 '"t";;"Makers of processor chips.";;"Intel Corporation";;"Intel"',
			 '"t";;"Makers of processor chips.";;"AMD";;"AMD"'
			];
		
		artifact_manufacturer VARCHAR;
		
		active BOOLEAN := true;
		description VARCHAR := NULL;
		name VARCHAR;
		short_name VARCHAR;

		last_slug_seq INTEGER;
		next_slug_seq INTEGER;
		slug VARCHAR(128);
		
		the_year INTEGER;
		year_seq INTEGER;

BEGIN
	FOREACH artifact_manufacturer IN ARRAY artifact_manufacturer_array LOOP

		active := trim(both '"' from split_part(artifact_manufacturer,';;',1));
		
		description := trim(both '"' from split_part(artifact_manufacturer,';;',2));
		
		name := trim(both '"' from split_part(artifact_manufacturer,';;',3));
		
		short_name := trim(both '"' from split_part(artifact_manufacturer,';;',4));
		
		last_slug_seq = (SELECT MAX(CAST(SUBSTRING(artifact_manufacturer."slug",9) AS INTEGER)) FROM artifact_manufacturer);
		IF last_slug_seq IS NULL THEN
			next_slug_seq := 1;
		ELSE
			next_slug_seq := (SELECT MAX(CAST(SUBSTRING(artifact_manufacturer."slug",9) AS INTEGER))+1 FROM artifact_manufacturer);
		END IF;
		slug := CONCAT('AM-', DATE_PART('year', CURRENT_DATE), '-', next_slug_seq);
		
		the_year := DATE_PART('year', CURRENT_DATE);
		
		year_seq := (SELECT MAX("yearSequence")+1 FROM artifact_manufacturer);
		IF year_seq IS NULL THEN
			year_seq := 1;
		END IF;

		INSERT INTO public.artifact_manufacturer ("active", "description", "name", "shortName", "slug", "year", "yearSequence" )
			VALUES (active, description, name, short_name, slug, the_year, year_seq );
			
	END LOOP;
END;
$$