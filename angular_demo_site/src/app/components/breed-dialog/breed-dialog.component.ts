import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatFormField } from "@angular/material/form-field";

@Component({
  selector: 'app-breed-dialog',
  templateUrl: './breed-dialog.component.html',
  styleUrls: ['./breed-dialog.component.scss']
})
export class BreedDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<BreedDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

  close() {
    this.dialogRef.close();
    // this.dialogRef.close("Thanks for using me!");
  }

  ngOnInit(): void {
  }

}
