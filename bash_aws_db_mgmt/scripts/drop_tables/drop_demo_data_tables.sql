DROP TABLE $(dbname).dbo.AlertRoleXWalk;

DROP TABLE $(dbname).dbo.Alerts;

DROP TABLE $(dbname).dbo.AlertRoles;

DROP TABLE $(dbname).dbo.AlertType;

DROP TABLE $(dbname).dbo.AlertUserRead;

DROP TABLE $(dbname).dbo.CloudServiceOrders;

DROP TABLE $(dbname).dbo.CloudAccounts;

DROP TABLE $(dbname).dbo.CrmInvoice;

DROP TABLE $(dbname).dbo.FinancialPOCs;

DROP TABLE $(dbname).dbo.FundingAllocation;

DROP TABLE $(dbname).dbo.PurchaseRequests;

DROP TABLE $(dbname).dbo.Notifications;

DROP TABLE $(dbname).dbo.PurchaseAgreement;

DROP TABLE $(dbname).dbo.PurchaseBundles;

DROP TABLE $(dbname).dbo.PurchaseRequestForm;

DROP TABLE $(dbname).dbo.SystemOwners;

DROP TABLE $(dbname).dbo.TechnicalPOCs;