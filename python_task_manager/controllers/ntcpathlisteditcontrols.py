from app_defaults import *
from ntcmain import *

class NtcPathListEditControls(StackLayout):
	def script_path_directory_selection(self, directory, filename):
		if os.path.dirname(filename) != "\\":
			self.ids.path_value.text = os.path.dirname(filename)


	def reset_script_path_chooser_to_root(self):
		self.ids.path_value.text=''
		self.ids.script_path_chooser.path="\\"