/* INSERT records into case table

	"id" and "createDate" are generated by DB Sequences,
	and are not included in the SQL below
	
	Sam Mikitarian 28 Dec 2021
*/

/* case table structure
createDate | creatorId | description | directory | externalId | id | name | parentId | requestType | situationDescription | slug | status | type | typeOther | year | yearSequence
-----------+-----------+-------------+-----------+------------+----+------+----------+-------------+----------------------+------+--------+------+-----------+------+--------------
*/

DO $$
DECLARE

	/* forensic_cases_array contents shown for readability:
	createDate | creatorId |                                       description                                                        | directory | externalId |                   name                            |  requestType |                                                  situationDescription                                                                                           | status | type | typeOther 
	-----------+-----------+----------------------------------------------------------------------------------------------------------+-----------+------------+---------------------------------------------------+--------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+------+-----------
					1		Initial request for support.  US Cyber Command sending forensics team within 24 hours.  Details to follow                2022-001     Log4j exploit at Defense Acquisition University          0        A Chinese hacking group known for industrial espionage and intelligence collection used a vulnerability in Log4j to go after DAU faculty and student identities.    4       2
	*/

	forensic_cases_array VARCHAR[] := ARRAY[
			'"";;"1";;"Initial request for support.  US Cyber Command sending forensics team within 24 hours.  Details to follow.";;"";;"2022-001";;"Log4j exploit at Defense Acquisition University";;"";;"0";;"A Chinese hacking group known for industrial espionage and intelligence collection used a vulnerability in Log4j to go after DAU faculty and student identities.";;"4";;"2";;""',
			'"";;"1";;"Severe weather preventing forensics team from arriving for 48 hours.  BAMC has switched to manual procedures until investigation can begin";;"";;"2022-002";;"Brooke Army Medical Center HR ransomware attack";;"";;"0";;"Ransonmware attack on legacy HR systems at Brooke Army Medical Center targeting PII/PHI of thousands of veterans.";;"4";;"2";;""',
			'"";;"1";;"Malware was detected by local HBSS.  On-site team has isolated infected systems.";;"";;"2022-003";;"Blister Campaign targets Windows machines at Oak Ridge National Labs.";;"";;"1";;"Preparations for Annual Security Inspection discovered a stealthy malware campaign using valid code signing certificates in Windows systems to stay hidden. The malware loader, named Blister, further deploys second-stage payloads in memory.  https://cyware.com/news/new-blister-campaign-stealthily-targets-windows-c9297496/?web_view=true";;"4";;"6";;""'
		];

	forensic_case VARCHAR;
	
	create_date TIMESTAMP WITH TIME ZONE;
	creator_id INTEGER;
	description VARCHAR := NULL;
	directory VARCHAR := NULL;
	external_id VARCHAR := NULL;
	case_name VARCHAR;
	parent_id INTEGER := NULL;
	request_type case_requesttype_enum;
	situation_description VARCHAR := NULL;
	status case_status_enum;
	case_type case_type_enum;
	case_type_other VARCHAR := NULL;
	
	last_slug_id VARCHAR;
	next_slug_id INTEGER;
	slug VARCHAR;
	the_year INTEGER;
	year_seq INTEGER;
	
BEGIN

	FOREACH forensic_case IN ARRAY forensic_cases_array LOOP
	
		IF LENGTH(TRIM(BOTH '"' FROM split_part(forensic_case,';;',1))) > 0 THEN
			create_date := TRIM(BOTH '"' FROM split_part(forensic_case,';;',1));
		ELSE
			create_date := CURRENT_TIMESTAMP;
		END IF;
		
		creator_id := TRIM(BOTH '"' FROM split_part(forensic_case,';;',2));
		description := TRIM(BOTH '"' FROM split_part(forensic_case,';;',3));
		
		IF LENGTH(TRIM(BOTH '"' FROM split_part(forensic_case,';;',4))) > 0 THEN
			directory := TRIM(BOTH '"' FROM split_part(forensic_case,';;',4));
		END IF;

		IF LENGTH(TRIM(BOTH '"' FROM split_part(forensic_case,';;',5))) > 0 THEN
			external_id := TRIM(BOTH '"' FROM split_part(forensic_case,';;',5));
		END IF;
		
		case_name := TRIM(BOTH '"' FROM split_part(forensic_case,';;',6));

		IF LENGTH(TRIM(BOTH '"' FROM split_part(forensic_case,';;',7))) > 0 THEN
			parent_id := TRIM(BOTH '"' FROM split_part(forensic_case,';;',7));
		END IF;

		request_type := TRIM(BOTH '"' FROM split_part(forensic_case,';;',8));
		situation_description := TRIM(BOTH '"' FROM split_part(forensic_case,';;',9));
		status := TRIM(BOTH '"' FROM split_part(forensic_case,';;',10));
		
		case_type := TRIM(BOTH '"' FROM split_part(forensic_case,';;',11));
		case_type_other := TRIM(BOTH '"' FROM split_part(forensic_case,';;',12));
		
		last_slug_id = (SELECT MAX(CAST(SUBSTRING(public.case."slug",8) AS INTEGER)) FROM public.case);
		IF last_slug_id IS NULL THEN
			next_slug_id := 1;
		ELSE
			next_slug_id := (SELECT MAX(CAST(SUBSTRING(public.case."slug",8) AS INTEGER))+1 FROM public.case);
		END IF;
		slug := CONCAT('C-', date_part('year', CURRENT_DATE), '-', next_slug_id);

		the_year := date_part('year', CURRENT_DATE);

		year_seq := (SELECT MAX("yearSequence")+1 FROM public.case);
		IF year_seq IS NULL THEN
			year_seq := 1;
		END IF;

		INSERT INTO public.case ("createDate", "creatorId", description, directory, "externalId", "name", "parentId", "requestType", "situationDescription", slug, status, "type", "typeOther", "year", "yearSequence") 
			VALUES (create_date, creator_id, description, directory, external_id, case_name, parent_id, request_type, situation_description, slug, status, case_type, case_type_other, the_year, year_seq);
		
	END LOOP;
	
END;
$$