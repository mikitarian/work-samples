# tfsec:ignore:aws-elb-alb-not-public
resource "aws_lb" "this" {
  count = var.create_lb ? 1 : 0

  name        = var.name
  name_prefix = var.name_prefix

  load_balancer_type = var.load_balancer_type
  internal           = var.internal
  security_groups    = var.security_groups
  subnets            = var.subnets

  idle_timeout                     = var.idle_timeout
  enable_cross_zone_load_balancing = var.enable_cross_zone_load_balancing
  enable_deletion_protection       = var.enable_deletion_protection
  enable_http2                     = var.enable_http2
  ip_address_type                  = var.ip_address_type
  drop_invalid_header_fields       = var.drop_invalid_header_fields

  dynamic "access_logs" {
    for_each = length(keys(var.access_logs)) == 0 ? [] : [var.access_logs]

    content {
      enabled = lookup(access_logs.value, "enabled", lookup(access_logs.value, "bucket", null) != null)
      bucket  = lookup(access_logs.value, "bucket", null)
      prefix  = lookup(access_logs.value, "prefix", null)
    }
  }

  dynamic "subnet_mapping" {
    for_each = var.subnet_mapping

    content {
      subnet_id     = subnet_mapping.value.subnet_id
      allocation_id = lookup(subnet_mapping.value, "allocation_id", null)
    }
  }

  tags = merge(
    var.tags,
    {
      Name = var.name != null ? var.name : var.name_prefix
    },
  )

  timeouts {
    create = var.load_balancer_create_timeout
    update = var.load_balancer_update_timeout
    delete = var.load_balancer_delete_timeout
  }
}

resource "aws_lb_target_group" "main" {
  for_each = var.create_lb ? var.target_groups : {}

  name        = lookup(each.value, "name", null)
  name_prefix = lookup(each.value, "name_prefix", null)

  vpc_id      = var.vpc_id
  port        = lookup(each.value, "backend_port", null)
  protocol    = lookup(each.value, "backend_protocol", null) != null ? upper(lookup(each.value, "backend_protocol")) : null
  target_type = lookup(each.value, "target_type", null)

  deregistration_delay               = lookup(each.value, "deregistration_delay", null)
  slow_start                         = lookup(each.value, "slow_start", null)
  proxy_protocol_v2                  = lookup(each.value, "proxy_protocol_v2", null)
  lambda_multi_value_headers_enabled = lookup(each.value, "lambda_multi_value_headers_enabled", null)
  preserve_client_ip                 = lookup(each.value, "preserve_client_ip", null)

  dynamic "health_check" {
    for_each = length(keys(lookup(each.value, "health_check", {}))) == 0 ? [] : [lookup(each.value, "health_check", {})]

    content {
      enabled             = lookup(health_check.value, "enabled", null)
      interval            = lookup(health_check.value, "interval", null)
      path                = lookup(health_check.value, "path", null)
      port                = lookup(health_check.value, "port", null)
      healthy_threshold   = lookup(health_check.value, "healthy_threshold", null)
      unhealthy_threshold = lookup(health_check.value, "unhealthy_threshold", null)
      timeout             = lookup(health_check.value, "timeout", null)
      protocol            = lookup(health_check.value, "protocol", null)
      matcher             = lookup(health_check.value, "matcher", null)
    }
  }

  dynamic "stickiness" {
    for_each = length(keys(lookup(each.value, "stickiness", {}))) == 0 ? [] : [lookup(each.value, "stickiness", {})]

    content {
      enabled         = lookup(stickiness.value, "enabled", null)
      cookie_duration = lookup(stickiness.value, "cookie_duration", null)
      type            = lookup(stickiness.value, "type", null)
    }
  }

  tags = merge(
    var.tags,
    {
      "Name" = lookup(each.value, "name", null) != null ? lookup(each.value, "name", null) : lookup(each.value, "name_prefix", "not named")
    },
  )

  depends_on = [aws_lb.this]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_listener" "frontend_http_tcp" {
  for_each = var.create_lb ? var.http_tcp_listeners : {}

  load_balancer_arn = aws_lb.this[0].arn

  port     = each.value["port"]
  protocol = each.value["protocol"]

  default_action {
    target_group_arn = aws_lb_target_group.main[each.value.target_group_index].id
    type             = "forward"
  }
}

resource "aws_lb_listener" "frontend_https" {
  for_each = var.create_lb ? var.https_listeners : {}

  load_balancer_arn = aws_lb.this[0].arn

  port            = each.value["port"]
  protocol        = lookup(each.value, "protocol", "HTTPS")
  certificate_arn = each.value["certificate_arn"]
  ssl_policy      = lookup(each.value, "ssl_policy", var.listener_ssl_policy_default)

  default_action {
    target_group_arn = aws_lb_target_group.main[each.value.target_group_index].id
    type             = "forward"
  }
}

resource "aws_lb_listener_certificate" "https_listener" {
  count = var.create_lb ? length(var.extra_ssl_certs) : 0

  listener_arn    = aws_lb_listener.frontend_https[var.extra_ssl_certs[count.index]["https_listener_index"]].arn
  certificate_arn = var.extra_ssl_certs[count.index]["certificate_arn"]
}

resource "aws_lb_listener" "redirect_http_to_https" {
  count = var.create_lb ? length(var.redirect_http_to_https) : 0

  load_balancer_arn = aws_lb.this[0].arn
  port              = var.redirect_http_to_https[count.index]["port"]
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener_rule" "https_listener_rule" {
  count = var.create_lb ? length(var.https_listener_rules) : 0

  listener_arn = aws_lb_listener.frontend_https[lookup(var.https_listener_rules[count.index], "https_listener_index", count.index)].arn
  priority     = lookup(var.https_listener_rules[count.index], "priority", null)

  # forward actions
  dynamic "action" {
    for_each = [
      for action_rule in var.https_listener_rules[count.index].actions :
      action_rule
      if action_rule.type == "forward"
    ]

    content {
      type             = action.value["type"]
      target_group_arn = aws_lb_target_group.main[lookup(action.value, "target_group_index", count.index)].id
    }
  }

  # Host header condition
  dynamic "condition" {
    for_each = [
      for condition_rule in var.https_listener_rules[count.index].conditions :
      condition_rule
      if length(lookup(condition_rule, "host_headers", [])) > 0
    ]

    content {
      host_header {
        values = condition.value["host_headers"]
      }
    }
  }
}
