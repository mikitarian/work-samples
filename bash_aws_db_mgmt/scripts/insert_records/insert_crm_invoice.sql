SET NOCOUNT ON;

INSERT INTO $(dbname).dbo.CrmInvoice (InvoiceDate,ApprovalStatus,Notes,DateInvoiceInputtedIntoWawfBySossec,DateInvoiceApprovedByCamoCorInWawf,DateInvoiceApprovalSubmittedToSossec,DateInvoiceSubmittedToSossec,DateInvoiceApproved,DateInvoiceSentToGov,InvoiceStatus,IsInvoiceSentToGovt,IsInvoiceApproved,IsInvoiceSubmittedToSossec,IsInvoiceApprovalSubmittedToSossec,IsInvoiceInputtedIntoWawfBySossec,IsInvoiceApprovedByCamoCorInWawf,IsPaymentReceived) VALUES
	 ('2023-02-01 00:00:00.0000000',1,N'','2023-03-28 00:00:00.0000000',NULL,'2023-03-20 00:00:00.0000000','2023-03-20 00:00:00.0000000','2023-03-13 00:00:00.0000000','2023-03-08 00:00:00.0000000',1,0,0,0,0,0,0,0),
	 ('2023-02-01 00:00:00.0000000',3,N'',NULL,NULL,'2023-05-26 00:00:00.0000000','2023-05-18 00:00:00.0000000','2023-04-06 00:00:00.0000000','2023-03-16 00:00:00.0000000',1,0,0,0,0,0,0,0),
	 ('2023-02-01 00:00:00.0000000',1,N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0),
	 ('2023-02-01 00:00:00.0000000',1,N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0),
	 ('2023-03-01 00:00:00.0000000',1,N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0);

SET NOCOUNT OFF;