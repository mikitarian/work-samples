"""
The Extract-Transform-Load process will require both the origin (1x)
and the source (develop).  This will require creating the query to pull
the 1x data and then any transformations on the 2x data

We'll provide two sets of queries
1. The extract query (which will always be a SELECT * FROM)
2. The insert (which may be slightly different)

Also, because there's a large swath of data, we'll use a
INSERT  so we don't blow up on one bad record.  We'll manually resolve those later.
"""

purchase_agreement_extract = {
    "tableName": "PurchaseAgreement",
    "query": "SELECT * FROM PurchaseAgreement"
}

purchase_agreement_insert = ('''
    INSERT INTO PurchaseAgreement VALUES(?,?,?,?,?,?,?,?,?,?,?)
''')

purchaseRequests_extract = {
    "tableName": "purchaseRequests",
    "query": "SELECT * FROM purchaseRequests"
}

purchaseRequests_insert = ('''
 INSERT INTO PurchaseRequests VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
''')

purchase_form_extract = {
    "tableName": "purchaseForms",
    "query": "SELECT * FROM purchaseForms"
}

purchase_form_insert = ('''
 INSERT INTO PurchaseRequestForm VALUES(?,?,?,?,?,?,?)
''')

purchase_form_extract = {
    "tableName": "purchaseForms",
    "query": "SELECT * FROM purchaseForms"
}

camo_rom_form_extract = {
    "tableName": "camoRomForms",
    "query": "SELECT * FROM camoRomForms"
}

purchase_form_insert = ('''
 INSERT INTO PurchaseRequestForm VALUES(?,?,?,?,?,?,?)
''')

purchase_bundle_extract = {
    "tableName": "purchaseBundles",
    "query": "SELECT * FROM purchaseBundles"
}

purchase_bundle_insert = ('''
    INSERT INTO PurchaseBundles VALUES(?,?,?,?,?,?,?,?,?,?)
''')

funding_allocation_extract = {
    "tableName": "FundingAllocation",
    "query": "SELECT * FROM FundingAllocation"
}

funding_allocation_insert = ('''
    INSERT INTO FundingAllocation (FiscalYear, Amount, ColorOfMoney, PrNumber)
    VALUES(?,?,?,?)
''')


class SqlQuery:

    def __init__(self, extract_query, load_query):
        """
        The constructor
        :param extract_query:  The query we want to use to pull data
        :param load_query:  The query we use to load the data
        """
        self.extract_query = extract_query
        self.load_query = load_query


# create instances for SqlQuery class
purchase_agreement_query = SqlQuery(purchase_agreement_extract, purchase_agreement_insert)
purchase_request_query = SqlQuery(purchaseRequests_extract, purchaseRequests_insert)
purchase_form_query = SqlQuery(purchase_form_extract, purchase_form_insert)
camo_form_query = SqlQuery(camo_rom_form_extract, purchase_form_insert)
purchase_bundle_query = SqlQuery(purchase_bundle_extract, purchase_bundle_insert)
funding_allocation_query = SqlQuery(funding_allocation_extract, funding_allocation_insert)

# store as list for iteration
sqlserver_queries = [purchase_agreement_query, purchase_request_query, purchase_form_query, camo_form_query, purchase_bundle_query,
					 funding_allocation_query]
