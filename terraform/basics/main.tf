locals {
  app_name = "cm"
  tags = {
    app_name      = local.app_name
    managed       = "Terraform"
    configuration = "terraform/basics"
  }
}

data "aws_partition" "current" {}
data "aws_region" "current" {}
data "aws_caller_identity" "current" {}


#######
# VPC #
#######

# tfsec:ignore:aws-ec2-no-public-ip-subnet
module "vpc" {
  source = "../community-modules/terraform-aws-vpc"

  name = local.app_name
  cidr = "10.0.0.0/16"

  azs             = ["us-gov-east-1a", "us-gov-east-1b", "us-gov-east-1c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  tags = local.tags
}

module "vpc_endpoints" {
  source = "../community-modules/terraform-aws-vpc/modules/vpc-endpoints"

  vpc_id = module.vpc.vpc_id

  endpoints = {
    s3 = {
      service         = "s3"
      route_table_ids = module.vpc.private_route_table_ids
      service_type    = "Gateway"
      tags            = local.tags
    }
  }
}

#######
# IAM #
#######
locals {
  iam_policy_ecs_execution = templatefile("${path.root}/templates/iam_policy_ecs_execution.json", {
    partition   = data.aws_partition.current.partition
    region      = data.aws_region.current.name
    account_id  = data.aws_caller_identity.current.account_id
    name_prefix = local.app_name
  })

  trust_policy_ecs_execution = templatefile("${path.root}/templates/trust_policy_ecs.json", {})
}

# TODO: add separate access for prod to deny non-prod access to prod secrets

module "ecs_execution_role" {
  source = "../modules/aws-iam-role"

  name                = "${local.app_name}_ecs_execution"
  description         = "CloudManager ECS execution"
  assume_role_policy  = local.trust_policy_ecs_execution
  iam_policy_document = local.iam_policy_ecs_execution
  tags                = local.tags
}
