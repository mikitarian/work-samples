from app_defaults import *
from ntcmain import *
from controllers.datatools import SpinnerListInfo
from controllers.ntcpathlisteditcontrols import *
from controllers.ntcipaddresslisteditcontrols import *

Builder.load_file('./views/list_editor/ntclisteditormain.kv')
Builder.load_file('./views/list_editor/ntcpathlisteditor.kv')
Builder.load_file('./views/list_editor/ntcipaddresslisteditor.kv')
Builder.load_file('./views/list_editor/ntclistgrid.kv')

class NtcListEditor(Popup):
	def __init__(self, target_list, **kwargs):
		super().__init__(**kwargs)

		self.target_list = target_list
		self.edit_list_item_flag = False
		self.current_list_item_index = ''

		#--------- Set Edit List controls based on the list the user called ---------#
		if str(self.target_list) == str(script_path_file_name): #'data_model\\scriptpaths':
			self.EditListControls = NtcPathListEditControls()
			self.ids.edit_controls_row.add_widget(self.EditListControls)
			self.EditListControls.ids.save_script_path_button.bind(on_release=lambda a:[self.validate_save_list_item()])
		elif str(self.target_list) == str(ip_address_file_name):
			self.EditListControls = NtcIpAddressListEditControls()
			self.ids.edit_controls_row.add_widget(self.EditListControls)
			self.EditListControls.ids.save_ip_address.bind(on_release=lambda a:[self.validate_save_list_item()])

		self.list_contents = SpinnerListInfo.get_list_contents(self, self.target_list, False)

		self.display_list_items()


	def display_list_items(self):
		for list_item_index, list_item in enumerate(self.list_contents):

			seq_num = list_item_index + 1
			list_item_label = NtcListItemLabel(text=list_item)
			self.ids.list_grid.add_widget(list_item_label)

			edit_button = NtcListItemEditButton()
			edit_button.bind(on_release=lambda a,b=list_item_index:[self.load_list_item(b)])

			delete_button = NtcListItemDeleteButton()
			delete_button.bind(on_release=lambda a,b=list_item_index:[self.confirm_delete_list_item(b)])

			self.ids.list_grid.add_widget(edit_button)
			self.ids.list_grid.add_widget(delete_button)

	def reload_list(self):
		self.list_contents = SpinnerListInfo.get_list_contents(self, self.target_list, False)

	def clear_list_items(self):
		self.ids.list_grid.clear_widgets()


	def load_list_item(self,list_item_index):
		self.set_edit_list_item_flag(True)
		self.current_list_item_index = list_item_index
		if str(self.target_list) == str(script_path_file_name):
			self.EditListControls.ids.path_value.text=str(self.list_contents[list_item_index])
			self.EditListControls.ids.script_path_chooser.path=str(self.list_contents[list_item_index])
			self.EditListControls.ids.save_script_path_button.text='Save Edits'

		elif str(self.target_list) == str(ip_address_file_name):
			self.EditListControls.load_address_to_edit(self.list_contents[list_item_index])

	def clear_current_list_item(self):
		print(str(self.target_list))
		print(str(script_path_file_name))
		if str(self.target_list) == str(script_path_file_name):
			self.EditListControls.ids.path_value.text=''
		elif str(self.target_list) == str(ip_address_file_name):
			self.EditListControls.ids.ip_address_one.text=''
			self.EditListControls.ids.ip_address_two.text=''


	def set_edit_list_item_flag(self,flag_value):
		if flag_value == True:
			self.edit_list_item_flag = True
			if str(self.target_list) == str(script_path_file_name):
				self.remove_changes_button()
				self.cancel_changes_button = NtcCancelPathChangesButton()
				self.cancel_changes_button.bind(on_release=lambda a:[self.cancel_list_item_changes(), self.EditListControls.reset_script_path_chooser_to_root()])
				self.EditListControls.ids.path_value_box.add_widget(self.cancel_changes_button)
			elif str(self.target_list) == str(ip_address_file_name):
				pass
		else:
			self.edit_list_item_flag = False
			

	def cancel_list_item_changes(self):
		self.set_edit_list_item_flag(False)
		self.remove_changes_button()
		self.clear_current_list_item()
		self.EditListControls.ids.save_script_path_button.text='Add Path to List'


	def remove_changes_button(self):
		try:
			self.cancel_changes_button
		except AttributeError:
			pass
		else:
			self.EditListControls.ids.path_value_box.remove_widget(self.cancel_changes_button)


	def validate_save_list_item(self):
		if str(self.target_list) == str(script_path_file_name):

			if len(self.EditListControls.ids.path_value.text) < 1:
				NtcMain().ntc_ok_cancel_box('ok_only', 'large', 0, 'select_script_path','')
			else:
				self.save_list_item()

		elif str(self.target_list) == str(ip_address_file_name):
			my_return = self.EditListControls.validate_ip_addresses()

			if my_return == 'invalid_list_item':
				pass
			elif my_return == 'valid_list_item':
				self.save_list_item()
			

	def save_list_item(self):
		if str(self.target_list) == str(script_path_file_name):
			list_item = str(self.EditListControls.ids.path_value.text)

		elif str(self.target_list) == str(ip_address_file_name):
			if len(self.EditListControls.ids.ip_address_one.text) > 0 and len(self.EditListControls.ids.ip_address_two.text) > 0:
				prelim_list_item = self.EditListControls.ids.ip_address_one.text+'-'+self.EditListControls.ids.ip_address_two.text
				list_item = str(prelim_list_item)
			else:
				list_item = self.EditListControls.ids.ip_address_one.text

		if self.edit_list_item_flag == True:
			self.list_contents.pop(self.current_list_item_index)
			self.list_contents.insert(self.current_list_item_index, list_item)

			with open(self.target_list, 'w') as the_file:    # empty the file before writing new list
				pass

			for index, item_from_list in enumerate(self.list_contents):
				with open(self.target_list, 'a') as the_file:    # write new list to file
					the_file.write(item_from_list.strip('\n'))
					the_file.write('\n')

		else:
			with open(self.target_list, 'a') as the_file:
				the_file.write(list_item + '\n')

		self.current_list_item_index = ''
		self.clear_list_items()
		self.clear_current_list_item()
		self.reload_list()
		self.display_list_items()
		# if str(self.target_list) == str(ip_address_file_name):
		# 	NtcMain().get_ip_address_list()


	def confirm_delete_list_item(self, list_item_index):

		dialog_data = [self.target_list, list_item_index, self.list_contents[list_item_index]]
		NtcMain().ntc_ok_cancel_box('ok_cancel', 'large', 0, 'delete_list_item_prompt',dialog_data)

	def delete_list_item(self, list_item_to_delete):

		self.list_contents.pop(list_item_to_delete)
        
		with open(self.target_list, 'w') as the_file:    # empty the file before writing new list
			pass

		with open(self.target_list, 'a') as the_file:    # write new list to file
			for task_index, data in enumerate(self.list_contents):
				the_file.write(data)
				the_file.write('\n')

		self.clear_list_items()
		self.reload_list()
		self.display_list_items()


class FileChooserListEditor(FileChooserListView):
	pass

class NtcListItemLabel(Label):
	pass

class NtcListItemEditButton(Button):
	pass

class NtcListItemDeleteButton(Button):
	pass

class NtcCancelPathChangesButton(Button):
	pass