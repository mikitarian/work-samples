variable "environment" {
  description = "The environment name, used to prefix and distinguish resources"
  type        = string
}

variable "fqdn" {
  description = "The FQDN of the application (do not include https://)"
  type        = string
}

variable "ui_fqdn" {
  description = "The FQDN of the S3 UI bucket (do not include http://"
  type        = string
}

variable "ecs_execution_role" {
  description = "The IAM role to use for ECS execution"
  type        = string
}

variable "ecs_task_role" {
  description = "The IAM role used by the ECS task"
  type        = string
}

variable "vpc_id" {
  description = "The ID of the VPC"
  type        = string
}

variable "vpc_cidr_block" {
  description = "The CIDR of the VPC"
  type        = string
}

variable "private_subnets" {
  description = "List of private subnets to use"
  type        = list(string)
}

variable "public_subnets" {
  description = "List of private subnets to use"
  type        = list(string)
}

variable "container_repository_base" {
  description = "The base path for the container repository"
  type        = string
  default     = "281978155256.dkr.ecr.us-gov-east-1.amazonaws.com"
}

variable "image_tag" {
  description = "The container image tag to use when pulling images from repository"
  type        = string
}

variable "log_retention_days" {
  description = "Number of days to retain logs"
  type        = number
  default     = 60
}

variable "web_ingress_prefix_list" {
  description = "List of prefix lists to add to ingress for api-gateway"
  type        = list(string)
  default     = []
}

variable "web_ingress_cidr_list" {
  description = "List of CIDR blocks to add to ingress for api-gateway"
  type        = list(string)
  default     = []
}

// RDS
variable "db_engine" {
  description = "The plain text name of database engine to use"
  type        = string
  default     = "sqlserver-web"
}

variable "db_engine_version" {
  description = "The database engine to use"
  type        = string
  default     = "15.00.4236.7.v1"
}

variable "db_family" {
  description = "The database family to use"
  type        = string
  default     = "sqlserver-web-15.0"
}

variable "db_major_engine_version" { # used by the option group in AWS
  description = "The database engine to use"
  type        = string
  default     = "15.00"
}

variable "db_instance_class" {
  description = "The instance class to use for the database"
  type        = string
  default     = "db.t3.medium"
}

variable "db_allocated_storage" {
  description = "Allocated size for RDS instance"
  type        = number
  default     = 20
}

variable "db_max_allocated_storage" {
  description = "Max allocated size for RDS instance"
  type        = number
  default     = 1000
}
