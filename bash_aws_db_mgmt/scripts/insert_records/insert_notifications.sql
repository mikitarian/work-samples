SET NOCOUNT ON;

INSERT INTO $(dbname).dbo.Notifications (StartDate,EndDate,Severity,Description,Link) VALUES
	 ('2023-03-14 00:00:00.0000000','2023-03-17 00:00:00.0000000',3,N'Welcome to Cloud Manager 3.0',NULL);

SET NOCOUNT OFF;