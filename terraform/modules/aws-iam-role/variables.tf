variable "name" {
  description = "Name of the IAM role"
  type        = string
}

variable "assume_role_policy" {
  description = "Assume role policy document for the IAM role"
  type        = string
}

variable "description" {
  description = "Description of the IAM role"
  type        = string
  default     = null
}

variable "iam_policy_document" {
  description = "IAM policy document json"
  type        = string
  default     = null
}

variable "force_detach_policies" {
  description = "Boolean to control whether to force detach any policies the role has before destroying it"
  type        = bool
  default     = null
}

variable "max_session_duration" {
  description = "Maximum session duration (in seconds) to set for the role. The default is one hour. This setting can have a value from 1 hour to 12 hours"
  type        = number
  default     = null
}

variable "path" {
  description = "Path for the role"
  type        = string
  default     = "/"
}

variable "permissions_boundary" {
  description = "ARN of the managed policy to set as the permissions boundary for the role"
  type        = string
  default     = null
}

variable "tags" {
  description = "A map of tags to assign to the resource."
  type        = map(string)
  default     = {}
}
