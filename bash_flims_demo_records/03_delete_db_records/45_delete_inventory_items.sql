/*	DELETE records from inventory_item TABLE
	
	Sam Mikitarian 04 Jan 2022
*/

DO $$
DECLARE

BEGIN

	DELETE FROM inventory_item;
	
END;
$$