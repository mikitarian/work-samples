### Machine Agent Install

The Machine Agent is installed on each CIRRAS server.
Once installed and configured, it will be visible in the AppDynamics Controller -> Servers tabl.

**Download the Machine Agent:**
Download the Machine Agent from https://accounts.appdynamics.com/
example file name:  `appdynamics-machine-agent-24.1.0.3984.x86_64.rpm`

**Prepare the target server:**
Log in to the target server in a terminal window

* Create installation directory:  
  cd to /opt  
  mkdir /opt/appdynamics  
  mkdir /opt/appdynamics/machine-agent  
* Change ownership to jboss-eap user:  
  chown -R jboss-eap: /opt/appdynamics  
* Copy the machine agent to /opt/appdynamics on the target machine (scp, WinSCP, etc)  
  
**Install the machine agent file**  
  `cd /opt/appdynamics`  
    
  Run the installation command as `root` user.  
    
  To install if FIPS **IS NOT** enabled on the server:
  `rpm -ivh appdynamics-machine-agent-24.1.0.3984.x86_64.rpm`  
    
  To install if FIPS **IS** enabled on the server:  
  `rpm -ivh --nodigest --nofiledigest appdynamics-machine-agent-24.1.0.3984.x86_64.rpm`  
  -or-  
  `rpm -U --nodigest --nofiledigest appdynamics-machine-agent-24.1.0.3984.x86_64.rpm`  
  
**NOTE 1:**  Installation will create a user named "appdynamics-machine-agent" which will run the machine agent service.  
**NOTE 2:**  Installation will create the /etc/systemd/system/appdynamics-machine-agent.service file.  

  Output should be something like this:  
  ```
  Installing machine agent service...
Created group appdynamics-machine-agent for AppDynamics machine agent service
Created user appdynamics-machine-agent for AppDynamics machine agent service
Updating file permissions for user appdynamics-machine-agent and group appdynamics-machine-agent
Synchronizing state of appdynamics-machine-agent.service with SysV service script with /usr/lib/systemd/systemd-sysv-install.
Executing: /usr/lib/systemd/systemd-sysv-install enable appdynamics-machine-agent
Created symlink /etc/systemd/system/multi-user.target.wants/appdynamics-machine-agent.service → /etc/systemd/system/appdynamics-machine-agent.service.
------------------------------------------------------------------------
Installed the appdynamics-machine-agent as a service. You'll need to
configure the controller information by editing:

		/etc/appdynamics/machine-agent/controller-info.xml

You can start the machine agent service by:

		systemctl start appdynamics-machine-agent

To configure the user under which the machine agent service runs, edit:

		/etc/sysconfig/appdynamics-machine-agent
------------------------------------------------------------------------
```  
   
Contents of the /etc/systemd/system/appdynamics-machine-agent.service file:  
```
[Unit]
Description=AppDynamics Machine Agent

[Service]

# The AppDynamics machine agent startup script does not fork a process, so
# this is a simple service.
# Note: If you are changing the User running the machine agent, you must also ensure
# that the desired user has read access to controller-info.xml as well as write access
# to the log file. You can change specific file permissions or, most simply, do a
# chown command to give the desired user ownership of the MACHINE_AGENT_HOME directory.

Type=simple

Environment=MACHINE_AGENT_HOME=/opt/appdynamics/machine-agent
Environment=JAVA_HOME=/opt/appdynamics/machine-agent/jre

# Specify agent system properties for systemd here by setting or editing JAVA_OPTS, e.g.,
#Environment="JAVA_OPTS=-D<sys-property1>=<value1> -D<sys-property2>=<value2>"

# Modify the next two lines to specify the user to run the machine agent as. Note that
# you will need to ensure that:
# 1. The controller-info.xml in the agent conf directory is readable by this user
# 2. The logs directory is writeable by this user
# 3. The scripts directory is writeable by this user
User=appdynamics-machine-agent
Environment=MACHINE_AGENT_USER=appdynamics-machine-agent

# The next three lines must point to the same location (i.e. the
# PIDFILE env var and the PIDFile property.)
Environment=PIDDIR=/var/run/appdynamics
Environment=PIDFILE=${PIDDIR}/appdynamics-machine-agent.pid
PIDFile=/var/run/appdynamics/appdynamics-machine-agent.pid

# Killing the service using systemd causes Java to exit with status 143. This is OK.
SuccessExitStatus=143

# Run ExecStartPre with root-permissions
PermissionsStartOnly=true

# Create the pid dir
ExecStartPre=/usr/bin/install -o $MACHINE_AGENT_USER -d $PIDDIR

# This specifies the command line to use
ExecStart=/bin/sh -c "\"${MACHINE_AGENT_HOME}/bin/machine-agent\" -d -p ${PIDFILE}"

[Install]
# Start the AppDynamics machine agent service during the setup for a
# non-graphical multi-user system.
WantedBy=multi-user.target
```  
  
**Configure the machine agent**  
  
Configuration is done by editing the /opt/appdynamics/machine-agent/conf/controller-info.xml file (example at bottom of page)   
You will provide the following information so the machine agent can communicate with the AppDynamics Controller:  
* controller-host = \<ip address or DNS name of controller\>  
* controller-port = \<controller port number\>  
* controller-ssl-enabled = false (that will change once we confiugre SSL for AppDynamics)  
* unique-host-id = (a unique name for each server, such as test-app-machine-agent, or uat-proxy-machine-agent)  
* account-access-key = (on the Controller Web Interface, click the Gear Icon -> License, then click Account Access Key -> Show)  
* account-name = customer1 (or whatever value is supplied by the vendor)  
* sim-enabled = true  

When changes are complete, restart the machine agent:
`systemctl restart appdynamics-machine-agent`  
  
<br/>  

### Useful Commands  

**Stop/Start the machine agent:**  
`systemctl stop appdynamics-machine-agent`  
`systemctl start appdynamics-machine-agent`  
  
  <br/>  

### controller-info.xml example  
  
```
<?xml version="1.0" encoding="UTF-8"?>
<controller-info>
    <!-- For more detailed information on different configurations that an agent can support and the respective rules,
    consult the online documentation. In general, configuration properties may be set in different ways. Here is the
    order of precedence of how the agent will read and respect specific configurations.
      1. Environment Variables
      2. System Properties
      3. Default/Root controller-info.xml. See .../$(Agent-Base-Install-Dir)/conf/controller-info.xml -->

    <!-- This is the host name or the IP address of the AppDynamics Controller (e.g., 192.168.1.22 or myhost or
    myhost.abc.com). This is the same host that you use to access the AppDynamics browser-based User interface. This can
    be overridden with the environment variable 'CONTROLLER_HOST_NAME' or the system property
    '-Dappdynamics.controller.hostName' -->
    <controller-host>10.205.199.41</controller-host>

    <!-- This is the http(s) port of the AppDynamics Controller. If 'controller-ssl-enabled' (below) is set to true, you
    must specify the HTTPS port of the Controller; otherwise, specify the HTTP port. The default values are 8090 for
    HTTP and 8181 for HTTPS. This is the same port that you use to access the AppDynamics browser-based User interface.
    This can be overridden with the environment variable 'APPDYNAMICS_CONTROLLER_PORT' or the system property
    '-Dappdynamics.controller.port' -->
    <controller-port>8090</controller-port>

    <!-- This specifies if the AppDynamics agent should use SSL (HTTPS) to connect to the Controller. If you set this to
    true, the controller port property above should be set to the HTTPS port of the Controller. This can be overridden
    with the environment variable 'APPDYNAMICS_CONTROLLER_SSL_ENABLED' or the system property
    '-Dappdynamics.controller.ssl.enabled' -->
    <controller-ssl-enabled>false</controller-ssl-enabled>

    <!-- Set this flag to 'true' to enable features required for AppDynamics Orchestration, including the following:
    1) Enables the agent to execute tasks specified in AppDynamics Orchestration workflows.
    2) Enables the agent to resolve the AppDynamics Controller host and port when the host machine where this agent
       resides is created through AppDynamics Orchestration. -->
    <enable-orchestration>false</enable-orchestration>

    <!-- The Machine Agent uses the Java API to get the host name of the agent. The results from the API can be
    inconsistent, and the same JVM can sometimes return a different value for the same machine each time the machine
    agent is restarted. It is recommended to set this field in the following scenarios:

    1) The machine host name is not constant
    2) You prefer to use a specific name in the UI
    3) The machine has both a machine agent and app agents on it.

    This can be overridden with the environment variable 'APPDYNAMICS_AGENT_UNIQUE_HOST_ID' or the system property
    '-Dappdynamics.agent.uniqueHostId' -->
    <unique-host-id>test-app-machine-agent</unique-host-id>

    <!-- This key is generated at installation time and can be found by viewing the license information in the
    controller settings. This can be overridden with the environment variable 'APPDYNAMICS_AGENT_ACCOUNT_ACCESS_KEY' or
    the system property '-Dappdynamics.agent.accountAccessKey' -->
    <account-access-key>22fe7355-7ff4-4967-b06c-65dcbe006e40</account-access-key>

    <!-- If the AppDynamics Controller is running in multi-tenant mode or you are using the AppDynamics SaaS Controller,
    you must specify the account name for this agent to authenticate with the controller. If you are running in
    single-tenant mode (the default) there is no need to configure this value. This can be overridden with the
    environment variable 'APPDYNAMICS_AGENT_ACCOUNT_NAME' or the system property '-Dappdynamics.agent.accountName' -->
    <account-name>customer1</account-name>

    <!-- If this agent is licensed for Server Monitoring, set this flag to 'true' to enable Server Monitoring expanded
    metrics. This can be overridden with the environment variable 'APPDYNAMICS_SIM_ENABLED' or the system property
    '-Dappdynamics.sim.enabled' -->
    <sim-enabled>true</sim-enabled>

    <!-- If this machine is sap machine , set this flag to 'true' to enable is sap flag for ibl licensing
        This can be overridden with the environment variable 'APPDYNAMICS_IS_SAP_MACHINE' or the system property
       '-Dappdynamics.is.sap.machine' -->

    <is-sap-machine></is-sap-machine>

    <agent-runtime-dir></agent-runtime-dir>

    <!-- This value is used to create the hierarchy infrastructure view in the UI for this machine. Each hierarchy level
    should be separated with a vertical bar ("|"). For example, if this machine belongs to "DataRack1" and it is located
    in "Virginia Data Center", then the machine path could be set to "Virginia Data Center|DataRack1|Machine1" and the
    UI will display it in that hierarchy ("Virginia Data Center|DataRack1"). The last element of the path indicates the
    server name (e.g., "Machine1") and appears as the name on the servers list in the UI. This can be overridden with
    the environment variable 'APPDYNAMICS_MACHINE_HIERARCHY_PATH' or the system property
    '-Dappdynamics.machine.agent.hierarchyPath'.

    Note: The number of characters up to but not including the last vertical bar must not exceed 95. -->
    <machine-path></machine-path>

</controller-info>
```