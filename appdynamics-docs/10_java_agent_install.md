**Java Agent Install**
The Java Agent is installed on each CIRRAS server.

Once installed and configured, it will be visible in the AppDynamics Controller -> Applications tab -> (target app) -> Tiers & Nodes.

**IMPORTANT:**  At this time I cannot get the Java Agent to run properly on our servers.  It generates Java security errors and prevents JBoss from running our application.  The basic installation steps below are sound, but specific Java settings are needed to overcome this problem.  See the Troubleshooting section at the end of this page for more information.



Before installing the Java Agent, make sure the AppDyamics Enterprise Console and Controllers are running

**Download the software** from https://accounts.appdynamics.com/
    example file name:  AppServerAgent-1.8-23.11.0.35328.zip

**Copy the file to each server** (Test App, Test Proxy, UAT App, UAT Proxy).

    create a directory on the target servers and copy the zip file to it
        example: /home/smikitarian/appdynamics-install-files
    create the installation directory:
        mkdir /opt/appd-java-agent
    change ownership of the installation directory to jboss-eap:
        chown -R jboss-eap: /opt/appd-java-agent
    set permissions on the installation directory:
        chmod 775 /opt/appd-java-agent
    cd to /opt
        cd /opt
    copy the insallation file to /opt
        cp /path/to/install-file/AppServerAgent-<version>.zip /opt
    change ownership of the installation file:
        chown jboss-eap: AppServerAgent-<version>.zip
    switch user to jboss-eap:
        su jboss-eap
    unzip the file to the target directory (this completes the installation - next step is configuration and activation):
        unzip -q ./AppServerAgent-1.8-23.12.0.35361.zip -d /opt/appd-java-agent

**Configure and Activate the installation**
  
Edit the AppDyamics controller-info.xml file:  
cd /opt/appd-java-agent/conf  
  
  become root user  
  vi controller-info.xml  
  type :set number change the following settings in each line in the controller-info.xml file.  
  complete the following:
  
    set <controller-host> to 10.205.199.41 (APPD-ENT-CONSOLE) (line 20)
    set <controller-port> to 8090 (line 29)
    set <controller-ssl-enabled> to false (line 37)
    set <application-name> to CIRRAS-test (tst servers) or CIRRAS-uat (uat server) (line 51)
    set <tier-name> to test-app-server or uat-app-server as appropriate (line 60)
    set <nodename> to app-server or proxy-server as appropriate (line 69)
    set <account-name> customer1 (line 114)
    set <account-access-key> found on the controller's license page in browser (line 119)
    save file (:wq)
    
  
  
Edit the CIRRAS JBoss standalone.conf file:  
  
become root user  
cd /opt/jboss-eap/bin  
vi standalone.conf 
Add the following lines to the file BEFORE the line reading SECMGR="true"  

```
# Load classes into the JVM so the AppDynamics agent (below) can use it
JAVA_OPTS="$JAVA_OPTS -Djboss.modules.system.pkgs=org.jboss.byteman,com.singularity"
 
# Allows CIRRAS to communicate with AppDynamics
JAVA_OPTS="$JAVA_OPTS -javaagent:/opt/appd-java-agent/javaagent.jar"
 
# Allows AppDynamics to find logmanager
JAVA_OPTS="$JAVA_OPTS -Djava.util.logging.manager=org.jboss.logmanager.LogManager -Xbootclasspath/p:{{}} /opt/jboss-eap/modules/system/layers/base/org/jboss/logmanager/main/jboss-logmanager-2.1.18.Final-redhat-00001.jar"
```
  
save file (:wq!)  
  
Restart JBoss:  
sudo -i  
restart-jboss  
  
  
**Troubleshooting**

In the end I am stuck in a cycle of limited success.
Sometimes I can get the Java Agent to register and run for a period of time, but eventually JBoss will fail and my application becomes inaccessible.

One of the errors I receive consistently is this: java.security.AccessControlException

I also see jboss failing to start when I enable logging in my /opt/jboss-eap/bin/standalone.conf file

I get the sense that setting file permissions for logging, and then configuring the controller-info.xml parameters (either in the xml file or vial the JAVA_OPTS in the conf file) is where the answer lies, but I'm missing the target by just a bit.



**Security Exception**

I attempted to solve the java.security.AccessControlException  problems by adding the following line to /opt/jboss-eap/standalone/configuration/standalone.xml  file within the <minimum-set> tag:
<permission class="java.io.FilePermission" name="/opt/appd-java-agent/-" actions="read,write,delete"/>

Unfortunately I had no luck with this.



Here are steps I've taken in the /opt/jboss-eap/bin/standalone.conf  file:



The Java Agent is installed at /opt/appd-java-agent
Owner and Group are jboss-eap jboss-eap
Permissions are 775

AppDynamics configuration in our /opt/jboss-eap/bin/standalone.conf file are:
```
# Load classes into the JVM so the AppDynamics agent (below) can use it
JAVA_OPTS="$JAVA_OPTS -Djboss.modules.system.pkgs=org.jboss.byteman,com.singularity"

# Allows CIRRAS to communicate with AppDynamics
JAVA_OPTS="$JAVA_OPTS -javaagent:/opt/appd-java-agent/javaagent.jar"

# Allows AppDynamics to find logmanager
# JAVA_OPTS="$JAVA_OPTS -Djava.util.logging.manager=org.jboss.logmanager.LogManager -Xbootclasspath/p:{{}} /opt/jboss-eap/modules/system/layers/base/org/jboss/logmanager/main/jboss-logmanager-2.1.18.Final-redhat-00001.jar"

# Configure AppDynamics Java Agent to find the AppDynamics Controller
# This takes the place of editing the /opt/appd-java-agent/conf/controller-info.xml file
# JAVA_OPTS="$JAVA_OPTS -javaagent:/opt/appd-java-agent/javaagent.jar -Dappdynamics.controller.hostName=10.205.199.41 -Dappdynamics.controller.port=8090 -Dappdynamics.agent.applicationName=CIRRAS-UAT -Dappdynamics.agent.accountName=customer1 -Dappdynamics.agent.accountAccessKey=41b70120-1b3c-4ab9-a58f-d67bc7ef8df6 -Dappdynamics.agent.tierName=uat-app-server -Dappdynamics.agent.nodeName=app-server"
```

The first two configuration items load without a problem. Logging in JBoss is normal when they do.

When I uncomment the third item (# Allows AppDynamics to find logmanager), JBoss doesn't start and there is nothing in the logs.
I re-comment it out and things work again.

When I uncomment the fouth item (# Configure AppDynamics Java Agent to find the AppDynamics Controller), JBoss doesn't start and the logs show the following:

2024-02-27 21:59:40,525 ERROR [stderr] (Log4j2-TF-10-AsyncLoggerConfig-2) AsyncLogger error handling event seq=5734, value='Logger=com.singularity.bci.TransformationManager Level=ERROR Message=java.lang.NoClassDefFoundError: Could not initialize class org.wildfly.security.manager._private.SecurityMessages caught trying to transform class org/jboss/as/server/BootstrapImpl$ShutdownHook': java.lang.NoClassDefFoundError: Could not initialize class org.wildfly.security.manager._private.SecurityMessages
2024-02-27 21:59:40,527 ERROR [stderr] (Log4j2-TF-10-AsyncLoggerConfig-2) java.lang.NoClassDefFoundError: Could not initialize class org.wildfly.security.manager._private.SecurityMessages

Permissions on /opt/appd-java-agent/ver23.12.0.35361 are as follows:
drwxrwxr-x. 10 jboss-eap jboss-eap 4096 Dec 12 03:53 ver23.12.0.35361

And on its contents:
drwxrwxr-x. 5 jboss-eap jboss-eap 4096 Feb 27 21:56 conf
drwxrwxr-x. 6 jboss-eap jboss-eap 95 Dec 12 03:43 external-services
-rwxrwxr-x. 1 jboss-eap jboss-eap 170911 Dec 12 03:43 javaagent.jar
-rwxrwxr-x. 1 jboss-eap jboss-eap 198 Dec 12 03:53 javaagent.jar.asc
drwxrwxr-x. 3 jboss-eap jboss-eap 4096 Dec 12 03:53 lib
drwxrwxr-x. 4 jboss-eap jboss-eap 39 Feb 27 21:56 logs
drwxrwxr-x. 2 jboss-eap jboss-eap 54 Dec 12 03:53 multi-release
-rwxrwxr-x. 1 jboss-eap jboss-eap 3480 Dec 12 03:43 readme.txt
-rwxrwxr-x. 1 jboss-eap jboss-eap 198 Dec 12 03:53 readme.txt.asc
drwxrwxr-x. 3 jboss-eap jboss-eap 17 Dec 12 03:43 sdk
drwxrwxr-x. 2 jboss-eap jboss-eap 6 Dec 12 03:43 sdk-plugins
drwxrwxr-x. 5 jboss-eap jboss-eap 55 Dec 12 03:43 utils



All of these steps are documented with the AppDynamics Support Team in their ticketing system under Request #389809

All AppDynamics Support POCs are listed on the POCs and License Page here in Confluence

