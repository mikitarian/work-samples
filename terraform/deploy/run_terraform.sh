#!/bin/bash

set -e

# Wrapper script to ensure the environment file exists and the workspace is selected
# Example: ./run_terraform.sh dev plan

ENVIRONMENT=${1:-'dev'}
ACTION=${2:-'apply'}
ARGUMENTS=${@:3}

usage() {
    echo ""
    echo "Wrapper around Terraform"
    echo "First argument is the environment which must match a file inside the env/ directory and an existing workspace"
    echo "Everything after first is passed to Terraform"
    echo "./run_terraform.sh dev plan"
    echo "..or..."
    echo "./run_terraform.sh dev apply -var=\"image_tag=latest\""
    echo ""
}

if [ ! -f "env/$ENVIRONMENT.tfvars" ]; then
    echo "The environment file env/$ENVIRONMENT.tfvars does not exist"
    exit 1
fi

if ! type terraform; then
    echo "Could not find Terraform"
    exit 1
fi

if terraform workspace select $ENVIRONMENT; then
    echo "Running terraform $ACTION"
    terraform $ACTION -var-file="env/$ENVIRONMENT.tfvars" $ARGUMENTS
else
    echo "Could not confirm Terraform worksapce exists ($ENVIRONMENT)"
fi
