#!/bin/bash

# NOTE:  This script will NOT run as-is.
#         AWS ARNs have been removed and some data sanitization has been done
#         so the script can be reviewed outside of proprietary channels.


# This script allows the user to clear and load the target application's data
# in MS SQL Server and in AWS Dynamo databases
#

aws_region="us-east-1"

username=default
password=default
dbinstance='proxy.mydomain.net'
dbport=1433
dbname='DEFAULT_SQL_DB_NAME'

# ######### Begin Common functions #########

error_exit()
{
	echo ""
    echo "Error: $1"
	echo ""
    exit aw
}

quit_script(){

    clear
    printf "\n\n\tQuitting script\n\n"
    sleep 1
    clear

    exit 0
}

# ######### End Common functions #########

# ######### Begin SQL Server functions #########

get_sql_username(){
    username=$(aws secretsmanager get-secret-value --secret-id arn:aws-us-gov:secretsmanager:<AWS-REGION>:<AWS-ID>:secret:dev/SQLDB-cRmnrT --query SecretString --output text | jq -r '.rds_db_username')
    echo $username
    # printf "\nusername is $username\n\n"
}

get_sql_password(){
    password=$(aws secretsmanager get-secret-value --secret-id arn:aws-us-gov:secretsmanager:<AWS-REGION>:<AWS-ID>:secret:dev/SQLDB-cRmnrT --query SecretString --output text | jq -r '.rds_db_password')
    echo $password
    # printf "\npassword is $password\n\n"
}

db_conn_test(){

    dbuser=$(get_sql_username)
    dbpwd=$(get_sql_password)

    sqlcmd -S$dbinstance,$dbport -U$dbuser -P$dbpwd -v dbname=$dbname -q "exit()" >> 2&>1
    if [ -f "./1" ]
    then
        error=$(cat ./1)
        rm -f ./1
        rm -f ./2
    fi

    echo "$error"
}


select_sql_server_db(){
    clear;

    printf "\n\nEnter Database Parameters:\n\n" 

    read -p "Enter the SQL Server Instance (press Enter for: $dbinstance)" dbinstance_entry
        dbinstance_entry="${dbinstance_entry:=$dbinstance}"
        printf "\t$dbinstance_entry\n\n"

    read -p "Enter the SQL Server Port (press Enter for: $dbport)" dbport_entry
        dbport_entry="${dbport_entry:=$dbport}"
        printf "\t$dbport\n\n"

    read -p "Enter the SQL Server Port (press Enter for: $dbname)" dbname_entry
        dbname_entry="${dbname_entry:=$dbname}"
        printf "\t$dbname\n\n"

    printf "You are about to $2 in the $dbname_entry database.\n"
            # $2 is second parameter passed from main_menu() to select_sql_server_db()
            # and is the description of the action about to be taken

    printf "\n"
    read -n1 -p "Proceed? (y/n)" go_nogo
        case "$go_nogo" in
            y)

                dbinstance="$dbinstance_entry"
                dbport="$dbport_entry"
                dbname="$dbname_entry"

                dbconn_result=$(db_conn_test)
                # echo "dbconn_result is: $dbconn_result"

                if [[ -z $dbconn_result ]]
                then

                    $1
                    # $1 is first parameter passed from main_menu() to select_sql_server_db()
                    # and is the name of the next function to be executed

                else
                    printf "\n\n**********************************************************"
                    printf "\n\nDatabase Connection Failed."
                    printf "\n\nThe DB Parameters you entered were:"
                    printf "\n\tInstance:\t$dbinstance"
                    printf "\n\tPort:\t\t$dbport"
                    printf "\n\tName:\t\t$dbname"
                    printf "\nPlease check the DB parameters and try again."
                    printf "\n\n**********************************************************\n\n"
                    exit 1

                fi
                
                ;;
            n)
                clear
                main_menu
                ;;
        esac

}

list_tables(){
    sqlcmd -S$dbinstance,$dbport -U$dbuser -P$dbpwd -d$dbname -Q "SELECT table_name FROM INFORMATION_SCHEMA.TABLES ORDER BY table_name;"
}

delete_sql_server_demo_records(){

    dbuser=$(get_sql_username)
    dbpwd=$(get_sql_password)

    FILES="./scripts/delete_records/*.sql"
    for f in $FILES
    do
        printf "\nRunning SQL file: $f\n"
        sleep 1.25;
        sqlcmd -S$dbinstance,$dbport -U$dbuser -P$dbpwd -v dbname=$dbname -i $f
        printf "\n"
    done

}

insert_sql_server_demo_records(){
    
    dbuser=$(get_sql_username)
    dbpwd=$(get_sql_password)

    FILES="./scripts/insert_records/*.sql"
    for f in $FILES
    do
        printf "\nRunning SQL file: $f\n"
        sleep 1.25;
        sqlcmd -S$dbinstance,$dbport -U$dbuser -P$dbpwd -v dbname=$dbname -i $f
        printf "\n"
    done

}

drop_sql_server_demo_data_tables(){

    dbuser=$(get_sql_username)
    dbpwd=$(get_sql_password)

    printf "\nTables in the $dbname database at start:\n\n"
    list_tables

    FILES="./scripts/drop_tables/*.sql"
    for f in $FILES
    do
        printf "\nRunning SQL file: $f\n"
        sleep 1.25;
        sqlcmd -S$dbinstance,$dbport -U$dbuser -P$dbpwd -v dbname=$dbname -i $f
        printf "\n"
    done

    printf "\nTables in the $dbname database at end:\n\n"
    list_tables
    printf "\n\n"

}

add_sql_server_demo_data_tables(){
    
    dbuser=$(get_sql_username)
    dbpwd=$(get_sql_password)

    printf "\nTables in the $dbname database at start:\n\n"
    list_tables

    FILES="./scripts/add_tables/*.sql"
    for f in $FILES
    do
        printf "\nRunning SQL file: $f\n"
        sleep 1.25;
        sqlcmd -S$dbinstance,$dbport -U$dbuser -P$dbpwd -v dbname=$dbname -i $f
        printf "\n"
    done

    printf "\nTables in the $dbname database at end:\n\n"
    list_tables
    printf "\n\n"
}

# ######### End SQL Server functions #########

# ######### Begin DynamoDb functions #########

select_dynamodb_table(){
    # only allow Dev or Test users
    # then include related UserRole & UserProfile tables in export/delete/insert functions below
    clear;

    printf "\n\nYou have chosen to:\n\n"
    printf "\t$2\n\n"
    printf "Please choose a table for this action:\n\n"
    printf "\t(1) Dev Users\n"
    printf "\t(2) Test Users\n\n"
    printf "\t(3) Return to main menu\n\n"
    printf "\t(q) Quit\n\n"

    read -n1 -p "Enter selection here:" dynamodb_table
        case "$dynamodb_table" in
            1)
                $1 Dev dev
                ;;
            2)
                $1 Test test
                ;;
            3)
                clear
                main_menu
                ;;
            *)
                quit_script
                ;;
        esac
}


# for testing only
# remove before final commit
choose_dynamodb_action(){
    printf "\n\nwow\n"
    printf "$1\n\n"
    printf "$2\n\n"
}

export_users_from_dynamodb(){

    env=$2
    declare a table_array=("User" "UserRole"  "UserProfile")

    clear;

    mkdir -p ./outputs/$env/users
    rm -f ./outputs/$env/users/*.json

    printf "\n\nExporting from $1 DynamoDB..."

	for t in "${table_array[@]}";
	do

        file_name="cm-$env-$t.json"
        table_name="cm-$env-$t"

        printf "\n\t$t"

        aws dynamodb scan --table-name $table_name --region $aws_region --max-items 1000 --output json > ./outputs/$env/users/$file_name
    
    done
    printf "\nDone!\n\n"
}

check_for_export_files(){

    env=$2
    declare a file_array=("User" "UserRole"  "UserProfile")
    missing_files=""

    clear

    printf "\n\nChecking for table backup files before deleting...\n"

	for f in "${file_array[@]}";
	do

        file_name="cm-$env-$f.json"

        printf "\n\t$file_name"

        if ! [[ -f ./outputs/$env/users/$file_name ]]
        then
            missing_files+="\t${file_name}\n"
        fi

    done

    if ! [[ -z $missing_files ]]
    then
        clear
        printf "\n\nThe following table backup files are missing from ./outputs/$env/users"
        printf "\n\n$missing_files"
        printf "\nIf you delete these tables from AWS, you may not be able to recover the user accounts."
        printf "\nPlease run this script\'s export command - or - ensure you have a backup stored somewhere else.\n\n"
        read -n1 -p "Do you want to proceed? (y/n)" proceed
        case "$proceed" in
            y)
                delete_all_users_from_dynamodb $1 $2
                ;;
            *)
                quit_script
                ;;
        esac
    else
        printf "\n\nTable backup files exist.  It's safe to proceed with deleting records.\n"

        delete_all_users_from_dynamodb $1 $2
    fi

    
}

delete_all_users_from_dynamodb(){

    env=$2
    declare a table_array=("User" "UserRole" "UserProfile")

	for t in "${table_array[@]}";
	do

        if [[ "$t" == "User" ]]
        then
            attribute_key="UserName"
        elif [[ "$t" == "UserRole" ]]
        then
            attribute_key="UserIdentification"
        elif [[ "$t" == "UserProfile" ]]
        then
            attribute_key="UserIdentification"
        fi

        printf "\n$attribute_key\n"
        printf "\nDeleting users from cm-$env-$t\n\n"

        while read -r user; do
            printf "$user\n"
            aws dynamodb delete-item --table-name cm-$env-$t --key="$user"
        done <<<$(aws dynamodb scan --attributes-to-get "$attribute_key" --table-name cm-$env-$t --query "Items[*]" | jq --compact-output '.[]')

        printf "\nDone!\n\n"
    done

    # printf "\n\nin delete all users from dynamodb and $1 and $2\n\n"
    # aws dynamodb scan --table-name cm-dev-User --region us-gov-east-1 --max-items 1000 --output json > ./cm-dev-User-export.json
    # # this uses jq but basically we're just removing 
    # # some of the json fields that describe an existing 
    # # ddb table and are not actually part of the table schema/defintion
    # aws dynamodb describe-table --table-name $table_name | jq '.Table | del(.TableId, .TableArn, .ItemCount, .TableSizeBytes, .CreationDateTime, .TableStatus, .ProvisionedThroughput.NumberOfDecreasesToday)' > schema.json
    # # delete the table
    # aws dynamodb delete-table --table-name $table_name
    # # create table with same schema (including name and provisioned capacity)
    # aws dynamodb create-table --cli-input-json file://schema.json
}

import_users_to_dynamodb(){

    env=$2
    declare a table_array=("User" "UserRole" "UserProfile")

	for t in "${table_array[@]}";
	do

        cat ./outputs/dev/users/cm-$env-$t-export.json | jq -c '.Items[]' | while read -r line; do aws --region $aws_region  dynamodb put-item --table-name cm-$env-$t --item "$line"; done

        printf "\nDone!\n\n"
    done

    
}


# ######### End DynamoDb functions #########

main_menu()
{
	
	printf "\nThis is the Data Management Script."
	printf "\n"
	printf "\nRequirements to run this script are:"
	printf "\n"
    printf "\n\t- AWS Credentials and AWS CLI access to the AWS environment"
    printf "\n\t\taws cli info here: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html"
    printf "\n"
    printf "\n\t- sqlcmd and jq utilities installed locally"
    printf "\n\t\tsqlcmd info here: https://learn.microsoft.com/en-us/sql/linux/sql-server-linux-setup-tools"
    printf "\n\t\tjq info here: https://confluence.accenturefederal.com/display/EAAD/Miscellaneous+Utilities"
    printf "\n"
	printf "\nChoose your next action:"
    printf "\n"
    printf "\n\tSQL Server Database:"
	printf "\n\t\t(1)-Delete Demo Data Records"
	printf "\n\t\t(2)-Insert Demo Data Records"
	printf "\n\t\t(3)-Drop Demo Data Tables"
    printf "\n\t\t(4)-Add Demo Data Tables"
    printf "\n"
    printf "\n\tDynamoDB:"
    printf "\n\t\t(5)-Export Users"
    printf "\n\t\t(6)-Delete Users"
    printf "\n\t\t(7)-Import Users"
    printf "\n"
	printf "\n\t-Quit this script (q)"
	printf "\n\n"
	read -n1 -p "        Enter your choice here: " decision
		case "$decision" in
			1)
				printf "\n\n\t-you chose \"Delete SQL Server Demo Data Records\"\n\n"
                sleep 1
				select_sql_server_db "delete_sql_server_demo_records" "Delete SQL Server Demo Data Records"
				;;
			2)
				printf "\n\n\t-you chose \"Insert SQL Server Demo Data Records\"\n\n"
                sleep 1
				select_sql_server_db "insert_sql_server_demo_records" "Insert SQL Server Demo Data Records"
				;;
			3)
				printf "\n\n\t-you chose \"Drop SQL Server Demo Data Tables\"\n\n"
                sleep 1
				select_sql_server_db "drop_sql_server_demo_data_tables" "Drop SQL Server Demo Data Tables"
				;;
			4)
				printf "\n\n\t-you chose \"Add SQL Server Demo Data Tables\"\n\n"
                sleep 1
				select_sql_server_db "add_sql_server_demo_data_tables" "Add SQL Server Demo Data Tables"
				;;
			5)
				printf "\n\n\t-you chose \"Export Users from DynamoDB\"\n\n"
                sleep 1
				select_dynamodb_table "export_users_from_dynamodb" "Export Users from DynamoDB"
				;;
			6)
				printf "\n\n\t-you chose \"Delete Users from DynamoDB\"\n\n"
                sleep 1
				select_dynamodb_table "check_for_export_files" "Delete Users from DynamoDB"
				;;
			7)
				printf "\n\n\t-you chose \"Import Users to DynamoDB\"\n\n"
                sleep 1
				select_dynamodb_table "import_users_to_dynamodb" "Import Users to DynamoDB"
				;;
			q)
				quit_script
				;;
			*)
				printf "\n\n***********************************************************************************************************"
				printf "\n\nValid inputs are \"1-7\" and \"q\"."
				printf "\n\nAnything else will bring you to this message and end the script.\n"
				printf "\n***********************************************************************************************************\n"
				;;
		esac
}

check_aws_login(){

    not_logged_in_msg=""

    aws_user_id=( $(aws sts get-caller-identity | jq '.UserId' | tr -d '"') )

    if [[ ${#aws_user_id} < 1 ]]
    then
        clear
        printf "\n*************************************************************************\n"
        printf "You do not appear to be logged in to AWS.\n\n"
        printf "You can check your login status by running the following command:\n\n"
        printf "\taws sts get-caller-identity\n\n"
        printf "When you are logged in, the command should return something like this:\n\n"
        printf "    {\n"
        printf "\t\"UserId\": \"AIDxxxxxxxxxxxxxxxxSQL\",\n"
        printf "\t\"Account\": \"28########56\",\n"
        printf "\t\"Arn\": \"arn:aws-us-gov:iam::28########56:user/your.name\"\n"
        printf "    }\n\n"
        printf "You must be logged in before running this script."
        printf "\n*************************************************************************\n\n"
    else
        main_menu
    fi

}

check_aws_login
