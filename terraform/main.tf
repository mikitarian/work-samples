data "aws_region" "current" {}

locals {
  app_name    = "cm"
  name_prefix = "${local.app_name}-${var.environment}"

  admin_user_info = jsondecode(data.aws_secretsmanager_secret_version.admin_user_info.secret_string)

  tags = {
    app_name      = local.app_name
    managed       = "terraform"
    configuration = "terraform/deploy"
    environment   = var.environment
  }

  ports = {
    nlb_http                = 80
    nlb_https               = 443
    api_gateway_http        = 80
    api_gateway_https       = 443
    auth_server             = 8383
    cloud_manager_server    = 8385
    purchase_request_server = 8386
    database                = 1433
    services = {
      low  = 8383
      high = 8386
    }
  }

  cm_service_environment_vars = [
    { name = "AWS_REGION", value = data.aws_region.current.name },
    { name = "ASPNETCORE_ENVIRONMENT", value = var.environment },
    { name = "ServerURI", value = "app-server.${local.name_prefix}" },
    { name = "PurchaseRequestServerURI", value = "purchase-request-server.${local.name_prefix}" },
    { name = "AuthServerURI", value = "auth-server.${local.name_prefix}" },
    { name = "StaticWebURI", value = aws_s3_bucket_website_configuration.www.website_endpoint }
  ]

  cm_secrets = [
    { name = "ConnectionString", valueFrom = aws_secretsmanager_secret_version.db_connection_string.arn }
  ]
}


##################################
# Default Admin User Credentials #
##################################
data "aws_secretsmanager_secret" "admin_user_secret" {
  name = "${local.app_name}/${var.environment}/default-admin-credentials"
}

data "aws_secretsmanager_secret_version" "admin_user_info" {
  secret_id = data.aws_secretsmanager_secret.admin_user_secret.id
}


#################
# Load Balancer #
#################
module "nlb" {
  source = "../modules/aws-lb"

  name = local.name_prefix

  vpc_id             = var.vpc_id
  subnets            = var.public_subnets
  internal           = false
  load_balancer_type = "network"

  # NOTE: The name_prefix is limited to 6 characters
  target_groups = {
    api-u = {
      # http (unsecure)
      name_prefix          = "api-u"
      backend_protocol     = "TCP"
      backend_port         = local.ports.api_gateway_http
      target_type          = "ip"
      deregistration_delay = "30"
      preserve_client_ip   = true
    },
    api-s = {
      # https (secure)
      name_prefix          = "api-s"
      backend_protocol     = "TCP"
      backend_port         = local.ports.api_gateway_https
      target_type          = "ip"
      deregistration_delay = "30"
      preserve_client_ip   = true
    }
  }

  http_tcp_listeners = {
    api-u = {
      # http
      port               = local.ports.nlb_http
      protocol           = "TCP"
      target_group_index = "api-u"
    },
    api-s = {
      # https
      port               = local.ports.nlb_https
      protocol           = "TCP"
      target_group_index = "api-s"
    }
  }

  tags = local.tags
}


###################
# CloudWatch Logs #
###################
# tfsec:ignore:aws-cloudwatch-log-group-customer-key
resource "aws_cloudwatch_log_group" "this" {
  name = local.name_prefix

  retention_in_days = var.log_retention_days

  tags = local.tags
}

#######
# ECS #
#######
# tfsec:ignore:aws-ecs-enable-container-insight
resource "aws_ecs_cluster" "this" {
  name = local.name_prefix

  setting {
    name  = "containerInsights"
    value = "disabled"
  }

  tags = local.tags
}


#####################
# Service Discovery #
#####################
resource "aws_service_discovery_private_dns_namespace" "this" {
  name        = local.name_prefix
  description = "Private DNS namespace"
  vpc         = var.vpc_id
}

##################
# Security Group #
##################
resource "aws_security_group" "ecs-services" {
  name        = "${local.name_prefix}-ecs-services"
  description = "Ingress for ECS Services"
  vpc_id      = var.vpc_id

  # ingress by prefix list
  dynamic "ingress" {
    for_each = length(var.web_ingress_prefix_list) > 0 ? [local.ports.api_gateway_http, local.ports.api_gateway_https] : []
    content {
      description     = "Ingress by prefix list (${ingress.value})"
      from_port       = ingress.value
      to_port         = ingress.value
      protocol        = "tcp"
      prefix_list_ids = var.web_ingress_prefix_list
    }
  }

  # ingress by CIDR block
  dynamic "ingress" {
    for_each = length(var.web_ingress_cidr_list) > 0 ? [local.ports.api_gateway_http, local.ports.api_gateway_https] : []
    content {
      description = "Ingress by CIDR (${ingress.value})"
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = var.web_ingress_cidr_list
    }
  }

  # NLB health check
  dynamic "ingress" {
    for_each = [local.ports.api_gateway_http, local.ports.api_gateway_https]
    content {
      description = "NLB Health Checks (${ingress.value})"
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = [var.vpc_cidr_block]
    }
  }

  ingress {
    description = "Service to service communication"
    from_port   = local.ports.services.low
    to_port     = local.ports.services.high
    protocol    = "tcp"
    self        = true
  }

  ingress {
    description = "Service to database communication"
    from_port   = local.ports.database
    to_port     = local.ports.database
    protocol    = "tcp"
    self        = true
  }

  # tfsec:ignore:aws-ec2-no-public-egress-sgr
  egress {
    description = "Egress to all"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.tags
}

################
# ECS Services #
################
module "api_gateway" {
  source                         = "../modules/ecs-service"
  service_name                   = "api-gateway"
  service_discovery_namespace_id = aws_service_discovery_private_dns_namespace.this.id
  task_name                      = "${local.name_prefix}-api-gateway"
  ecs_cluster                    = aws_ecs_cluster.this.id
  subnets                        = var.private_subnets
  security_groups                = [aws_security_group.ecs-services.id]
  execution_role_arn             = var.ecs_execution_role
  task_role_arn                  = var.ecs_task_role
  repository_uri                 = "${var.container_repository_base}/cm-api-gateway"
  image_tag                      = var.image_tag
  health_check_command           = ["CMD-SHELL", "curl --insecure --fail https://localhost:${local.ports.api_gateway_https}/Auth/health || exit 1"]
  container_cpu                  = 512
  container_memory               = 2048
  container_is_essential         = true
  container_ports                = [local.ports.api_gateway_http, local.ports.api_gateway_https]
  environment_variables          = local.cm_service_environment_vars
  region                         = data.aws_region.current.name
  log_group                      = aws_cloudwatch_log_group.this.name

  load_balancer_blocks = [
    {
      target_group_arn = module.nlb.target_groups.api-u.arn
      container_name   = "api-gateway"
      container_port   = local.ports.api_gateway_http
    },
    {
      target_group_arn = module.nlb.target_groups.api-s.arn
      container_name   = "api-gateway"
      container_port   = local.ports.api_gateway_https
    }
  ]
}

module "auth_server" {
  source                         = "../modules/ecs-service"
  service_name                   = "auth-server"
  service_discovery_namespace_id = aws_service_discovery_private_dns_namespace.this.id
  task_name                      = "${local.name_prefix}-auth-server"
  ecs_cluster                    = aws_ecs_cluster.this.id
  subnets                        = var.private_subnets
  security_groups                = [aws_security_group.ecs-services.id]
  execution_role_arn             = var.ecs_execution_role
  task_role_arn                  = var.ecs_task_role
  repository_uri                 = "${var.container_repository_base}/cm-auth-server"
  image_tag                      = var.image_tag
  health_check_command           = ["CMD-SHELL", "curl --fail http://localhost:${local.ports.auth_server}/Auth/health || exit 1"]
  container_cpu                  = 512
  container_memory               = 2048
  container_is_essential         = true
  container_ports                = [local.ports.auth_server]
  environment_variables          = local.cm_service_environment_vars
  region                         = data.aws_region.current.name
  log_group                      = aws_cloudwatch_log_group.this.name
}

module "cloud_manager_server" {
  source                         = "../modules/ecs-service"
  service_name                   = "app-server"
  service_discovery_namespace_id = aws_service_discovery_private_dns_namespace.this.id
  task_name                      = "${local.name_prefix}-app-server"
  ecs_cluster                    = aws_ecs_cluster.this.id
  subnets                        = var.private_subnets
  security_groups                = [aws_security_group.ecs-services.id]
  execution_role_arn             = var.ecs_execution_role
  task_role_arn                  = var.ecs_task_role
  repository_uri                 = "${var.container_repository_base}/cm-app-server"
  image_tag                      = var.image_tag
  health_check_command           = ["CMD-SHELL", "curl --fail http://localhost:${local.ports.cloud_manager_server}/User/health || exit 1"]
  container_cpu                  = 512
  container_memory               = 2048
  container_is_essential         = true
  container_ports                = [local.ports.cloud_manager_server]
  environment_variables          = local.cm_service_environment_vars
  secrets                        = local.cm_secrets
  region                         = data.aws_region.current.name
  log_group                      = aws_cloudwatch_log_group.this.name
}

module "purchase_request_server" {
  source                         = "../modules/ecs-service"
  service_name                   = "purchase-request-server"
  service_discovery_namespace_id = aws_service_discovery_private_dns_namespace.this.id
  task_name                      = "${local.name_prefix}-purchase-request-server"
  ecs_cluster                    = aws_ecs_cluster.this.id
  subnets                        = var.private_subnets
  security_groups                = [aws_security_group.ecs-services.id]
  execution_role_arn             = var.ecs_execution_role
  task_role_arn                  = var.ecs_task_role
  repository_uri                 = "${var.container_repository_base}/cm-purchase-request-server"
  image_tag                      = var.image_tag
  health_check_command           = ["CMD-SHELL", "curl --fail http://localhost:${local.ports.purchase_request_server}/PurchaseRequest/health || exit 1"]
  container_cpu                  = 512
  container_memory               = 2048
  container_is_essential         = true
  container_ports                = [local.ports.purchase_request_server]
  environment_variables          = local.cm_service_environment_vars
  secrets                        = local.cm_secrets
  region                         = data.aws_region.current.name
  log_group                      = aws_cloudwatch_log_group.this.name
}


#######
# RDS #
#######

# generate a random password and setup a secret
resource "random_password" "db" {
  length           = 16
  special          = true
  override_special = ".-_$%^#"
}

# tfsec:ignore:aws-ssm-secret-use-customer-key
resource "aws_secretsmanager_secret" "db_connection_string" {
  name = "${local.app_name}/${var.environment}/db-connection-string"
}

resource "aws_secretsmanager_secret_version" "db_connection_string" {
  secret_id     = aws_secretsmanager_secret.db_connection_string.id
  secret_string = "Data Source=${module.db.db_instance_address},${local.ports.database};Initial Catalog=CM;User ID=cloud_manager;Password=${random_password.db.result};"
}

# create the instance
# tfsec:ignore:aws-rds-enable-performance-insights
module "db" {
  source = "../community-modules/terraform-aws-rds"

  identifier = local.name_prefix

  engine               = var.db_engine
  engine_version       = var.db_engine_version
  family               = var.db_family               # DB parameter group
  major_engine_version = var.db_major_engine_version # used by the option group in AWS
  instance_class       = var.db_instance_class

  allocated_storage     = var.db_allocated_storage
  max_allocated_storage = var.db_max_allocated_storage
  storage_encrypted     = true

  username               = "cloud_manager"
  port                   = local.ports.database
  create_random_password = false
  password               = random_password.db.result

  multi_az               = false
  vpc_security_group_ids = [aws_security_group.ecs-services.id]

  maintenance_window              = "Sun:00:00-Sun:03:00"
  backup_window                   = "03:00-06:00"
  enabled_cloudwatch_logs_exports = ["error"]
  create_cloudwatch_log_group     = true

  backup_retention_period = 7
  skip_final_snapshot     = true
  deletion_protection     = true

  create_db_subnet_group      = true
  db_subnet_group_description = "Subnet group for ${local.name_prefix}"
  subnet_ids                  = var.private_subnets

  options                   = []
  create_db_parameter_group = false
  license_model             = "license-included"
  character_set_name        = "Latin1_General_CI_AS"

  tags = local.tags
}

###################
# DynamoDB Tables #
###################
# tfsec:ignore:aws-dynamodb-enable-at-rest-encryption using default encryption
module "ddb_permission" {
  source = "../community-modules/terraform-aws-dynamodb-table"

  name = "${local.name_prefix}-Permission"

  attributes = [
    {
      name = "PermissionName"
      type = "S"
    }
  ]

  hash_key = "PermissionName"

  point_in_time_recovery_enabled = true

  tags = local.tags
}

resource "aws_dynamodb_table_item" "permission" {
  for_each   = jsondecode(file("./data/permission.json"))
  table_name = module.ddb_permission.dynamodb_table_id
  hash_key   = "PermissionName"
  item       = jsonencode(each.value)
}

# tfsec:ignore:aws-dynamodb-enable-at-rest-encryption using default encryption
module "ddb_site_role" {
  source = "../community-modules/terraform-aws-dynamodb-table"

  name = "${local.name_prefix}-SiteRole"

  attributes = [
    {
      name = "Identification"
      type = "S"
    },
    {
      name = "SiteName"
      type = "S"
    }
  ]

  hash_key  = "Identification"
  range_key = "SiteName"

  point_in_time_recovery_enabled = true

  tags = local.tags
}

resource "aws_dynamodb_table_item" "site_role" {
  for_each   = jsondecode(file("./data/site_role.json"))
  table_name = module.ddb_site_role.dynamodb_table_id
  hash_key   = "Identification"
  range_key  = "SiteName"
  item       = jsonencode(each.value)
}

# tfsec:ignore:aws-dynamodb-enable-at-rest-encryption using default encryption
module "ddb_user" {
  source = "../community-modules/terraform-aws-dynamodb-table"

  name = "${local.name_prefix}-User"

  attributes = [
    {
      name = "UserName"
      type = "S"
    }
  ]

  hash_key = "UserName"

  point_in_time_recovery_enabled = true

  tags = local.tags
}

# tfsec:ignore:aws-dynamodb-enable-at-rest-encryption using default encryption
resource "aws_dynamodb_table_item" "admin_user" {
  table_name = module.ddb_user.dynamodb_table_id
  hash_key   = "UserName"

  item = <<ITEM
  {
    "UserName": { "S": "${local.admin_user_info.username}" },
    "DistinguishedName": { "S": "${local.admin_user_info.distinguished-name}" },
    "FirstName": { "S": "${local.admin_user_info.first-name}" },
    "Identification": { "S": "${local.admin_user_info.identification}" },
    "LastName": { "S": "${local.admin_user_info.last-name}" },
    "Password": { "S": "${local.admin_user_info.password-hash}" }
  }
  ITEM

  lifecycle {
    ignore_changes = [item]
  }

}


# tfsec:ignore:aws-dynamodb-enable-at-rest-encryption using default encryption
module "ddb_user_profile" {
  source = "../community-modules/terraform-aws-dynamodb-table"

  name = "${local.name_prefix}-UserProfile"

  attributes = [
    {
      name = "UserIdentification"
      type = "S"
    },
    {
      name = "LastLoginDate"
      type = "S"
    }
  ]

  hash_key  = "UserIdentification"
  range_key = "LastLoginDate"

  point_in_time_recovery_enabled = true

  tags = local.tags
}

# tfsec:ignore:aws-dynamodb-enable-at-rest-encryption using default encryption
module "ddb_user_role" {
  source = "../community-modules/terraform-aws-dynamodb-table"

  name = "${local.name_prefix}-UserRole"

  attributes = [
    {
      name = "UserIdentification"
      type = "S"
    }
  ]

  hash_key = "UserIdentification"

  point_in_time_recovery_enabled = true

  tags = local.tags
}

# tfsec:ignore:aws-dynamodb-enable-at-rest-encryption using default encryption
resource "aws_dynamodb_table_item" "admin_user_role" {
  table_name = module.ddb_user_role.dynamodb_table_id
  hash_key   = "UserIdentification"

  item = <<ITEM
  {
    "UserIdentification": { "S": "${local.admin_user_info.user-identification-user-role-table}" },
    "IsDisabled": { "N": "${local.admin_user_info.is-disabled-user-role-table}" },
    "LastUpdate": { "S": "${local.admin_user_info.last-update-user-role-table}" },
    "SiteRole": { "S": "${local.admin_user_info.site-role-user-role-table}" }
  }
  ITEM

  lifecycle {
    ignore_changes = [item]
  }

}



######
# S3 #
######

### Purchase Requests ###

# tfsec:ignore:aws-s3-enable-bucket-logging
resource "aws_s3_bucket" "purchase_requests" {
  bucket = "${local.name_prefix}-purchase-requests"

  tags = local.tags
}

# tfsec:ignore:aws-s3-encryption-customer-key
resource "aws_s3_bucket_server_side_encryption_configuration" "purchase_requests" {
  bucket = aws_s3_bucket.purchase_requests.bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_versioning" "purchase_requests" {
  bucket = aws_s3_bucket.purchase_requests.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_public_access_block" "purchase_requests" {
  bucket = aws_s3_bucket.purchase_requests.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

### CloudTracker Invoices ###

# tfsec:ignore:aws-s3-enable-bucket-logging
resource "aws_s3_bucket" "cloudtracker_invoices" {
  bucket = "${local.name_prefix}-cloudtracker-invoices"

  tags = local.tags
}

# tfsec:ignore:aws-s3-encryption-customer-key
resource "aws_s3_bucket_server_side_encryption_configuration" "cloudtracker_invoices" {
  bucket = aws_s3_bucket.cloudtracker_invoices.bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_versioning" "cloudtracker_invoices" {
  bucket = aws_s3_bucket.cloudtracker_invoices.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_public_access_block" "cloudtracker_invoices" {
  bucket = aws_s3_bucket.cloudtracker_invoices.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

### Static UI ###

# tfsec:ignore:aws-s3-enable-versioning
# tfsec:ignore:aws-s3-enable-bucket-logging
resource "aws_s3_bucket" "www" {
  bucket = var.ui_fqdn

  tags = local.tags
}

# tfsec:ignore:aws-s3-encryption-customer-key
resource "aws_s3_bucket_server_side_encryption_configuration" "www" {
  bucket = aws_s3_bucket.www.bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_public_access_block" "www" {
  bucket = aws_s3_bucket.www.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_website_configuration" "www" {
  bucket = aws_s3_bucket.www.bucket

  index_document {
    suffix = "index.html"
  }

  routing_rule {
    condition {
      http_error_code_returned_equals = "404"
    }
    redirect {
      host_name               = var.fqdn
      protocol                = "https"
      replace_key_prefix_with = "?p=/"
    }
  }
}

resource "aws_s3_bucket_policy" "www" {
  bucket = aws_s3_bucket.www.id
  policy = data.aws_iam_policy_document.www_access.json
}

# TODO: should break out the ZPA access to a dynamic block
data "aws_iam_policy_document" "www_access" {
  statement {
    sid = "AllowFromVPC"
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    actions = ["s3:GetObject"]

    resources = ["${aws_s3_bucket.www.arn}/*"]

    condition {
      test     = "StringEquals"
      variable = "aws:sourceVpc"

      values = [var.vpc_id]
    }
  }

  statement {
    sid = "AllowFromZPA"
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    actions = ["s3:GetObject"]

    resources = ["${aws_s3_bucket.www.arn}/*"]

    condition {
      test     = "IpAddress"
      variable = "aws:SourceIp"

      values = [
        "170.248.172.244/30",
        "170.248.173.244/30"
      ]
    }
  }
}

#############################
# System Manager Parameters #
#############################
locals {
  systems_manager_parameters = yamldecode(file("${path.module}/env/${var.environment}.yaml"))
}

# all the parameters that Terraform MANAGES values (ignore_changes NOT true)
resource "aws_ssm_parameter" "ssm_paremeters_with_values" {
  for_each = { for k, v in local.systems_manager_parameters.parameters : k => v if lookup(v, "ignore_changes", false) != true }

  name        = "/${local.app_name}/${var.environment}/${each.key}"
  description = each.value.description
  type        = each.value.type
  value       = each.value.value

  tags = local.tags
}

# all the parameters that Terraform IGNORES values (ignore_changes is true)
resource "aws_ssm_parameter" "ssm_paremeters_without_values" {
  for_each = { for k, v in local.systems_manager_parameters.parameters : k => v if lookup(v, "ignore_changes", false) == true }

  name        = "/${local.app_name}/${var.environment}/${each.key}"
  description = each.value.description
  type        = each.value.type
  value       = each.value.value

  tags = local.tags

  lifecycle {
    ignore_changes = [value]
  }

  depends_on = [aws_ssm_parameter.ssm_paremeters_with_values]
}

## the dynamic parameters
resource "aws_ssm_parameter" "cloud_region" {
  name        = "/${local.app_name}/${var.environment}/CrmSettings/CloudRegion"
  description = "The AWS region the application is running"
  type        = "String"
  value       = data.aws_region.current.name

  tags = local.tags

  depends_on = [aws_ssm_parameter.ssm_paremeters_without_values]
}

resource "aws_ssm_parameter" "invoice_bucket" {
  name        = "/${local.app_name}/${var.environment}/CloudTracker/InvoiceBucket"
  description = "S3 bucket to use for CloudTracker invoices"
  type        = "String"
  value       = aws_s3_bucket.cloudtracker_invoices.bucket

  tags = local.tags

  depends_on = [aws_ssm_parameter.ssm_paremeters_without_values]
}

resource "aws_ssm_parameter" "purchase_bucket" {
  name        = "/${local.app_name}/${var.environment}/CrmSettings/PurchaseBucket"
  description = "S3 bucket to use for purchase requests"
  type        = "String"
  value       = aws_s3_bucket.purchase_requests.bucket

  tags = local.tags

  depends_on = [aws_ssm_parameter.ssm_paremeters_without_values]
}

resource "aws_ssm_parameter" "name_prefix" {
  name        = "/${local.app_name}/${var.environment}/CrmSettings/NamePrefix"
  description = "The naming prefix for cloud resources"
  type        = "String"
  value       = "${local.name_prefix}-"

  tags = local.tags

  depends_on = [aws_ssm_parameter.ssm_paremeters_without_values]
}